namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BoundaryIdChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boundary", "code", c => c.String(maxLength: 8));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boundary", "code");
        }
    }
}
