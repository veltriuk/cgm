namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInAlarmFire : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AlarmFire", "reportInformId", c => c.Long(nullable: false));
            AlterColumn("dbo.AlarmFire", "isActive", c => c.Boolean(nullable: false));
            CreateIndex("dbo.AlarmFire", "reportInformId");
            AddForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform", "id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AlarmFire", "reportInformId", "dbo.Report_Inform");
            DropIndex("dbo.AlarmFire", new[] { "reportInformId" });
            AlterColumn("dbo.AlarmFire", "isActive", c => c.Int(nullable: false));
            AlterColumn("dbo.AlarmFire", "reportInformId", c => c.Int(nullable: false));
        }
    }
}
