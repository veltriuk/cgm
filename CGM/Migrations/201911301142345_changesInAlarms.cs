namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changesInAlarms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetUsersAlarm",
                c => new
                    {
                        AspNetUsers_Id = c.String(nullable: false, maxLength: 128),
                        Alarm_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AspNetUsers_Id, t.Alarm_id })
                .ForeignKey("dbo.AspNetUsers", t => t.AspNetUsers_Id, cascadeDelete: true)
                .ForeignKey("dbo.Alarm", t => t.Alarm_id, cascadeDelete: true)
                .Index(t => t.AspNetUsers_Id)
                .Index(t => t.Alarm_id);
            
            AddColumn("dbo.Alarm", "send_mail", c => c.Boolean(nullable: false));
            AddColumn("dbo.AlarmCase", "allows_multiple", c => c.Boolean(nullable: false));
            DropColumn("dbo.Alarm", "mail_params");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Alarm", "mail_params", c => c.String());
            DropForeignKey("dbo.AspNetUsersAlarm", "Alarm_id", "dbo.Alarm");
            DropForeignKey("dbo.AspNetUsersAlarm", "AspNetUsers_Id", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsersAlarm", new[] { "Alarm_id" });
            DropIndex("dbo.AspNetUsersAlarm", new[] { "AspNetUsers_Id" });
            DropColumn("dbo.AlarmCase", "allows_multiple");
            DropColumn("dbo.Alarm", "send_mail");
            DropTable("dbo.AspNetUsersAlarm");
        }
    }
}
