namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSubstatusClockSync : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Telemetry.ClockSync", "id_substatus");
            AddForeignKey("Telemetry.ClockSync", "id_substatus", "dbo.Substatus", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Telemetry.ClockSync", "id_substatus", "dbo.Substatus");
            DropIndex("Telemetry.ClockSync", new[] { "id_substatus" });
        }
    }
}
