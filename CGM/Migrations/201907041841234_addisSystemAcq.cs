namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addisSystemAcq : DbMigration
    {
        public override void Up()
        {
            AddColumn("Telemetry.Acquisition", "isSystemAcq", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Telemetry.Acquisition", "isSystemAcq");
        }
    }
}
