namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredFieldsInMeterandModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Meter", "serial", c => c.String(nullable: false, maxLength: 30, fixedLength: true));
            AlterColumn("dbo.Model_Meter", "brand", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Model_Meter", "model", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Model_Meter", "model", c => c.String(maxLength: 30));
            AlterColumn("dbo.Model_Meter", "brand", c => c.String(maxLength: 30));
            AlterColumn("dbo.Meter", "serial", c => c.String(maxLength: 30, fixedLength: true));
        }
    }
}
