namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addClockSync : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Telemetry.ClockSync",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_meter = c.Int(nullable: false),
                        time_current = c.DateTime(nullable: false),
                        time_assigned = c.DateTime(nullable: false),
                        sync_date = c.DateTime(nullable: false),
                        id_substatus = c.Int(nullable: false),
                        message = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("Telemetry.Meter", t => t.id_meter, cascadeDelete: true)
                .Index(t => t.id_meter);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Telemetry.ClockSync", "id_meter", "Telemetry.Meter");
            DropIndex("Telemetry.ClockSync", new[] { "id_meter" });
            DropTable("Telemetry.ClockSync");
        }
    }
}
