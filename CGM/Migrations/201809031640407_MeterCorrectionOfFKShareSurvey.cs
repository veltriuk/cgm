namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MeterCorrectionOfFKShareSurvey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Telemetry.Meter", "SurveyMasterId", "Survey.SurveyMaster");
            DropIndex("Telemetry.Meter", new[] { "SurveyMasterId" });
            AddColumn("Telemetry.Meter", "ShareSurveyId", c => c.Guid(nullable: false));
            CreateIndex("Telemetry.Meter", "ShareSurveyId");
            AddForeignKey("Telemetry.Meter", "ShareSurveyId", "Survey.ShareSurvey", "Id", cascadeDelete: true);
            DropColumn("Telemetry.Meter", "SurveyMasterId");
        }
        
        public override void Down()
        {
            AddColumn("Telemetry.Meter", "SurveyMasterId", c => c.Int());
            DropForeignKey("Telemetry.Meter", "ShareSurveyId", "Survey.ShareSurvey");
            DropIndex("Telemetry.Meter", new[] { "ShareSurveyId" });
            DropColumn("Telemetry.Meter", "ShareSurveyId");
            CreateIndex("Telemetry.Meter", "SurveyMasterId");
            AddForeignKey("Telemetry.Meter", "SurveyMasterId", "Survey.SurveyMaster", "Id");
        }
    }
}
