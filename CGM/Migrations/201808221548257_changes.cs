namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Measure", "boundary_id", "dbo.Boundary");
            DropIndex("dbo.Measure", new[] { "boundary_id" });
            AlterColumn("dbo.Measure", "Report_Inform_id", c => c.Int());
            CreateIndex("dbo.Measure", "Report_Inform_id");
            CreateIndex("dbo.Measure", "Boundary_id");
            AddForeignKey("dbo.Measure", "Report_Inform_id", "dbo.Report_Inform", "id");
            AddForeignKey("dbo.Measure", "Boundary_id", "dbo.Boundary", "id");
            DropColumn("dbo.Measure", "back_up_measure");
            DropColumn("dbo.Measure", "unit");
            DropColumn("dbo.Measure", "is_back_up");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measure", "is_back_up", c => c.Boolean());
            AddColumn("dbo.Measure", "unit", c => c.String(maxLength: 20));
            AddColumn("dbo.Measure", "back_up_measure", c => c.Double());
            DropForeignKey("dbo.Measure", "Boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.Measure", "Report_Inform_id", "dbo.Report_Inform");
            DropIndex("dbo.Measure", new[] { "Boundary_id" });
            DropIndex("dbo.Measure", new[] { "Report_Inform_id" });
            AlterColumn("dbo.Measure", "Report_Inform_id", c => c.Long());
            CreateIndex("dbo.Measure", "boundary_id");
            AddForeignKey("dbo.Measure", "boundary_id", "dbo.Boundary", "id", cascadeDelete: true);
        }
    }
}
