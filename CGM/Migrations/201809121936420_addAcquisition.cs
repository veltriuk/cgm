namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAcquisition : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Measure", "quantity_id", "Telemetry.Quantity");
            DropIndex("dbo.Measure", new[] { "quantity_id" });
            CreateTable(
                "Telemetry.Acquisition",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        type = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "Telemetry.Acquisition_Item",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        tag_line_id = c.Int(nullable: false),
                        quantity_id = c.Int(),
                        register_id = c.Int(),
                        waveform_id = c.Int(),
                        meter_events_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Telemetry.Quantity", t => t.quantity_id)
                .ForeignKey("Telemetry.Tag_Line", t => t.tag_line_id, cascadeDelete: true)
                .Index(t => t.tag_line_id)
                .Index(t => t.quantity_id);
            
            CreateTable(
                "Telemetry.Tag_Line",
                c => new
                    {
                        id = c.Int(nullable: false),
                        name = c.String(),
                        nameShort = c.String(),
                        type = c.Int(nullable: false),
                        numval = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.AcquisitionBindTable",
                c => new
                    {
                        acquisition_id = c.Int(nullable: false),
                        acquisition_item_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.acquisition_id, t.acquisition_item_id })
                .ForeignKey("Telemetry.Acquisition", t => t.acquisition_id, cascadeDelete: true)
                .ForeignKey("Telemetry.Acquisition_Item", t => t.acquisition_item_id, cascadeDelete: true)
                .Index(t => t.acquisition_id)
                .Index(t => t.acquisition_item_id);
            
            AddColumn("dbo.Measure", "acquisition_item_id", c => c.Int());
            CreateIndex("dbo.Measure", "acquisition_item_id");
            AddForeignKey("dbo.Measure", "acquisition_item_id", "Telemetry.Acquisition_Item", "Id");
            DropColumn("dbo.Measure", "quantity_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measure", "quantity_id", c => c.Int());
            DropForeignKey("dbo.Measure", "acquisition_item_id", "Telemetry.Acquisition_Item");
            DropForeignKey("dbo.AcquisitionBindTable", "acquisition_item_id", "Telemetry.Acquisition_Item");
            DropForeignKey("dbo.AcquisitionBindTable", "acquisition_id", "Telemetry.Acquisition");
            DropForeignKey("Telemetry.Acquisition_Item", "tag_line_id", "Telemetry.Tag_Line");
            DropForeignKey("Telemetry.Acquisition_Item", "quantity_id", "Telemetry.Quantity");
            DropIndex("dbo.AcquisitionBindTable", new[] { "acquisition_item_id" });
            DropIndex("dbo.AcquisitionBindTable", new[] { "acquisition_id" });
            DropIndex("dbo.Measure", new[] { "acquisition_item_id" });
            DropIndex("Telemetry.Acquisition_Item", new[] { "quantity_id" });
            DropIndex("Telemetry.Acquisition_Item", new[] { "tag_line_id" });
            DropColumn("dbo.Measure", "acquisition_item_id");
            DropTable("dbo.AcquisitionBindTable");
            DropTable("Telemetry.Tag_Line");
            DropTable("Telemetry.Acquisition_Item");
            DropTable("Telemetry.Acquisition");
            CreateIndex("dbo.Measure", "quantity_id");
            AddForeignKey("dbo.Measure", "quantity_id", "Telemetry.Quantity", "id");
        }
    }
}
