namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPriorityParameter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boundary", "priority", c => c.Int());
            AddColumn("Telemetry.Meter", "priority", c => c.Int());
            AddColumn("dbo.Report_Inform", "priority", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Report_Inform", "priority");
            DropColumn("Telemetry.Meter", "priority");
            DropColumn("dbo.Boundary", "priority");
        }
    }
}
