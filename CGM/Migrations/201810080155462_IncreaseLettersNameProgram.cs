namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreaseLettersNameProgram : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Program", new[] { "name" });
            AlterColumn("dbo.Program", "name", c => c.String(maxLength: 100));
            CreateIndex("dbo.Program", "name", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Program", new[] { "name" });
            AlterColumn("dbo.Program", "name", c => c.String(maxLength: 30));
            CreateIndex("dbo.Program", "name", unique: true);
        }
    }
}
