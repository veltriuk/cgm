namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AlterColumn("dbo.Report_Inform", "status", c => c.Int(nullable: false));
            CreateIndex("dbo.Report_Inform", "status");
            AddForeignKey("dbo.Report_Inform", "status", "dbo.Status", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "status", "dbo.Status");
            DropIndex("dbo.Report_Inform", new[] { "status" });
            AlterColumn("dbo.Report_Inform", "status", c => c.String(maxLength: 10, fixedLength: true, unicode: false));
            DropTable("dbo.Status");
        }
    }
}
