namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class coordinatesBoundaries : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Boundary", "latitude", c => c.Single());
            AddColumn("dbo.Boundary", "longitude", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Boundary", "longitude");
            DropColumn("dbo.Boundary", "latitude");
        }
    }
}
