namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class multipleAddings : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Filtro", "isFiled", c => c.Boolean(nullable: false));
            AddColumn("dbo.Filtro", "dateCreated", c => c.DateTime());
            AddColumn("Telemetry.ClockSync", "time_shift", c => c.Int(nullable: false));
            AddColumn("Telemetry.ClockSync", "process_time", c => c.String());
            AddColumn("dbo.ParamQuestion", "alias", c => c.String());
            AlterColumn("dbo.Filtro", "defaultFilter", c => c.Int(nullable: false));
            AlterColumn("Telemetry.ClockSync", "time_current", c => c.DateTime());
            AlterColumn("Telemetry.ClockSync", "time_assigned", c => c.DateTime());
            AlterColumn("Telemetry.ClockSync", "sync_date", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("Telemetry.ClockSync", "sync_date", c => c.DateTime(nullable: false));
            AlterColumn("Telemetry.ClockSync", "time_assigned", c => c.DateTime(nullable: false));
            AlterColumn("Telemetry.ClockSync", "time_current", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Filtro", "defaultFilter", c => c.Boolean(nullable: false));
            DropColumn("dbo.ParamQuestion", "alias");
            DropColumn("Telemetry.ClockSync", "process_time");
            DropColumn("Telemetry.ClockSync", "time_shift");
            DropColumn("dbo.Filtro", "dateCreated");
            DropColumn("dbo.Filtro", "isFiled");
        }
    }
}
