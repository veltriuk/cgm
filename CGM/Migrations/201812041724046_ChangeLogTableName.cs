namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeLogTableName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Log", newName: "LogUser");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.LogUser", newName: "Log");
        }
    }
}
