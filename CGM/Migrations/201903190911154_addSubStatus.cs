namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSubStatus : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Substatus",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idStatus = c.Int(nullable: false),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Status", t => t.idStatus, cascadeDelete: true)
                .Index(t => t.idStatus);
            
            AddColumn("dbo.Report_Inform", "substatus_id", c => c.Int());
            CreateIndex("dbo.Report_Inform", "substatus_id");
            AddForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "substatus_id", "dbo.Substatus");
            DropForeignKey("dbo.Substatus", "idStatus", "dbo.Status");
            DropIndex("dbo.Substatus", new[] { "idStatus" });
            DropIndex("dbo.Report_Inform", new[] { "substatus_id" });
            DropColumn("dbo.Report_Inform", "substatus_id");
            DropTable("dbo.Substatus");
        }
    }
}
