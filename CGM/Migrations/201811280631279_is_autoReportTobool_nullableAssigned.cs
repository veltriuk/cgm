namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class is_autoReportTobool_nullableAssigned : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Report", "is_auto", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Report_Inform", "assignedReading", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Report_Inform", "assignedReading", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Report", "is_auto", c => c.Int(nullable: false));
        }
    }
}
