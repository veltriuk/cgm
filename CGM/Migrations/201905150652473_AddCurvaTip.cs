namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCurvaTip : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CurvaTip",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        month = c.Int(nullable: false),
                        year = c.Int(nullable: false),
                        acquisition_item_id = c.Int(),
                        scope = c.String(),
                        dateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("Telemetry.Acquisition_Item", t => t.acquisition_item_id)
                .Index(t => t.acquisition_item_id);
            
            CreateTable(
                "dbo.CurvaTipMeasureGroup",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        boundary_id = c.Int(nullable: false),
                        curvaTip_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Boundary", t => t.boundary_id, cascadeDelete: true)
                .ForeignKey("dbo.CurvaTip", t => t.curvaTip_id, cascadeDelete: true)
                .Index(t => t.boundary_id)
                .Index(t => t.curvaTip_id);
            
            CreateTable(
                "dbo.CurvaTipMeasure",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        datetime = c.DateTime(nullable: false),
                        measure = c.Double(nullable: false),
                        group_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CurvaTipMeasureGroup", t => t.group_id, cascadeDelete: true)
                .Index(t => t.group_id);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CurvaTipMeasure", "group_id", "dbo.CurvaTipMeasureGroup");
            DropForeignKey("dbo.CurvaTipMeasureGroup", "curvaTip_id", "dbo.CurvaTip");
            DropForeignKey("dbo.CurvaTipMeasureGroup", "boundary_id", "dbo.Boundary");
            DropForeignKey("dbo.CurvaTip", "acquisition_item_id", "Telemetry.Acquisition_Item");
            DropIndex("dbo.CurvaTipMeasure", new[] { "group_id" });
            DropIndex("dbo.CurvaTipMeasureGroup", new[] { "curvaTip_id" });
            DropIndex("dbo.CurvaTipMeasureGroup", new[] { "boundary_id" });
            DropIndex("dbo.CurvaTip", new[] { "acquisition_item_id" });
            DropTable("dbo.CurvaTipMeasure");
            DropTable("dbo.CurvaTipMeasureGroup");
            DropTable("dbo.CurvaTip");
        }
    }
}
