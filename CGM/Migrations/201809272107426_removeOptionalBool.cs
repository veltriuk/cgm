namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeOptionalBool : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Boundary", "has_backup", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Boundary", "has_backup", c => c.Boolean());
        }
    }
}
