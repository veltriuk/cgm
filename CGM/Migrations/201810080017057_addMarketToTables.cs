namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMarketToTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Filtro", "market_id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Filtro", "market_id");
        }
    }
}
