namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addContinuousToProgram : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Program", "continuous", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Program", "continuous");
        }
    }
}
