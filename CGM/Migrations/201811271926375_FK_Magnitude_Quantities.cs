namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FK_Magnitude_Quantities : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Telemetry.Quantity", "magnitude_id");
            AddForeignKey("Telemetry.Quantity", "magnitude_id", "Telemetry.Magnitude", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("Telemetry.Quantity", "magnitude_id", "Telemetry.Magnitude");
            DropIndex("Telemetry.Quantity", new[] { "magnitude_id" });
        }
    }
}
