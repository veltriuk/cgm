namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class filedAndProgram : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Program", "filed", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Program", "filed");
        }
    }
}
