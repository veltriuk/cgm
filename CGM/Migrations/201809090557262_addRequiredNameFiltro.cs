namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRequiredNameFiltro : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Filtro", "Name", c => c.String(nullable: false, maxLength: 30, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Filtro", "Name", c => c.String(maxLength: 30, unicode: false));
        }
    }
}
