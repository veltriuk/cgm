namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class connectionMeterAndSurvey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Meter", "SurveyMasterId", c => c.Int());
            CreateIndex("dbo.Meter", "SurveyMasterId");
            AddForeignKey("dbo.Meter", "SurveyMasterId", "dbo.SurveyMaster", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Meter", "SurveyMasterId", "dbo.SurveyMaster");
            DropIndex("dbo.Meter", new[] { "SurveyMasterId" });
            DropColumn("dbo.Meter", "SurveyMasterId");
        }
    }
}
