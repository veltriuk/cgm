namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isFiledAlarmAnNullEventInRepInf : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Report_Inform", "event_id", "dbo.Event");
            DropIndex("dbo.Report_Inform", new[] { "event_id" });
            AddColumn("dbo.Alarm", "isFiled", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Report_Inform", "event_id", c => c.Int());
            CreateIndex("dbo.Report_Inform", "event_id");
            AddForeignKey("dbo.Report_Inform", "event_id", "dbo.Event", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "event_id", "dbo.Event");
            DropIndex("dbo.Report_Inform", new[] { "event_id" });
            AlterColumn("dbo.Report_Inform", "event_id", c => c.Int(nullable: false));
            DropColumn("dbo.Alarm", "isFiled");
            CreateIndex("dbo.Report_Inform", "event_id");
            AddForeignKey("dbo.Report_Inform", "event_id", "dbo.Event", "id", cascadeDelete: true);
        }
    }
}
