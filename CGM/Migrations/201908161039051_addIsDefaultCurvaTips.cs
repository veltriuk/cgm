namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsDefaultCurvaTips : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CurvaTip", "isDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CurvaTip", "isDefault");
        }
    }
}
