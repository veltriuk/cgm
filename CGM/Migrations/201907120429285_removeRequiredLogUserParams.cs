namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeRequiredLogUserParams : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LogUser", "IP", c => c.String(maxLength: 20));
            AlterColumn("dbo.LogUser", "lat", c => c.String(maxLength: 50));
            AlterColumn("dbo.LogUser", "lon", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LogUser", "lon", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.LogUser", "lat", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.LogUser", "IP", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
