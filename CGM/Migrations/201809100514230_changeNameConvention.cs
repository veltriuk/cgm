namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNameConvention : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Telemetry.Meter", name: "BoundaryId", newName: "boundary_id");
            RenameColumn(table: "dbo.Measure", name: "QuantityId", newName: "quantity_id");
            RenameColumn(table: "dbo.Report_Inform", name: "EventId", newName: "event_id");
            RenameIndex(table: "Telemetry.Meter", name: "IX_BoundaryId", newName: "IX_boundary_id");
            RenameIndex(table: "dbo.Measure", name: "IX_QuantityId", newName: "IX_quantity_id");
            RenameIndex(table: "dbo.Report_Inform", name: "IX_EventId", newName: "IX_event_id");
            AddColumn("dbo.Filtro", "filter_type", c => c.Int(nullable: false));
            AddColumn("dbo.Program", "execution_time", c => c.DateTime(nullable: false));
            DropColumn("dbo.Filtro", "FilterType");
            DropColumn("dbo.Program", "ExecutionTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Program", "ExecutionTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Filtro", "FilterType", c => c.Int(nullable: false));
            DropColumn("dbo.Program", "execution_time");
            DropColumn("dbo.Filtro", "filter_type");
            RenameIndex(table: "dbo.Report_Inform", name: "IX_event_id", newName: "IX_EventId");
            RenameIndex(table: "dbo.Measure", name: "IX_quantity_id", newName: "IX_QuantityId");
            RenameIndex(table: "Telemetry.Meter", name: "IX_boundary_id", newName: "IX_BoundaryId");
            RenameColumn(table: "dbo.Report_Inform", name: "event_id", newName: "EventId");
            RenameColumn(table: "dbo.Measure", name: "quantity_id", newName: "QuantityId");
            RenameColumn(table: "Telemetry.Meter", name: "boundary_id", newName: "BoundaryId");
        }
    }
}
