namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedIndexesToForms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParamAnswerOption", "index", c => c.Int(nullable: false));
            AddColumn("dbo.ParamQuestion", "index", c => c.Int(nullable: false));
            AddColumn("dbo.ParamGroup", "index", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ParamGroup", "index");
            DropColumn("dbo.ParamQuestion", "index");
            DropColumn("dbo.ParamAnswerOption", "index");
        }
    }
}
