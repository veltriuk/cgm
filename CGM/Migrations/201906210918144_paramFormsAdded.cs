namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class paramFormsAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ParamAnswerOption",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        paramQuestion_id = c.Int(nullable: false),
                        name = c.String(),
                        alias = c.String(),
                        value = c.String(),
                        type_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ParamQuestion", t => t.paramQuestion_id, cascadeDelete: true)
                .ForeignKey("dbo.ParamType", t => t.type_id, cascadeDelete: false)
                .Index(t => t.paramQuestion_id)
                .Index(t => t.type_id);
            
            CreateTable(
                "dbo.ParamAnswer",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        formInstance_id = c.Int(nullable: false),
                        answerOption_id = c.Int(nullable: false),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ParamAnswerOption", t => t.answerOption_id, cascadeDelete: true)
                .ForeignKey("dbo.ParamFormInstance", t => t.formInstance_id, cascadeDelete: true)
                .Index(t => t.formInstance_id)
                .Index(t => t.answerOption_id);
            
            CreateTable(
                "dbo.ParamFormInstance",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        paramForm_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ParamForm", t => t.paramForm_id, cascadeDelete: true)
                .Index(t => t.paramForm_id);
            
            CreateTable(
                "dbo.ParamForm",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ParamQuestion",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        group_id = c.Int(nullable: false),
                        question = c.String(),
                        type_id = c.Int(nullable: false),
                        conditionals = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ParamGroup", t => t.group_id, cascadeDelete: true)
                .ForeignKey("dbo.ParamType", t => t.type_id, cascadeDelete: false)
                .Index(t => t.group_id)
                .Index(t => t.type_id);
            
            CreateTable(
                "dbo.ParamGroup",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        paramForm_id = c.Int(nullable: false),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ParamForm", t => t.paramForm_id, cascadeDelete: false)
                .Index(t => t.paramForm_id);
            
            CreateTable(
                "dbo.ParamType",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        level = c.Int(nullable: false),
                        type = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParamAnswerOption", "type_id", "dbo.ParamType");
            DropForeignKey("dbo.ParamAnswerOption", "paramQuestion_id", "dbo.ParamQuestion");
            DropForeignKey("dbo.ParamQuestion", "type_id", "dbo.ParamType");
            DropForeignKey("dbo.ParamQuestion", "group_id", "dbo.ParamGroup");
            DropForeignKey("dbo.ParamGroup", "paramForm_id", "dbo.ParamForm");
            DropForeignKey("dbo.ParamAnswer", "formInstance_id", "dbo.ParamFormInstance");
            DropForeignKey("dbo.ParamFormInstance", "paramForm_id", "dbo.ParamForm");
            DropForeignKey("dbo.ParamAnswer", "answerOption_id", "dbo.ParamAnswerOption");
            DropIndex("dbo.ParamGroup", new[] { "paramForm_id" });
            DropIndex("dbo.ParamQuestion", new[] { "type_id" });
            DropIndex("dbo.ParamQuestion", new[] { "group_id" });
            DropIndex("dbo.ParamFormInstance", new[] { "paramForm_id" });
            DropIndex("dbo.ParamAnswer", new[] { "answerOption_id" });
            DropIndex("dbo.ParamAnswer", new[] { "formInstance_id" });
            DropIndex("dbo.ParamAnswerOption", new[] { "type_id" });
            DropIndex("dbo.ParamAnswerOption", new[] { "paramQuestion_id" });
            DropTable("dbo.ParamType");
            DropTable("dbo.ParamGroup");
            DropTable("dbo.ParamQuestion");
            DropTable("dbo.ParamForm");
            DropTable("dbo.ParamFormInstance");
            DropTable("dbo.ParamAnswer");
            DropTable("dbo.ParamAnswerOption");
        }
    }
}
