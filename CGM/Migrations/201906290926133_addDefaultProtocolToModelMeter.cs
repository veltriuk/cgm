namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDefaultProtocolToModelMeter : DbMigration
    {
        public override void Up()
        {
            AddColumn("Telemetry.Model_Meter", "defaultProtocol", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Telemetry.Model_Meter", "defaultProtocol");
        }
    }
}
