namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class idofReportsFRomGuidToInt : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Report_Inform", "reports_id", "dbo.Report");
            DropIndex("dbo.Report_Inform", new[] { "reports_id" });
            DropPrimaryKey("dbo.Report");
            CreateTable(
                "Survey.Dependency",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        id_question = c.Int(nullable: false),
                        id_response = c.Int(nullable: false),
                        id_response_value = c.Int(nullable: false),
                        id_response_dependant = c.Int(nullable: false),
                        possible_values = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.id);
            DropColumn("dbo.Report","Id");
            AddColumn("dbo.Report", "id", c => c.Int(nullable: false, identity: true));

            DropColumn("dbo.Report_Inform", "reports_id");
            AddColumn("dbo.Report_Inform", "reports_id", c => c.Int());
            AddPrimaryKey("dbo.Report", "id");
            CreateIndex("dbo.Report_Inform", "reports_id");
            AddForeignKey("dbo.Report_Inform", "reports_id", "dbo.Report", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Report_Inform", "reports_id", "dbo.Report");
            DropIndex("dbo.Report_Inform", new[] { "reports_id" });
            DropPrimaryKey("dbo.Report");

            DropColumn("dbo.Report_Inform", "reports_id");
            DropColumn("dbo.Report", "Id");

            AddColumn("dbo.Report_Inform", "reports_id", c => c.Guid());
            AddColumn("dbo.Report", "id", c => c.Guid(nullable: false));
            DropTable("Survey.Dependency");
            AddPrimaryKey("dbo.Report", "id");
            CreateIndex("dbo.Report_Inform", "reports_id");
            AddForeignKey("dbo.Report_Inform", "reports_id", "dbo.Report", "id");
        }
    }
}
