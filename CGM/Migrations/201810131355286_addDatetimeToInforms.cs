namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDatetimeToInforms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Report_Inform", "end_time", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Report_Inform", "end_time");
        }
    }
}
