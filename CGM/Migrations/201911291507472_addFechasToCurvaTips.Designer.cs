// <auto-generated />
namespace CGM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addFechasToCurvaTips : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addFechasToCurvaTips));
        
        string IMigrationMetadata.Id
        {
            get { return "201911291507472_addFechasToCurvaTips"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
