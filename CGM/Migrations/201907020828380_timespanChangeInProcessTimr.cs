namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class timespanChangeInProcessTimr : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Telemetry.ClockSync", "process_time", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("Telemetry.ClockSync", "process_time", c => c.String());
        }
    }
}
