namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAcquisitionItemGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("Telemetry.Acquisition_Item", "group", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Telemetry.Acquisition_Item", "group");
        }
    }
}
