namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullsInMeasures : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Measure", "measure_date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Measure", "measure", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Measure", "measure", c => c.Double());
            AlterColumn("dbo.Measure", "measure_date", c => c.DateTime());
        }
    }
}
