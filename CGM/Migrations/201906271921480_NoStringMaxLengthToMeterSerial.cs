namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NoStringMaxLengthToMeterSerial : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("Telemetry.Meter", "serial", c => c.String(nullable: false, maxLength: 128, fixedLength: true));
        }
        
        public override void Down()
        {
            //AlterColumn("Telemetry.Meter", "serial", c => c.String(nullable: false, maxLength: 30, fixedLength: true));
        }
    }
}
