namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPriorityAndKVAR : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Priority",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        value = c.Int(nullable: false),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            RenameColumn("dbo.Boundary", "max_measure_value", "max_active");
            RenameColumn("dbo.Boundary", "min_measure_value", "min_active");

            AddColumn("dbo.Boundary", "max_reactive", c => c.Double());
            AddColumn("dbo.Boundary", "min_reactive", c => c.Double());

            AddColumn("dbo.Boundary", "id_priority", c => c.Int());
            CreateIndex("dbo.Boundary", "id_priority");
            AddForeignKey("dbo.Boundary", "id_priority", "dbo.Priority", "id");

            DropColumn("dbo.Boundary", "priority");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Boundary", "priority", c => c.Int());
            AddColumn("dbo.Boundary", "min_measure_value", c => c.Double());
            AddColumn("dbo.Boundary", "max_measure_value", c => c.Double());
            DropForeignKey("dbo.Boundary", "id_priority", "dbo.Priority");
            DropIndex("dbo.Boundary", new[] { "id_priority" });
            DropColumn("dbo.Boundary", "id_priority");
            DropColumn("dbo.Boundary", "min_reactive");
            DropColumn("dbo.Boundary", "max_reactive");
            DropColumn("dbo.Boundary", "min_active");
            DropColumn("dbo.Boundary", "max_active");
            DropTable("dbo.Priority");
        }
    }
}
