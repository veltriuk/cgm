namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StringChangeConfigCM : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Config_CM", "element", c => c.String(maxLength: 50));
            AlterColumn("dbo.Config_CM", "text", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Config_CM", "text", c => c.String(maxLength: 200, fixedLength: true));
            AlterColumn("dbo.Config_CM", "element", c => c.String(maxLength: 30, fixedLength: true));
        }
    }
}
