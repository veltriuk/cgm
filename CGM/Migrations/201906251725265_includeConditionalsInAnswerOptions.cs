namespace CGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includeConditionalsInAnswerOptions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParamAnswerOption", "conditionals", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ParamAnswerOption", "conditionals");
        }
    }
}
