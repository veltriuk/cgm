﻿using System.Web;
using System.Web.Optimization;

namespace CGM
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //Scripts--------------------------------------------------

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery1").Include(
                        "~/Content/vendor/jquery/jquery.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jquery2").Include(
                     "~/Content/vendor/jquery.cookie/jquery.cookie.js",
                     "~/Content/vendor/jquery-ajax-unobtrusive-master/src/jquery.unobtrusive-ajax.js",
                     "~/Content/vendor/jquery-validation/jquery.validate.min.js",
                     "~/Content/vendor/jquery.validate.unobtrusive.min.js",
                     "~/Content/js/jqueryValidateSpanish.js"
                      )); //No tuve tiempo para ver porqué solamente funcionaba en ese orden

            bundles.Add(new ScriptBundle("~/bundles/popper").Include(
                    "~/Content/vendor/popper.js/umd/popper.min.js"
                ));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/vendor/fileinput.js",
                      "~/Content/vendor/locales/es.js",
                      "~/Content/vendor/bootstrap-fileinput/themes/explorer/theme.js",
                      "~/Content/vendor/bootstrap/js/bootstrap.min.js",
                      "~/Content/vendor/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/otherJs1").Include(
                       "~/Content/js/grasp_mobile_progress_circle-1.0.0.min.js",
                      "~/Content/vendor/tempusdominus/moment-with-locales.min.js"
                       ));

           

            bundles.Add(new ScriptBundle("~/bundles/otherJs2").Include(
                       "~/Content/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
                       "~/Content/vendor/ladda/ladda.jquery.min.js",
                       "~/Content/vendor/ladda/spin.js",
                       "~/Content/vendor/ladda/ladda.min.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/dataTablesScript").Include(
                        "~/Content/vendor/DataTables/datatables.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/dataTablesScript1").Include(
            "~/Content/vendor/datatables.net/js/jquery.dataTables.js",
            "~/Content/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js",
            "~/Content/vendor/datatables.net-responsive/js/dataTables.responsive.min.js",
            "~/Content/vendor/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js",
            "~/Content/vendor/datatables.net/js/dataTables.buttons.min.js"
            //"~/Content/js/tables-datatable.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                     "~/Content/vendor/tempusdominus/moment-with-locales.min.js",
                     "~/Content/vendor/tempusdominus/tether.min.js"              ,     
                     "~/Content/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js"
                     ));
             bundles.Add(new ScriptBundle("~/bundles/messenger").Include(
                     "~/Content/vendor/messenger-hubspot/build/js/messenger.js"
                           ));

            bundles.Add(new ScriptBundle("~/bundles/dateRange").Include(
                "~/Content/vendor/daterangepicker-master/daterangepicker.js",
                "~/Content/vendor/daterangepicker-master/moment.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/dateRange").Include(
                "~/Content/vendor/daterangepicker-master/daterangepicker.js",
                "~/Content/vendor/daterangepicker-master/moment.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/highcharts").Include(
                "~/Content/vendor/highcharts/highstock.js"  ,
                "~/Content/vendor/highcharts/highcharts-more.js"  ,
                "~/Content/vendor/highcharts/data.js"     ,
                "~/Content/vendor/highcharts/drilldown.js",
                "~/Content/vendor/highcharts/exporting",
                "~/Content/vendor/highcharts/export-data.js",
                "~/Content/vendor/highcharts/histogram-bellcurve.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/multi_select").Include(
                "~/Content/vendor/multiselect/js/jquery.multi-select.js",
                "~/Content/vendor/jquery.quicksearch.js",
                "~/Scripts/Helpers.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/tablebuttons").Include(

                "~/Content/vendor/datatables_Buttons/dataTables.buttons.min.js",
                "~/Content/vendor/datatables_Buttons/buttons.flash.min.js",
                "~/Content/vendor/datatables_Buttons/jszip.min.js",
                "~/Content/vendor/datatables_Buttons/pdfmake.min.js",
                "~/Content/vendor/datatables_Buttons/vfs_fonts.js",
                "~/Content/vendor/datatables_Buttons/buttons.html5.min.js",
                "~/Content/vendor/datatables_Buttons/buttons.print.min.js",
                "~/Content/vendor/datatables_Buttons/buttons.colVis.min.js"

               ));

            bundles.Add(new ScriptBundle("~/bundles/multi_select").Include(
                "~/Content/vendor/multiselect/css/multi-select.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/multi_select").Include(
                "~/Content/vendor/multiselect/css/multi-select.css"

                ));

            //Styles--------------------------------------------------

            bundles.Add(new StyleBundle("~/Content/vendor").Include(
                "~/Content/vendor/bootstrap/css/bootstrap.min.css",
                "~/Content/vendor/bootstrap-fileinput/css/fileinput.min.css",
                "~/Content/vendor/bootstrap-fileinput/themes/explorertheme.css",
                "~/Content/vendor/font-awesome/css/font-awesome.min.css",
                "~/Content/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css",
                "~/Content/vendor/ladda/ladda.min.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/fontastic.css",
                "~/Content/css/grasp_mobile_progress_circle-1.0.0.min.css",
                "~/Content/css/style.blue.premium.css"
                

          ));

            bundles.Add(new StyleBundle("~/Content/messenger").Include(
               "~/Content/vendor/messenger-hubspot/build/css/messenger.css",
               "~/Content/vendor/messenger-hubspot/build/css/messenger-theme-air.css"   
          ));

            bundles.Add(new StyleBundle("~/bundles/dataTablesStyle").Include(
            "~/Content/vendor/DataTables/datatables.min.css",
            "~/Content/vendor/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"));

            bundles.Add(new StyleBundle("~/bundles/dataTablesStyle1").Include(
            "~/Content/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css",
            "~/Content/vendor/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css"));

            bundles.Add(new StyleBundle("~/bundles/datepicker").Include(
            "~/Content/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css"
                ));

            bundles.Add(new StyleBundle("~/bundles/dateRange").Include(
                       "~/Content/vendor/daterangepicker-master/daterangepicker.css"
                           ));
            bundles.Add(new StyleBundle("~/bundles/tablebuttonsStyle").Include(
                                 "~/Content/vendor/datatables_Buttons/buttons.bootstrap4.min.css"
                                      ));

        }
    }
}
