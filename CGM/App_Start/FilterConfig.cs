﻿using CGM.Helpers.Attributes;
using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;
using System;
using System.Web;
using System.Web.Mvc;

namespace CGM
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new ProlongExpirationTimeAttribute()); ////Apply attribute to extend the days of cleaning of the jobs. No se aplica, pues se requiere solamente en algunas acciones, más no en todos los controladores.
            // filters.Add(new CustomAuthorize()); //No debe agregarse! Porque solicita autenticación incluso para la página de Login 
        }
    }
}
