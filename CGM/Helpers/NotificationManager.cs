﻿using CGM.Models.MyModels;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using CGM.Models.CGMModels;

namespace CGM.Helpers
{
    public class NotificationManager
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alarmFires">List of triggered alarms. They must be saved already, and have an assigned ID.</param>
        /// <returns></returns>
        public static void addAlarmNotificationRange(List<AlarmFire> alarmFires)
        {
            foreach (AlarmFire alarmFire in alarmFires)
            {
                try
                {
                    // Iterate over the alarmFire list. They have an ID already.
                    addAlarmNotification(alarmFire);
                }
                catch (Exception ex)
                {
                    string message = "Error añadiendo una alarma disparada, con id = " + alarmFire.id + ". " + ex.Message;
                    logger.Error(message, ex);
                    throw new Exception(message, ex);
                }
            }
        }
        /// <summary>
        /// Takes the alarm and includes it into the corresponding group. If no group exists, then it is created.
        /// </summary>
        /// <param name="alarm"></param>
        /// <returns></returns>
        public static string addAlarmNotification(AlarmFire alarmFire)
        {
            Notification notif = new Notification()
            {
                alarmFire_id = alarmFire.id,
                date_created = DateTime.Now,
                message = GetNotificationMessage(alarmFire)
            };

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                // Get the non-read group, corresponding to the same Alarm instance 

                // There should be only one group non read at the time. 
                NotificationGroup notifGroupUpdate = db.NotificationGroups.Where(n => n.is_active
                                                                                  && n.alarm_id.Value == alarmFire.alarmId).FirstOrDefault();

                if (notifGroupUpdate != null) // If the group exists, add the new notification
                {
                    notifGroupUpdate.Notifications.Add(notif);

                }
                else // If not, create the group
                {
                    //db.Configuration.ProxyCreationEnabled = false;

                    //AlarmFire af = db.AlarmFire.Find(alarmFire.id);

                    notifGroupUpdate = new NotificationGroup()
                    {
                        alarm_id = alarmFire.alarmId,
                        is_active = true,
                        Notifications = new List<Notification>() { notif }, // Add the new notification
                    };
                    db.NotificationGroups.Add(notifGroupUpdate);
                }

                // Update message and save
                notifGroupUpdate.message = GetNotificationMessage(notifGroupUpdate);
                db.SaveChanges();
            }
            return "OK"; // Just to not return empty;
        }

        /// <summary>
        /// Se construye el mensaje a mostrar basado en el contenido del CriticSummary, en el caso No. 0.
        /// </summary>
        /// <param name="alarmFire"></param>
        /// <returns></returns>
        public static string GetNotificationMessage(AlarmFire alarmFire)
        {
            string message = "";

            switch (alarmFire.Alarm.alarmCase_id)
            {
                case 0:  // Valor excedido
                    CriticSummary criticSummary = JsonConvert.DeserializeObject<CriticSummary>(alarmFire.details);
                    List<CriticBoundary> boundaries = criticSummary.boundaries.Where(b => b.violates != null).Where(b => b.violates.Value).ToList();

                    message = "[" + alarmFire.Alarm.name + "] " +
                              "Se detectó un exceso en el rango programado en la frontera \"" + boundaries.First().name + "\"";

                    if (boundaries.Count > 1)
                    {
                        if (boundaries.Count == 2)
                        {
                            message += ", y en \"" + boundaries.Skip(1).First().name + "\"";
                        }
                        else
                        {
                            message += ", y en otras " + (boundaries.Count - 1) + " fronteras";
                        }
                    }

                    message += "."; // Se acaba el mensaje
                    break;

                // Se deben incluir los demás casos de alarmas
                default:
                    break;
            }

            return message;
        }
        public static string GetNotificationMessage(NotificationGroup notifGroup)
        {
            string message = "";
            int countCases = 0; // Number of excess cases in total in the notif group object internally
            CriticSummary criticSummary;
            Alarm alarm;
            List<AlarmFire> alarmFires;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                int[] alarmFiresIds = notifGroup.Notifications.Where(n => n.alarmFire_id != null).Select(n => n.alarmFire_id.Value).ToArray(); // Hay que hacerlo así porque dentro del query de Linq no deja
                //db.Configuration.ProxyCreationEnabled = false;
                alarm = db.Alarms.Find(notifGroup.alarm_id);
                alarmFires = db.AlarmFires.AsNoTracking().Where(a => alarmFiresIds.Contains(a.id)).ToList();
            }

            switch (alarm.alarmCase_id)
            {
                case 0: // Valor excedido

                    // Get the critic summary data from the first case, just to take a couple of names.
                    int cont = 0;
                    foreach (Notification notif in notifGroup.Notifications)
                    {
                        var afire = alarmFires.Where(a => a.id == notif.alarmFire_id).FirstOrDefault();

                        criticSummary = JsonConvert.DeserializeObject<CriticSummary>(afire.details);

                        // Only take those boundaries that violated the range
                        List<CriticBoundary> boundaries = criticSummary.boundaries.Where(b => b.violates != null).Where(b => b.violates.Value).ToList();

                        if (cont == 0)
                        {
                            message = "[" + alarm.name + "] " +
                                      "Se detectó excesos en el rango programado en la frontera \"" + boundaries.First().name + "\"";

                            countCases += boundaries.Count - 1; // Remove the named boundary from the count
                        }
                        else
                        {
                            // Sum the number of total cases where excess was found
                            countCases += boundaries.Count;
                        }
                        cont++;
                    }

                    if (countCases == 1)
                    {
                        message += ", y en otro caso.";
                    }
                    if (countCases > 1)
                    {
                        message += ", y en otros " + countCases + " casos.";
                    }

                    break;
                default:
                    break;
            }

            return message;
        }
    }
}