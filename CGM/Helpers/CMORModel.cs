﻿using CGM.Models.MyModels;
using GM_GD.Models.CMORMethods;
using GM_GD.Models.SGMModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace GM_GD.Models.CMORModel
{
    
    public class EnvioXM
    {
        static string sp = Environment.NewLine;
        XMReportService.ProcessRequestResult result { get; set; }

        public static XMReportService.ProcessRequestResult EnviarReporteServicioWebGeneral(CMORReportObject ObjectCMOR)
        {
            if (ObjectCMOR == null || UserData == null || URLService == null)
                return null;
            List<XMReportService.ReadingReportItem> listResult = new List<XMReportService.ReadingReportItem>();  // Se crea la lista inicial para incluir todas las Fronteras
            XMReportService.ReadingReportServiceClient client = new XMReportService.ReadingReportServiceClient("BasicHttpsBinding_IReadingReportService", URLService); //Inicialización del cliente
            XMReportService.UserData userData = UserData; // Estructura con los datos del usuario
            XMReportService.ReadingReportItem XMReport = new XMReportService.ReadingReportItem();
            foreach (var ReportElement in CMORReportObject.CmorObject.ReportList)
            {
                listResult.Add(new XMReportService.ReadingReportItem()
                {
                    is_backup = false,
                    ReadingCount = 24,
                    ReadingInterval = 60,
                    StartDate = CMORReportObject.CmorObject.dates_to_reports.operation_date.Date,
                    BorderCode = ReportElement.boundary.id, //Inserta código de frontera
                    Readings = Array.ConvertAll(ReportElement.measures.ToArray(), x => (double)x.measure1)
                });
            }
            try
            {
                XMReportService.ProcessRequestResult result = client.ReportReadings(listResult.ToArray(), userData);
                // Si el resultado contiene un mensaje de error ocurrió algun problema y no fue posible procesar las lecturas.
                if (!string.IsNullOrEmpty(result.ErrorMessage))
                {
                    //MessageBox.Show(string.Format("Error enviando lecturas : {0}", result.ErrorMessage));
                }
                else
                {
                    // Si no se obtuvo mensaje de error obtenemos el identificador del proceso que nos entrega el servicio.
                    //tbProcessID.Text = result1.ProcessId.ToString();
                    // MessageBox.Show(string.Format("Lecturas enviadas con exito : {0}", result.ProcessId));
                }
                client.Close();
                return result;
            }
            catch (Exception ex)
            {
                client.Close();
                return null;
            }
        }

        public static XMReportService.ReportReadingProcessResult GeneracionProcessResult(XMReportService.ProcessRequestResult result)
        {
            try
            {
                //Se hace la consulta de los resultados del proceso de cargas de medida
                XMReportService.ReadingReportServiceClient client = new XMReportService.ReadingReportServiceClient("BasicHttpsBinding_IReadingReportService", URLService);

                XMReportService.ReportReadingProcessResult ProcessResult = client.GetProcessResult(result.ProcessId, UserData);
                // var ProcessResult = client.GetProcessResult(idprueba, VarCMOR.userData);
                client.Close();
                return ProcessResult;
            }
            catch (Exception ex)
            {
                return null;
            }
         
        }

        public static XMReportService.UserData UserData { get; set; } = new XMReportService.UserData();

        public static void GenerarArchivoTipoCR(CMORReportObject cmorObject)
        {
            try
            {
                string path = System.Web.HttpContext.Current.Server.MapPath("~//Files//Reports//CRFiles//") + "CR" + String.Format("{0}", cmorObject.dates_to_reports.report_type_id == 0 ? "25" : "26") + cmorObject.dates_to_reports.operation_date.Month.ToString("D2") + cmorObject.dates_to_reports.operation_date.Day.ToString("D2") + ".txt";
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
                DataAndDB.pathCR = path;

                string text = "";
                foreach (var frt in cmorObject.ReportList)
                {
                    text += frt.boundary.id + "\tP\t0\t"; //Inserta el código de frontera
                    foreach (var medida in frt.measures)
                    {
                        text += String.Format("{0}\t", Math.Round(medida.measure1, 2)).Replace(',', '.'); //Horas de la 1 am hasta 12 am del día siguiente
                    }
                    text += Environment.NewLine;
                }
                GM_GD.Models.CMORMethods.FileManagement.WriteTXT(text, path);
            }
            catch (Exception ex)
            {

            }
        }

        public static void GenerarArchivodeResultados(CMORReportObject cmorObject)
        {
            string text = "";
            string path = System.Web.HttpContext.Current.Server.MapPath("~//Files//Reports//ProcessResults//") + "Resultado-" + "CR" + String.Format("{0}", cmorObject.dates_to_reports.report_type_id == 0 ? "25" : "26") + cmorObject.dates_to_reports.operation_date.Month.ToString("D2") + cmorObject.dates_to_reports.operation_date.Day.ToString("D2") + "_" + cmorObject.reports.report_id + ".txt";
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            DataAndDB.pathResults = path;

            text += "Resultado de Reporte" + sp + sp +
                        "\t\tDatos de Envío" + sp + sp +
                        "Unidad de Negocio: " + cmorObject.report_types.Where(r => r.id == cmorObject.dates_to_reports.report_type_id).Select(r => r.report_type1).First() + sp +
                        "Fecha de Envío: " + cmorObject.reports.date_and_time_report.Value.ToLongDateString() + sp +
                        "Hora de Envío " + cmorObject.reports.date_and_time_report.Value.ToLongTimeString() + sp +
                        "Proceso Aceptado: " + cmorObject.reports.result.ToString() + sp +
                        "ID de Proceso: " + cmorObject.reports.report_id + sp +
                        "Número de Fronteras reportadas: " + cmorObject.ReportList.Count + sp +
                        "Datos enviados:" + sp + sp;

            foreach (var frt in cmorObject.ReportList)
            {
                text += frt.boundary.id + "\tP\t0\t"; //Inserta el código de frontera
                foreach (var medida in frt.measures)
                {
                    text += String.Format("{0}\t", Math.Round(medida.measure, 2)).Replace(',', '.'); //Horas de la 1 am hasta 12 am del día siguiente
                }
                text += sp;
            }
            text += sp + sp;

            text += "\t\tResultado de Proceso" + sp + sp +
                    "Fecha de Operación: " + cmorObject.dates_to_reports.operation_date.ToLongDateString() + sp +
                    "Resultado: " + cmorObject.reports.result + sp +
                    "Datos:" + sp +
                    sp;

            text += "No." + "\tFrontera".PadRight(30) + "Código SIC" + "\tResultado" + "\tMensaje de Error" + sp;
            int c = 1;
            foreach (var frt in cmorObject.ReportInformlist)
            {
                text += c++ + "\t" +
                        cmorObject.BoundaryList.Where(f => f.id == frt.boundary_id).Select(f => f.name).First().PadRight(30) + "\t" + //Inserta el código de frontera
                        cmorObject.BoundaryList.Where(f => f.id == frt.boundary_id).Select(f => f.id).First() + "\t" + //Inserta el código de frontera
                        frt.success + "\t\t" +
                        frt.message; ;
                text += sp;
            }

            FileManagement.WriteTXT(text, path);
        }

        public static string URLService { get; set; }
        public static string ReportConclusion()
        {
            string lightredcolor = "#f86e6e";
            string whitecolor = "#ffffff";
            int c = 1;
            string redstyle = "style =\"color: red\"";
            string greenstyle = "style =\"color: green\"";
            string h1red_open = "<h1 " + redstyle + ">";
            string h2green_open = "<h2 " + greenstyle + ">";
            string h4red_open = "<h4 " + redstyle + ">";
            string h1_close = "</h1>";
            string h4_close = "</h4>";
            string h2_close = "</h2>";
            string par_open = "<p>";
            string par_close = "</p>";

            var informGroup = CMORReportObject.CmorObject.ReportInformlist.GroupBy(r => new { r.success, r.message }).GroupBy(i => i.Key.success);
            string textc = sp;

            textc = "<h2>" + "Envío de Reporte a X.M. E.S.P. " + "</h2>" + sp;

            textc = "<h2>" + "Fronteras reportadas: " + CMORReportObject.CmorObject.ReportList.Count + "</h2>" + sp;
            foreach (var igroup in informGroup)
            {
                if (igroup.Key == "Error" || igroup.Key == "False")
                {
                    c = 1;
                    textc += h1red_open + "ERROR:" + h1_close + sp;

                    foreach (var igroupinfo in igroup)
                    {
                        textc += h4red_open + c++ + ". " + igroupinfo.Key.message + h4_close + sp;
                        textc += par_open + "Fronteras con este error: " + igroupinfo.Count() + par_close + sp;
                        textc += EnvioXM.BoundaryTableHTML(igroupinfo.ToList(), lightredcolor, whitecolor);
                    }
                }
                if (igroup.Key == "Warning")
                {
                    c = 1;
                    textc += h1red_open + "Advertencia:" + h1_close + sp;

                    foreach (var igroupinfo in igroup)
                    {
                        textc += h4red_open + c++ + ". " + igroupinfo.Key.message + h4_close + sp;
                        textc += par_open + "Fronteras con esta advertencia: " + igroupinfo.Count() + par_close + sp;
                        textc += EnvioXM.BoundaryTableHTML(igroupinfo.ToList(), lightredcolor, whitecolor);
                    }
                }

                if (igroup.Key == "Success" || igroup.Key == "True")
                {
                    c = 1;
                    textc += h2green_open + "Fronteras reportadas exitosamente: " + igroup.Where(i => i.Key.success == "Success").First().Count() + h2_close + sp;
                }
            }

            textc += sp + sp + sp + sp + sp + sp + sp;
            textc += "<img style=\"width:400px; height: auto; text-align:left\" src=\"http://oi65.tinypic.com/kds46g.jpg\" />" + sp;

            return textc;
        }

        public static string BoundaryTableHTML(List<Report_Inform> informs, string colorHeader, string fontColor)
        {
            string HTMLstring = sp;
            string htmlHeaderRowStart = "<tr style = \"background-color:"+ colorHeader + "; color:"+ fontColor +";\">";
            string columnStart = "<td style = \"border: 1px solid #aaaaaa; text-align:left; padding: 8px\">";
            string headerColumnStart = "<th style =\"border:1px solid #aaaaaa; text-align: left ;padding:10px\">";
            string headerColumnEnd = "</th>";
            string columnEnd = "</td>";
            string rowStart = "<tr>";
            string rowEnd = "</tr>";

            HTMLstring += "<table style=\"border-collapse:collapse;  width:100%\" > " + sp +
            "<thead>" +
            htmlHeaderRowStart +
            headerColumnStart + "No. " + headerColumnEnd +
            headerColumnStart + "Nombre " + headerColumnEnd +
            headerColumnStart + "Código SIC" + headerColumnEnd +
            rowEnd +
            "</thead>" +
            "<tbody>";
            int conta = 1;
            foreach (var item in informs)
            {
                HTMLstring += rowStart +
                columnStart + conta++ + columnEnd +
                columnStart + CMORReportObject.CmorObject.BoundaryList.Where(b => b.id == item.boundary_id).Select(b => b.name).First() + columnEnd +
                columnStart + CMORReportObject.CmorObject.BoundaryList.Where(b => b.id == item.boundary_id).Select(b => b.id).First() + columnEnd +
                rowEnd;
            };
            HTMLstring += "</tbody>"+
           "</table>" + sp + sp;
            return HTMLstring;
        }


    }

    
}