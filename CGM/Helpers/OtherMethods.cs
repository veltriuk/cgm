﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CGM.Helpers
{
    public class OtherMethods
    {
        public static DateTime getboxdate(string fechabox) //Por defecto añade una hora a la conversión Datetime
        {
            string[] fecha = fechabox.Split('.');
            DateTime Fechabox = (new DateTime(Convert.ToInt32(fecha[0]), Convert.ToInt32(fecha[1]), Convert.ToInt32(fecha[2]))).AddHours(1);
            return Fechabox;
        }

        public static string objectToJSON(Object obj)
        {
            string json = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            return json;
        }

        /// <summary>
        /// Performs a mapping between a list of jObjects to another. The original has elements "name" and "value".
        /// The output has the "name" parameter as the dictionary key, and the "value" as the value of it.
        /// </summary>
        /// <param name="jObjectsInput">Deserialized html json form, into List<jObect></param>
        /// <returns></returns>
        public static Dictionary<string, string> htmlJsonFormToJobjectList(List<JObject> jObjectsInput)
        {
            // Options in this object:
            // name: compareTo, value: 0 (Valor máximo), 1 (Curva Típica)
            // name: excessValuePick, value: 0 (Valor fijo) , 1 (Porcentaje (%))
            // name: excessValue, value: num

            var parametersDict = new Dictionary<string, string>();
            
            foreach (var group in jObjectsInput.GroupBy(n => n.Values().First().ToString()).ToList()) // In case of múltiple elements with the same name, that can happen when editing the mail list in Alarms
            {
                if (group.Count() == 1) // Este es el caso normal
                {
                    parametersDict[group.First()["name"].ToString()] = group.First()["value"].ToString();
                }
                else // Este es el caso excepcional cuando se presenta más de un elemento por nombre 
                {
                    parametersDict[group.Key] = string.Join(",",group.Select(n => n.Values().Last().ToString()).ToArray());
                }
            }

            return parametersDict;
        }

        public static Dictionary<string, string> JObjectStringToDict(string json_string)
        {
            return htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(json_string));
        }

        internal static List<Object> getObjectChangesFromDB(Object objOriginal, Object objChanged)
        {
            //Se compara los objetos ingresados, y devuelve las propiedades que han cambiado
            var props = objOriginal.GetType().GetProperties();
            List<Object> propList = new List<Object>();
            foreach (var prop in props)
            {
                object original = prop.GetValue(objOriginal);
                object current = prop.GetValue(objChanged);

                if (original != null && current != null)
                {
                    if (original.ToString() != current.ToString())
                    {
                        propList.Add(new { propiedad = prop.Name, original = original.ToString(), nuevo = current.ToString() });
                    }
                }
                else if ((original == null && current != null) || (original != null && current == null))
                {
                    propList.Add(new { propiedad = prop.Name, original = original == null ? "" : original.ToString(), nuevo = current == null ? "" : current.ToString() });
                }
            }
            return propList;
        }
        /// <summary>
        /// Genera la expresión en Cron del timespan ingresado, para tareas diarias solamente.
        /// </summary>
        public static string TimespanToCron(TimeSpan ts)
        {
            string minutes;
            if (ts.Minutes != 59)
                minutes = (ts.Minutes + 1).ToString(); //Se agrega un minuto a la instrucción, para que el programa se realice para el día siguiente.
            else minutes = ts.Minutes.ToString();
            string cr = minutes +" "+ ts.Hours.ToString() + " * * *";
            return cr;
        }

        public static string CreateUrlWithParameters(string baseUrl, Dictionary<string, string> parameters)
        {
            string urlParams = baseUrl + "/?";
            string value = "";
            foreach (KeyValuePair<string, string> param in parameters)
            {
                value = param.Value == null ? "0" : param.Value;
                urlParams += param.Key + "=" + value + "&";
            }
            return urlParams;
        }

        internal static double toJavaScriptDate(DateTime datetime)
        {
            double datejs = datetime
                           .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                           .TotalMilliseconds;

            return datejs;
        }
    }
}