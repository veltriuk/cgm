﻿using CGM.Controllers.CGM;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using CGM.XMReportService;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CGM.Helpers
{
    public partial class MarketReport
    {
        /// <summary>
        /// Get measurements from query parameters
        /// </summary>
        /// <param name="boundariesids"></param>
        /// <param name="report_type_id"></param>
        /// <param name="opdate"></param>
        /// <returns></returns>
        public static List<VMeasure> getMeasuresReport(int[] boundariesids, DateTime opdate, int quantity_id)//, int report_type_id)
        {
            List<VMeasure> listToReport = new List<VMeasure>();
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                //var quantity_id = db.Acquisition_Item.Find(acq_item_id).quantity_id;
                List<Boundary> boundaries = db.Boundary.ToList();

                listToReport = db.VMeasure.AsNoTracking().Where(v => v.assignedReading.Value
                                                              && boundariesids.Contains(v.id)
                                                              //&& v.report_type_id == report_type_id //Podría estarse retirando. No tiene mucho sentido, pues con el código de la frontera es redundante.
                                                              && v.is_active
                                                              && v.operation_date.CompareTo(opdate.Date) == 0
                                                              && v.quantity_id == quantity_id).ToList(); // 1 = Energía activa positiva


                // Jan-3-2020
                // Puede existir el caso de que en la adquisición de los datos, por alguna razón, se haya incluído dos conjuntos de 24 medidas para la misma frontera, al mismo tiempo activos.
                // Este caso es muy raro, y no debería llegar aquí nunca. Pero pasó. Entonces le vamos a dar prioridad a los últimos datos que se hayan adquirido.

                // Se agrupan por código y respaldo, y debería, por cada grupo, haber sólamente 24 medidas. 
                // Si tiene más, se eliminan las más viejas, y en la base de datos se las marca como !assigned

                var groups = listToReport.GroupBy(m => new Tuple<int, bool?>(m.id, m.is_backup)).Where(g => g.Count() > 24).ToList();
                if (groups.Count > 0)
                {
                    try
                    {
                        List<Report_Inform> risToUnassignAccum = new List<Report_Inform>();

                        foreach (var item in groups)
                        {
                            // Get the report_informs in conflict. The idea is to leave only one: the most recent.
                            var reportInformsInConflict = db.Report_Inform.Where(r => r.boundary_id.Value == item.Key.Item1
                                                                                      && r.assignedReading.Value
                                                                                      && r.Report.operation_date.Value.CompareTo(opdate.Date) == 0).ToList();

                            if (reportInformsInConflict.Count == 1) // Esto significa que tiene Measure_groups repetidos, por alguna razón, y se debe eliminar los duplicados
                            {
                                var measure_groups = reportInformsInConflict.First().Measure_Groups;
                                foreach (var measure_group in measure_groups.GroupBy(m => m.acquisition_item_id))
                                {
                                    // Debe haber sólo un elemento por adquisition_item_it. Si hay más, se deja sólo uno
                                    if (measure_group.Count() > 1)
                                    {
                                        foreach (var group_to_delete in measure_group.Skip(1))
                                        {
                                            db.Measure_Group.Remove(group_to_delete);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Get the one that we will leave assigned.
                                var riMostRecent = reportInformsInConflict.OrderByDescending(r => r.end_time).First();

                                // Get the report_informs older, to mark them as not assigned.
                                List<Report_Inform> risToUnassign = reportInformsInConflict.Where(r => r.id != riMostRecent.id).ToList();
                                risToUnassign.ForEach(r => r.assignedReading = false);
                            }

                            db.SaveChanges();
                        }


                        // Lastimosamente, hay que volver a realizar la consulta para observar los datos limpiados. Entonces arrojamos un error.
                        throw new CGMResultException("Se realizó una limpieza de datos. Favor volver a realizar la consulta.");

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }


            }
            return listToReport;
        }
    }
}