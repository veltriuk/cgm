﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Helpers
{
    public class CGMResultException :Exception
    {
        public CGMResultException(string message) //Se crea para devolver una excepción en Hangfire, de modo que los trabajo vulvan a ser creados.
            : base(message)
        {

        }
    }
}