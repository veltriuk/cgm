﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace CGM.Helpers.Attributes
{
    /// <summary>
    /// It is used to update static filters that must not be erased nor edited.
    /// </summary>
    public class UpdateDefaultAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            updateFilter(1); //Comercialización
            updateFilter(0); //Generación
        }

        private void updateFilter(int report_type)
        {
            CGM_DB_Entities db = new CGM_DB_Entities();
            List<Boundary> boundaries = db.Boundary.Where(b => b.report_type_id == report_type && !b.is_filed ).ToList();
            Filtro filtro = new Filtro();
            if (report_type == 1)
            {
                filtro = db.Filtro.Include(f => f.Boundaries).Where(f => f.Name == "Comercialización").FirstOrDefault();
            }
            if (report_type == 0)
            {
                filtro = db.Filtro.Include(f => f.Boundaries).Where(f => f.Name == "Generación").FirstOrDefault();
            }
            List<Boundary> boundFiltro = filtro.Boundaries.ToList();

            var add = boundaries.Except(boundFiltro);
            var remove = boundFiltro.Except(boundaries).Select(b => b.id);

            filtro.Boundaries.RemoveAll(b => remove.Any(bb => bb == b.id));
            filtro.Boundaries.AddRange(add);

            db.Entry(filtro).State = EntityState.Modified;
            db.SaveChangesAsync();
        }
    }
}