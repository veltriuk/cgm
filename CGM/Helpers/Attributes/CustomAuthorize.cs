﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CGM.Helpers.Attributes
{
    //Class created to handle unauthorized requests. Basically to redirect these requests.
    public class CustomAuthorize : AuthorizeAttribute
    {
        //private bool _failedRoleValidation;

        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    //Check we have a valid HttpContext
        //    if (httpContext == null)
        //        throw new ArgumentNullException("httpContext");

        //    if (!httpContext.User.Identity.IsAuthenticated)
        //        return false;

        //    CGM_DB_Entities context = new CGM_DB_Entities(); // my entity  

        //    var roles = Roles.Split(',');

        //    foreach (var role in roles)
        //    {
        //        if (httpContext.User.IsInRole(role))
        //        {
        //            return true;
        //        }
        //    }

        //    //User is not in one of the applicable roles
        //    _failedRoleValidation = true;

        //    return false;
        //}


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.AuthorizeCore(filterContext.HttpContext))
            {
                base.OnAuthorization(filterContext);
                // Get the username and role name to display in the front page.
                USER.USERNAME = filterContext.HttpContext.User.Identity.Name;
                foreach (var role in USER.ROLES)
                {
                    if (filterContext.HttpContext.User.IsInRole(role))
                    {
                        USER.ROLE = role;
                        break;
                    }
                    
                }
            }
            else
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                                       new
                                       {
                                           controller = "LogUser",
                                           action = "Index",
                                           area = "",
                                           returnUrl = filterContext.HttpContext.Request.Url.AbsolutePath
                                       }));
            }

            else {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                                          new { controller = "LogUser",
                                                area = "",
                                                action = "NotAuthorized"
                                          }));
            }
        }
    }
}