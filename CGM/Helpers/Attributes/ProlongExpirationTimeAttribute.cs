﻿using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;
using System;

namespace CGM.Helpers.Attributes
{
    public class ProlongExpirationTimeAttribute : JobFilterAttribute, IApplyStateFilter //Apply attribute to extend the days of cleaning of the jobs.
    {
        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromDays(7);
        }
        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromDays(7);
        }
    }
}