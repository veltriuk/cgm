﻿using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using System;
using System.Data.Entity;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace CGM.Helpers
{
    /// <summary>
    /// Provides methods to handle data in spreadsheets with single title rows, with some particular cases to deal with objects of the dataset CGM_DB (Answers, Questions, Meters).
    /// In general, Class 0 is AswerOption, 1 is Questions. In this case, Class 1 is associated with the Questions with radio button, but return an AnswerOption anyway (RadioButtons are so messy, and didn't see it coming)
    /// </summary>
    public class Cells2Data
    {
        public int Class { get; set; }
        public List<ParamQuestion> questionsGeneral { get; set; }
        public List<Meter> metersOriginal { get; set; }
        public List<Model_Meter> modelsOriginal { get; set; }
        public List<Boundary> boundariesOriginal { get; set; }
        public List<ParamAnswerOption> answerOptions { get; set; }
        /// <summary>
        /// Column index to ID of AnswerOption (Class0) or Question (Class1)
        /// </summary>
        public Dictionary<int, int> index2Id { get; set; }
        public string[] colNames { get; set; }
        public Dictionary<string, int> colName2Index { get; set; }

        public Tuple<string, object> getObject(int colIndex, string cellValue)
        {
            if (Class == 0) // AnswerOption which is not a radio button
            {
                ParamAnswer answer = new ParamAnswer();
                answer.answerOption_id = index2Id[colIndex];
                answer.value = cellValue;
                return new Tuple<string, object>("true", answer);
            }
            if (Class == 1) // AnswerOption which IS radio button. Does not return Question
            {
                using (CGM_DB_Entities db = new CGM_DB_Entities())
                {
                    ParamAnswer answer = new ParamAnswer();
                    // Get the answer Id associated with that question, which is the value itself.
                    //answer.answerOption_id = questionsGeneral.Where(q => q.index == index2Id[colIndex]).Where(a => a.name == cellValue).FirstOrDefault().id; // I don't know why this does not work
                    var idAnsInCell = index2Id[colIndex];
                    var ans = db.ParamQuestions.Where(q => q.id == idAnsInCell).FirstOrDefault().AnswerOptions // From the question with that id, them get the id of the value in the cell
                                              .Where(a => RemoveDiacriticsLower(a.name).Equals(RemoveDiacriticsLower(cellValue))).FirstOrDefault();
                    if (ans != null)
                    {
                        answer.answerOption_id = ans.id;
                        return new Tuple<string, object>("true", answer);
                    }
                    else
                    {
                        throw new Exception("[CGM] Objeto en celda no encontrado. Verifique que los datos en la celda tienen la misma escritura que los datos en el formulario. Columna: " + colIndex + ".");
                    }
                }

            }
            return null;
        }
        public Tuple<string, object> getObject(string[] lineItems)
        {
            if (Class == 5)
            {
                try
                {
                    using (CGM_DB_Entities db = new CGM_DB_Entities())
                    {

                        string flag = "new"; // Retrun flag to identify which kind of object we are generating
                        string serial = lineItems[colName2Index[RemoveDiacriticsLower("SERIE")]];

                        if (serial == "")
                            throw new Exception("El serial del medidor no puede ser un espacio vacío. Verifique que este campo se encuentre lleno para todos los elementos.");

                        Meter meter = metersOriginal.FirstOrDefault(m => m.serial.Trim().Equals(serial));

                        if (meter == null)
                        {
                            var codeInCell = lineItems[colName2Index[RemoveDiacriticsLower("SIC")]].ToLower();
                            
                            meter = new Meter()
                            {
                                FormConfiguration = new ParamFormInstance() { paramForm_id = 1 },
                                serial = serial,
                            };

                            var boundary = db.Boundary.Where(b => b.code.ToLower().Equals(codeInCell)).FirstOrDefault();
                            if (boundary != null) //no boundary attached if null
                            {
                                bool? isBackup = false;
                                try
                                {
                                    isBackup = lineItems[colName2Index[RemoveDiacriticsLower("RESPALDO")]] == RemoveDiacritics("P") ? false : true;
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message == "The given key was not present in the dictionary.")
                                    {
                                        throw new Exception("No se encontró la columna RESPALDO. Esta es requerida para la lectura de los medidores asociados a una frontera.");
                                    }
                                }
                                meter.Boundary = boundary;
                                meter.boundary_id = boundary.id;
                                meter.is_backup = isBackup;
                            }

                            string modelo = lineItems[colName2Index[RemoveDiacriticsLower("MODELO")]];

                            // gets the model from the table Model_Meter directly
                            meter.Model_Meter = modelsOriginal.FirstOrDefault(m => m.model == modelo);
                            if (meter.Model_Meter == null)
                               throw new Exception("Problemas leyendo el modelo de medidor en el archivo. Serial: " + meter.serial + ". Verifique que el nombre del medidor corresponda a un nombre válido en la plataforma.");
                            else
                                meter.model_meter_id = meter.Model_Meter.id; // somehow avoids to insert new models
                        }

                        else
                        {
                            flag = "existing";
                        }
                        return new Tuple<string, object>(flag, meter);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return null;
        }

        public Cells2Data(int Class, string[] fileCols, string[] colNamesAim = null)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                // Dictionary<int, int> index2AnswerId = db.ParamAnswerOption.Where(a => a.type_id != 4 && fileCols.Contains(a.name)).ToDictionary(a => Array.IndexOf(fileCols, a.name), a => a.id); // Single line query
                this.Class = Class;

                // Iterate over the name of the columns, and add to the dictionary the found columns
                int i = 0;
                if (Class == 0) // AnswerOptions
                {
                    Dictionary<int, int> index2AnswerId = new Dictionary<int, int>();
                    answerOptions = db.ParamAnswerOptions.Where(a => a.type_id != 4).ToList(); // Ignore radio buttons.
                    foreach (var fileCol in fileCols)
                    {
                        // get answer id where the name is the same of the column name
                        var answerOption = answerOptions.Where(a => String.Compare(fileCol, a.name, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0) // Compare ignoring case and accents (ex HEllo, héLLo are equal)
                                                               .FirstOrDefault();
                        if (answerOption != null)
                            index2AnswerId.Add(i, answerOption.id);
                        i++;
                    }
                    index2Id = index2AnswerId;

                }
                if (Class == 1) // Questions
                {
                    questionsGeneral = db.ParamQuestions.Where(q => q.Group.paramForm_id == 1).ToList(); //1:MeterConfiguration
                    answerOptions = db.ParamAnswerOptions.Where(a => a.type_id == 4).ToList(); // Scope radio buttons.

                    Dictionary<int, int> index2QuestionId = new Dictionary<int, int>();

                    var questions = db.ParamQuestions.Where(a => a.type_id == 6).ToList(); // Only radio buttons
                    foreach (var fileCol in fileCols)
                    {
                        // get answer id where the name is the same of the column name
                        var question = questions.Where(q => String.Compare(fileCol, q.name, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0) // Compare ignoring case and accents (ex HEllo, héLLo are equal)
                                                               .FirstOrDefault();
                        if (question != null)
                            index2QuestionId.Add(i, question.id);
                        i++;
                    }
                    index2Id = index2QuestionId;
                }
                if (Class == 5) // Arbitrarily assigned class 5 to Meters
                {
                    colNames = colNamesAim; // We will look for these columns. We should have a template to download and see these structure.
                    db.Configuration.ProxyCreationEnabled = false;
                    metersOriginal = db.Meter.Include(m => m.Model_Meter).Include(m => m.Boundary).Where(m => !m.is_filed).ToList();
                    modelsOriginal = db.Model_Meter.ToList();
                    boundariesOriginal = db.Boundary.ToList();
                    colName2Index = getColName2Index(colNames, fileCols);
                }
            }

        }

        /// <summary>
        /// Maps the relevant column names with the indexes, with flexibility in spelling.
        /// </summary>
        /// <param name="colNames">Names of columns to search</param>
        /// <param name="fileCols">List of columns names in spreadsheet (Usually the first row)</param>
        /// <returns>Returns the dictionary between column names in spreadsheet with the indexes</returns>
        public Dictionary<string, int> getColName2Index(string[] colNames, string[] fileCols)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            // Single row approach. Does not use it since we cannot compare the strings.
            // colName2Index = colNames.Select(s => new { colName = s, index = Array.IndexOf(fileCols, s) }).ToDictionary(s => s.colName, s => s.index);
            int i = 0;
            foreach (var fileCol in fileCols)
            {
                var _fileCol = RemoveDiacriticsLower(fileCol).Replace("\0", ""); // Format string removing diacritics (accents) and putting in lowercase. /0 in casse of unicode
                foreach (var colName in colNames)
                {
                    var _colName = RemoveDiacriticsLower(colName);
                    // Compare aimColumns with the columns in the file, ignoring case and accents. If they match, add to dictionry.
                    if (_colName.Equals(_fileCol))
                    {
                        if (!dict.ContainsKey(_colName))
                            dict.Add(RemoveDiacriticsLower(_colName), i);
                    }
                }
                i++;
            }
            return dict;
        }
        public string RemoveDiacritics(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }
        public string RemoveDiacriticsLower(string text)
        {
            return RemoveDiacritics(text).ToLower();
        }
    }
}