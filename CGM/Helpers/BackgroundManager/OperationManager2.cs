﻿using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Helpers.BackgroundManager
{
    /// <summary>
    /// This partial section refers to the background methods of the different configurations of orders
    /// </summary>
    public partial class OperationManager
    {
        /// <summary>
        /// This method is called everytime a multiple program for acquisition is triggered
        /// </summary>
        /// <param name="id">Program Id</param>
        /// <returns></returns>
        public static string RegInforms_AcqMulti(int id)
        {
            Tuple<string, string, Report> reportResult = registerReport(id, null, true);
            if (reportResult.Item1 == "false") // No se agregará ninguna frontera.
            {
                return reportResult.Item2;
            }

            KeyValuePair<string, string> scheduleResult = scheduleInforms(reportResult.Item3);

            return scheduleResult.Key + scheduleResult.Value;
        }

        /// <summary>
        /// Método de reporte automático al mercado XM. Sólo se reporta energía activa.
        /// </summary>
        /// <param name="id">Identificador del programa</param>
        /// <returns></returns>
        public static string RegInforms_ReportMulti(int id)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                Program program = db.Program.Find(id);
                var boundariesids = program.Filtro.Boundaries.Select(b => b.id).ToArray();

                DateTime dt = DateTime.Now.AddDays(-program.daysBack.Value); // 1 = ayer, 2 = antier y así

                // Get data to report
                // Hay que verificar que hay 24 medidas por cada una de las fronteras en esta petición
                List<VMeasure> listToReport = MarketReport.getMeasuresReport(boundariesids, dt, 1);//, program.Filtro.market_id.Value);
                List<VBoundary> vBoundaries = VBoundary.ToVBoundaries(listToReport);

                MarketReport mr = new MarketReport();
                Tuple<string, Report> res = mr.ReportToMarket(vBoundaries);

                return res.Item1;

            }
        }
    }
}