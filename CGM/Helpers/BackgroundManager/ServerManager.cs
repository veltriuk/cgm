﻿using CGM.Models.CGMModels;
using Hangfire;
using Hangfire.Storage;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CGM.Helpers.BackgroundManager
{
    public class ServerManager
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static BackgroundJobServer staticServer;
        public static BackgroundJobServerOptions GetBackgroundJobServerOptions()
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {

                return new BackgroundJobServerOptions
                {
                    ServerName = String.Format("{0}.{1}",Environment.MachineName,Guid.NewGuid().ToString()),
                    WorkerCount = (int)db.Config_CM.Where(config => config.element.Trim().Equals("ProcesadoresSimultaneos")).FirstOrDefault().value.Value,
                    Queues = new[] { "4", "3", "2", "default" } //4:Muy alta ... default: Baja 
                };
            }
        }

        public static IList<Hangfire.Storage.Monitoring.ServerDto> GetServers()
        {
            IMonitoringApi monitoringApi = JobStorage.Current.GetMonitoringApi();
            return monitoringApi.Servers();
        }
        internal static string DisposeServers()
        {
            try
            {
                //var type = Type.GetType("Hangfire.AppBuilderExtensions, Hangfire.Core", throwOnError: false);
                //if (type == null) return "";

                //var field = type.GetField("Servers", BindingFlags.Static | BindingFlags.NonPublic);
                //if (field == null) return "";

                //var value = field.GetValue(null) as ConcurrentBag<BackgroundJobServer>;
                //if (value == null) return "";

                //var servers2 = value.ToArray();

                staticServer.SendStop();
                //var servers = GetServers();

                //if (servers.Count() == 0)
                //{
                //    return "ok";
                //}
                //foreach (var ser in servers)
                //{
                //    // Dispose method is a blocking one. It's better to send stop
                //    // signals first, to let them stop at once, instead of one by one.
                //    JobStorage.Current.GetConnection().RemoveServer(ser.Name);
                //}
                return "ok";
            }
            catch (Exception ex)
            {
                return "Se presentó un error en la detención del servidor de procesos.";
            } 
        }
        /// <summary>
        /// Reinicia el servidor, dependiendo de la fuente de la orden
        /// </summary>
        /// <param name="from">0:En el inicio; 1:Desde Monitor</param>
        /// <returns></returns>
        internal static string RestartServer(int from) 
        {
            var servers = GetServers();

            if (servers.Count > 0) //Se asegura que solamente se esté operando con un solo servidor.
            {
                if (from == 0) //Desde el inicio. Desde Monitor no se deben matar los procesos
                {
                    foreach (var ser in servers)
                    {
                        // Dispose method is a blocking one. It's better to send stop
                        // signals first, to let them stop at once, instead of one by one.
                        JobStorage.Current.GetConnection().RemoveServer(ser.Name); //Este método interrumpe las operaciones, y es aconsejable solo si no hay trabajos procesándose
                    }
                }
                else if (from == 1)
                {
                    return "El servidor ya se encuentra en funcionamiento.";
                }
            }
            try
            {
                staticServer = new BackgroundJobServer(GetBackgroundJobServerOptions());
                return "ok";
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Error al reiniciar el servidor");
                return "Se presentó un error al reiniciar el servidor.";
            }
        }
    }
}