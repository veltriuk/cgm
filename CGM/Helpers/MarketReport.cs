﻿using CGM.Controllers.CGM;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using CGM.XMReportService;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CGM.Helpers
{
    public partial class MarketReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public UserData userData { get; set; }
        public static string ServiceURL { get; set; }

        public MarketReport()
        {
            getXMConfigData();
        }

        /// <summary>
        /// Main method to report to the XM market.
        /// </summary>
        /// <param name="vBoundaries"></param>
        /// <returns></returns>
        public Tuple<string,Report> ReportToMarket(List<VBoundary> vBoundaries)
        {
            try
            {
                Config_CM testReport;
                Report report;
                using (CGM_DB_Entities db = new CGM_DB_Entities())
                {
                    testReport = db.Config_CM.Where(c => c.element.Equals("toyreports")).FirstOrDefault();
                }
                if (!testReport.boolean.Value)
                {
                    ProcessRequestResult result = SendReportToXM(vBoundaries);
                    ReportReadingProcessResult ProcessResult = GetProcessResult(result);
                    report = ToReport(vBoundaries, ProcessResult, result);
                }
                else
                {
                    report = ToReportTest(vBoundaries, testReport.value.Value);
                }
                DataAndDB.insertReports(report);
                return new Tuple<string, Report>("Carga de reporte cargada con éxito.", report);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error trying to send the report. Check the Stacktrace to see the source.");
                return new Tuple<string, Report>(ex.Message.ToString() + ". Probablemente sea un error interno del servidor. Notifique a los administradores.", null);
            }
        }

        public ProcessRequestResult SendReportToXM(List<VBoundary> vBoundaries)
        {
            List<ReadingReportItem> listResult = new List<ReadingReportItem>();  // Se crea la lista inicial para incluir todas las Fronteras
            ReadingReportServiceClient client = new ReadingReportServiceClient("BasicHttpsBinding_IReadingReportService", ServiceURL); //Inicialización del cliente , se ingresa la URL del servicio
            ReadingReportItem XMReport = new ReadingReportItem();
            int C = 0;

            foreach (VBoundary vb in vBoundaries)
            {
                try
                {
                    listResult.Add(new ReadingReportItem()
                    {
                        IsBackup = vb.is_backup,
                        ReadingCount = 24,
                        ReadingInterval = 60,
                        StartDate = vb.operation_date, //Fecha de operación
                        BorderCode = vb.code.First() + vb.code.Substring(1).ToLower(), //Inserta código de frontera
                        Readings = Array.ConvertAll(vb.VMeasures.ToArray(), x => x.measure)
                    });
                    C++;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            try
            {
                ProcessRequestResult result = client.ReportReadings(listResult.ToArray(), userData);
                // Si el resultado contiene un mensaje de error ocurrió algun problema y no fue posible procesar las lecturas.
                if (result == null)
                    throw new Exception(string.Format("Resultado de proceso es nulo. Hay un problema con la conexión al servicio web."));
                if (!string.IsNullOrEmpty(result.ErrorMessage))
                    throw new Exception(string.Format("Error enviando lecturas : {0}", result.ErrorMessage));

                return result;
            }
            catch (Exception ex)
            {
               throw ex;
            }
            finally
            {
                client.Close();
            }
        }

        private ReportReadingProcessResult GetProcessResult(ProcessRequestResult result)
        {
            using (ReadingReportServiceClient client = new ReadingReportServiceClient("BasicHttpsBinding_IReadingReportService", ServiceURL))
            {
                ReportReadingProcessResult ProcessResult = new ReportReadingProcessResult();
                bool ban = false;
                while (ban == false)
                {
                    try //a veces llega nulo el resultado de proceso, y se produce error
                    {
                        do //Se pide la respuesta hasta que llegue, pues no es inmediata
                        {
                            ProcessResult = client.GetProcessResult(result.ProcessId, userData);
                        } while (ProcessResult.Results.Count() == 0);
                        ban = true;
                    }
                    catch (Exception ex)
                    {
                        ban = false;
                    }
                }
                client.Close();
                return ProcessResult;
            }
        }
        
        /// <summary>
        /// Para reportes al mercado. Se usa ya cuando se va a hacer el reporte y se quiere guardar la información.
        /// </summary>
        /// <param name="vBoundaries"></param>
        /// <returns></returns>
        public Report ToReport(List<VBoundary> vBoundaries, ReportReadingProcessResult processResult, ProcessRequestResult result)
        {
            string g = processResult.Results.Any(r => r.ErrorMessage != null) ? "Error" : "Success";
            Report report = new Report()
            {
                destination_id = 2, //Reporte al mercado
                market_id = vBoundaries.FirstOrDefault().report_type_id,
                operation_date = vBoundaries.FirstOrDefault().operation_date,
                is_active = false,
                is_auto = false,
                status_id = getStatusId(processResult.ResultFlag.ToString()),
                external_report_id = result.ProcessId.ToString(),
                date_and_time_to_report_end = DateTime.Now,
            };

            List<Report_Inform> ris = new List<Report_Inform>();
            //var userName = System.Web.HttpContext.Current.User.Identity.Name;
            foreach (var r in processResult.Results)
            {
                VBoundary vb = vBoundaries.Where(v => v.code == r.Code).FirstOrDefault();
                Report_Inform ri = new Report_Inform()
                {
                    is_backup = vb.is_backup,
                    boundary_id = vb.id,
                    substatus_id = getSubStatusReport(getStatusId(r.ResultFlag.ToString())),
                    message = r.ErrorMessage,
                };
                List<Measure> lm = new List<Measure>();
                foreach (var item in vb.VMeasures)
                    lm.Add(new Measure { measure_date = item.hour, measure1 = item.measure });
                ri.Measure_Groups = new List<Measure_Group> { new Measure_Group { Measures = lm } };
                ris.Add(ri);
            }
            
            report.Report_Informs = ris;
            return report;
        }

        private int? getSubStatusReport(int status_id)
        {
            int substatus = 0;
            switch (status_id)
            {
                case 3: substatus = 350; break; 
                case 4: substatus = 450; break; 
                case 5: substatus = 550; break; 
                default:
                    break;
            }
            return substatus;
        }

        public Report ToReportTest(List<VBoundary> vBoundaries, double outcome) //outcome: 3: Error, 4: Advertencia, 5: Success
        {
            string outcomeStr;
            switch (outcome)
            {
                case 5: outcomeStr = "Success"; break;
                case 4: outcomeStr = "Warning"; break;
                case 3: outcomeStr = "Error"; break;
                default: outcomeStr = "Success"; break;
            }

            int res = 3;//Default

            Report report = new Report()
            {
                destination_id = 2, //Reporte al mercado
                market_id = vBoundaries.FirstOrDefault().report_type_id,
                operation_date = vBoundaries.FirstOrDefault().operation_date,
                is_active = false,
                is_auto = false,
                status_id = getStatusId(outcomeStr),
                external_report_id = "%%%%%%% Reporte simulado %%%%%%%",
                date_and_time_to_report_end = DateTime.Now

            };

            List<Report_Inform> ris = new List<Report_Inform>();
            //var userName = System.Web.HttpContext.Current.User.Identity.Name;
            foreach (var r in vBoundaries)
            {
                Report_Inform ri = new Report_Inform()
                {
                    is_backup = r.is_backup,
                    boundary_id = r.id,
                    substatus_id = getStatusId(outcomeStr)*100, //To do es de prueba// 300 es error de contraseña, 400 es llamada termina inesperadamente, 500 es exitoso
                    message = outcome == 3 ? "Mensaje de error simulado." : ""
                };
                List<Measure> lm = new List<Measure>();
                foreach (var item in r.VMeasures)
                    lm.Add(new Measure { measure_date = item.hour, measure1 = item.measure });
                ri.Measure_Groups = new List<Measure_Group> { new Measure_Group { Measures = lm } };
                ris.Add(ri);
            }

            report.Report_Informs = ris;
            return report;
        }

        private int getStatusId(string processResultFlagStr)
        {
            switch (processResultFlagStr)
            {
                case "Success":return 5; 
                case "Warning": return 4;
                case "Error": return 3; 
            }
            return 3;
        }
        public void getXMConfigData()
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                List<Config_CM> configList = db.Config_CM.ToList();
                userData = new UserData
                { // Estructura con los datos del usuario
                    Password = configList.Where(c => c.element.Trim() == "WebServicePassword").Select(c => c.text).First().Trim(),
                    UserName = configList.Where(c => c.element.Trim() == "WebServiceUser").Select(c => c.text).First().Trim(),
                };
                ServiceURL = configList.Where(c => c.element.Trim() == "WebServiceURL").Select(c => c.text).First().Trim();
                // MailAddress = configList.Where(c => c.element.Trim() == "CorreoCMOR").Select(c => c.text).First().Trim();
                // MailPassword = configList.Where(c => c.element.Trim() == "PasswordCorreoCGM").Select(c => c.text).First().Trim();
            }
        }
    }
}