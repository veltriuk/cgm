﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Excel = Microsoft.Office.Interop.Excel;

namespace CGM.Helpers
{
    public class FileManagement
    {
        public static void WriteTXT(string text, string file) // (Text,filename -address-)
        {
            using (StreamWriter sw = File.CreateText(file))
            {
                sw.Write(text);
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }
        internal static string InsideFileValidatation(string path)
        {
            NumberFormatInfo provider = new NumberFormatInfo { NumberDecimalSeparator = "." };
            string rta = "";
            string sp = Environment.NewLine;
            var lineas = FileManagement.readFileLines(path);
            if (lineas.Count() != 0)
            {
                int c = 1, ban = 0;
                foreach (string frt in lineas)
                {
                    string[] values = frt.Split('\t');
                    if (values[0].Substring(0, 3).ToLower() != "frt")
                    {
                        rta = "Error en formato. Primer espacio no corresponde al código correcto de una frontera. ";
                        ban = 1;
                    }
                    if (!(values[1] != "P" || values[1] != "R"))
                    {
                        rta = "Error en formato de frontera " + values[0] + ". Segundo espacio puede corresponder solamente a la letra P (Principal) o R (Respaldo). ";
                        ban = 1;
                    }
                    if (values[2] != "0")
                    {
                        rta = "Error en formato de frontera " + values[0] + ". Tercer espacio corresponde solamente al número cero (0) por defecto. ";
                        ban = 1;
                    }
                    for (int i = 0; i < 24; i++)
                    {
                        double medida;
                        try
                        {
                            medida = Convert.ToDouble(values[i + 3].Replace(',', '.'), provider);
                        }
                        catch
                        {
                            rta += "La frontera con código " + values[0] + " tiene un error en la medida de la hora " + i.ToString() +"."+ sp +
                                    "Puede que no haya 24 medidas";
                            ban = 1;
                            break;
                        }
                    }
                    c++;
                    if (ban == 1)
                    {
                        rta += sp + "Tenga en cuenta que: " + sp +
                                    "El primer espacio corresponde al código de frontera" + sp +
                                    "El segundo espacio corresponde a una P (Principal) o a una R (Respaldo)" + sp +
                                    "El tercer espacio corresponde a un cero (0) por defecto" + sp +
                                    "Y en adelante, se disponen las 24 medidas correspondientes a la frontera";
                        return rta;
                    }
                }

                return "OK";
            }
            else
            {
                return "Error en formato - No se leyó ninguna línea";
            }

        }
        public static IEnumerable<string> readFileLines(string path)
        {
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path, System.Text.Encoding.Default);
            List<string> lines = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                try
                {
                    lines.Add(line);
                }
                catch (Exception ex)
                {
                }
            }
            file.Close();
            file.Dispose();
            return lines;
        }
        public static string LoadFileValidation(HttpPostedFileBase cRFile, string CRoAP)
        {

            if (cRFile == null || cRFile.ContentLength == 0)
                return "Favor seleccionar archivo.";
            else
            {
                if (!cRFile.FileName.EndsWith("txt"))
                    return "El archivo tiene un formato incorrecto. Debe seleccionar un archivo con extensión .txt.";
                else
                {
                    string loadfilename = cRFile.FileName;
                    if (loadfilename.Length == 12)
                    {
                        try
                        {
                            if (CRoAP == "CR")
                            {
                                int num1 = Convert.ToInt32(loadfilename.Substring(4, 2));
                                int num2 = Convert.ToInt32(loadfilename.Substring(6, 2));

                                if (loadfilename.Substring(0, 2) != "CR" || num1 > 12 || !(loadfilename.Substring(2, 2) != "25" || loadfilename.Substring(2, 2) != "26"))
                                    return "Formato números incorrecto";
                            }
                            if (CRoAP == "AP")
                            {
                                int num1 = Convert.ToInt32(loadfilename.Substring(2, 5));
                                int num2 = Convert.ToInt32(loadfilename.Substring(6, 2));

                                if (loadfilename.Substring(0, 2) != "AP" || num2 > 12)
                                    return "Formato números incorrecto";
                            }

                            DateTime fecha = getDateFromFileName(cRFile);
                            if (fecha.Date.CompareTo(DateTime.Now.Date) >= 0)
                            {
                                return "La fecha del archivo corresponde al día actual o a una fecha futura.";

                            }
                            int dateCompare = DateTime.Compare(fecha.Date, DateTime.Now.Date.Add(new TimeSpan(-2, 0, 0, 0)));
                            string negocio = cRFile.FileName.Substring(2, 2);

                            //Se realiza una comparacion de la fecha ingresada con la actual, restingiendo el ingreso de datos de fechas anteriores a lo estipulado por la norma
                            if (DateTime.Compare(fecha.Date, DateTime.Now.Date.Add(new TimeSpan(-2, 0, 0, 0))) < 0 && negocio == "26") //Comercialización
                                return "Error: Para registros de comercialización, no se pueden modificar las medidas con fechas anteriores a tres días a la fecha actual.";
                            if (DateTime.Compare(fecha.Date, DateTime.Now.Date.Add(new TimeSpan(-1, 0, 0, 0))) < 0 && negocio == "25") //Comercialización
                                return "Error: Para registros de generación, no se pueden modificar las medidas con fechas anteriores a dos días a la fecha actual.";

                        }
                        catch
                        {
                            return "El formato de fecha es incorrecto. El primer par de números indica el tipo de negocio (25 o 26). " +
                                        "El segundo par indica el mes (menor o igual a 12), y el tercer par indica el día.";
                        }
                    }
                    else
                    {
                        return "Error en el nombre del archivo. Este debe tener 8 caracteres, sin contar la extensión";
                    }
                }
            }
            return "OK";
        }

        /// <summary>
        /// Method to check duplicate entries for a boundary. For example, measures for main meter twice (Might happen).
        /// </summary>
        /// <param name="repElemList"></param>
        /// <returns></returns>
        internal static string checkDuplicates(List<ReportElement> repElemList)
        {
            string rta = "";
            var check = repElemList.GroupBy(r => new { r.boundary.code, r.isBackup }).Where(g => g.Count() > 1); //Misma frontera con dos P, o dos R

            if (check.Count() > 0)
            {

                rta = "Fronteras que contienen entradas duplicadas de registros principales (P) o de respaldo (R): ";
                foreach (var item in check.Select(c => c.Key.code))
                {
                    rta += item + " - ";
                }
                return rta;
            }
            else
            {
                return "OK";
            }
        }
        internal static string checkDuplicates(List<VMeasure> vMeasures)
        {
            string rta = "";
            var check = vMeasures.GroupBy(r => new { r.code, r.is_backup }).Where(g => g.Count() > 24); //Misma frontera con posiblemente dos (o más) P, o dos R

            if (check.Count() > 0)
            {
                rta = "Fronteras que contienen entradas duplicadas de registros principales (P) o de respaldo (R): ";
                foreach (var item in check.Select(c => c.Key.code))
                {
                    rta += item + " - ";
                }
                return rta;
            }
            else
            {
                return "OK";
            }
        }


        public static DateTime getDateFromFileName(HttpPostedFileBase cRFile)
        {
            int mes = Convert.ToInt32(cRFile.FileName.Substring(4, 2));
            int dia = Convert.ToInt32(cRFile.FileName.Substring(6, 2));
            int year = DateTime.Now.Year;

            if (mes == 12 && dia == 31) //Si no se hace esta clarificación, los registros se guardan para el año nuevo siguiente
                year = DateTime.Now.Year - 1;

            DateTime fecha = new DateTime(year, mes, dia);
            return fecha;
        }

    }

    public class ExcelManagement
    {
        public static Array GetDataFromExcel(string filePath, int sheetNum, string rango) {
            var tm = Type.Missing;
            var excelApp = new Excel.Application();

            Excel.Application app = new Excel.Application();
            Excel.Workbook wb = app.Workbooks.Open(filePath, tm, tm, tm, tm, tm, tm, tm, tm, tm, tm, tm, tm, tm, tm);
            Excel.Worksheet sheet = (Excel.Worksheet)wb.Sheets[1];
            Excel.Range excelRange = sheet.UsedRange;

            Array data = ((Array)sheet.get_Range(rango, Type.Missing).Cells.Value2);

            Marshal.FinalReleaseComObject(excelRange);
            Marshal.FinalReleaseComObject(sheet);
            Marshal.FinalReleaseComObject(wb);
            return data;
        }

        public static void GetRange(string range, int u2, int u3, Excel.Worksheet excelWorksheet, out string[] arrayS, out double[] arrayD)
        {
            Excel.Range workingRangeCells =
            excelWorksheet.get_Range(range, Type.Missing);
            Array array = (System.Array)workingRangeCells.Cells.Value2;
            ConvertToStringArray(array, out arrayS, out arrayD);
        }
        public static void ConvertToStringArray(System.Array values, out string[] theArray, out double[] theArrayl)
        {
            // create a new string array
            theArray = new string[values.Length];
            theArrayl = new double[values.Length];
        }

        

    }
}