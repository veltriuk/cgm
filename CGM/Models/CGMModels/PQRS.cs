namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Preguntas - Quejas - Reclamos - Sugerencias
    /// </summary>
    [Table("PQRS")]
    public partial class PQRS
    {
        public int id { get; set; }

        /// <summary>
        /// Usuario que realiza la creaci�n del PQRS. Cabe aclarar que �ualquier usuario puede participar dentro del hilo en los mensajes
        /// </summary>
        [DisplayName("Usuario")]
        [ForeignKey("User")]
        public string user_id { get; set; }

        /// <summary>
        /// Fecha de creaci�n, para referencia.
        /// </summary>
        [DisplayName("Fecha de creaci�n")]
        public DateTime date_created { get; set; }

        /// <summary>
        /// Fecha de cierre del hilo.
        /// </summary>
        [DisplayName("Fecha de cierre")]
        public DateTime? date_closed{ get; set; }

        /// <summary>
        /// True: Resuelto. False: No resuelto.
        /// </summary>
        [DisplayName("Resuelto?")]
        public bool solved { get; set; }

        [DisplayName("T�tulo")]
        public string title { get; set; }
        
        public virtual AspNetUsers User{ get; set; }
        public virtual ICollection<PQRS_Message> PQRS_Messages{ get; set; }
    }
}
