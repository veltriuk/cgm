namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ExceptionLog")]
    public partial class ExceptionLog
    {
        public Guid id { get; set; }

        public string msg { get; set; }

        [StringLength(100)]
        public string type { get; set; }

        public string source { get; set; }

        [StringLength(100)]
        public string url { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string innerExcMsg { get; set; }
    }
}
