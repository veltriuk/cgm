namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("State", Schema = "Hangfire")]
    public partial class JobState
    {
        public int Id { get; set; }
        public int JobId { get; set; }

        public string Name { get; set; }
        public string Reason { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Data { get; set; }
        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }
    }
}
