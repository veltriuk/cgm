namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Boundary")]
    public partial class Boundary
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Boundary()
        {
            Boundary_To_Report = new HashSet<Boundary_To_Report>();
            Measure_AP = new HashSet<Measure_AP>();
            Report_Inform = new HashSet<Report_Inform>();
        }

        public int id { get; set; }

        [StringLength(8)]
        [DisplayName("C�digo")]
        public string code { get; set; }

        [DisplayName("Fuente")]
        public int source_id { get; set; }

        [DisplayName("Negocio")]
        public int report_type_id { get; set; }

        [StringLength(100)]
        [DisplayName("Nombre")]
        public string name { get; set; }

        [StringLength(100)]
        [DisplayName("Nombre interno")]
        public string real_name { get; set; }
        
        [DisplayName("Reporta respaldo")]
        public bool has_backup { get; set; }

        [StringLength(50)]
        [DisplayName("Comentarios")]
        public string commentary { get; set; }

        [DisplayName("Activa")]
        public bool is_active { get; set; }

        [DisplayName("Valor m�ximo activa")]
        public double? max_active { get; set; }
        [DisplayName("Valor m�nimo activa")]
        public double? min_active { get; set; }
        [DisplayName("Valor m�ximo reactiva")]
        public double? max_reactive { get; set; }
        [DisplayName("Valor m�nimo reactiva")]
        public double? min_reactive { get; set; }

        [DisplayName("Prioridad")]
        public int? id_priority { get; set; }

        [DisplayName("Archivada")]
        public bool is_filed { get; set; }

        public float? latitude { get; set; }
        public float? longitude { get; set; }
        //[DisplayName("Medidor principal")]
        //public int? meter_id { get; set; }

        //[DisplayName("Medidor de respaldo")]
        //public int? meter_id2 { get; set; }

        [DisplayName("Fuente")]
        public virtual Boundary_Source Boundary_Source { get; set; }

        [ForeignKey("id_priority")]
        public virtual Priority Priority { get; set; }

        public virtual Report_Type Report_Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Boundary_To_Report> Boundary_To_Report { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Measure_AP> Measure_AP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Report_Inform> Report_Inform { get; set; }

        //[ForeignKey("meter_id")]
        ////[InverseProperty("BoundaryMain")]
        //[DisplayName("Medidor principal")]
        //public virtual Meter MeterMain { get; set; }
        //[ForeignKey("meter_id2")]
        ////[InverseProperty("BoundaryBackup")]
        //[DisplayName("Medidor de respaldo")]
        public virtual List<Meter> Meters { get; set; }
        public virtual List<Filtro> Filtros { get; set; }
    }
}
