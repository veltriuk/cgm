﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.MyModels;

namespace CGM.Models.CGMModels
{
    public class ParamConditionalsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static List<ParamConditionalsFull> conditionsSet;
        // GET: ParamConditionals
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="ParamConditionals";
            
            // Get conditionals from Questions and AnswerOptions
            conditionsSet = ParamConditionalsFull.GetParamConditionalFullSet();

            return View(conditionsSet);
        }

        // GET: ParamConditionals/Delete/5
        public async Task<ActionResult> Delete(int? id, int? condId)
        {
            ViewBag.ControllerName="ParamConditionals";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ParamAnswerOption answerOption = await db.ParamAnswerOptions.FindAsync(id);
            List<ParamConditional> conditionals = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ParamConditional>>(answerOption.conditionals);
            var conditionalToRemove = conditionals.FirstOrDefault(c => c.Id == condId);
            conditionals.Remove(conditionalToRemove);

            answerOption.conditionals = Newtonsoft.Json.JsonConvert.SerializeObject(conditionals);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

