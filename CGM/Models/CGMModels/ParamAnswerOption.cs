namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Opciones de respuesta de las preguntas. Alberga como tal los espacios donde el usuario puede ingresar la informaci�n asociada al interrogante.
    /// </summary>
    [Table("ParamAnswerOption")]
    public partial class ParamAnswerOption
    {
        public int id { get; set; }

        [ForeignKey("Question")]
        public int paramQuestion_id { get; set; }

        /// <summary>
        /// Nombre o definici�n de la pregunta
        /// </summary>
        [DisplayName("Opci�n de respuesta")]
        public string name { get; set; }

        /// <summary>
        /// Alias para codificaci�n interna de la informaci�n. No lo define el usuario sino el desarrollador.
        /// </summary>
        [DisplayName("Alias")]
        public string alias { get; set; }

        /// <summary>
        /// Posible valor de la respuesta. Contiene el valor por defecto, el indicativo del bot�n de radio, etc.
        /// </summary>
        [DisplayName("Valor")]
        public string value { get; set; } // Could be numbers in the case of radio. Gotta convert.

        /// <summary>
        /// Tipo de opci�n. Puede ser bot�n de radio, texto, password, n�mero, etc.
        /// 1:text; 2:Password; 3:Number; 4:Radio; 7:File
        /// </summary>
        [DisplayName("Tipo")]
        [ForeignKey("Type")]
        public int type_id { get; set; }

        [DisplayName("N�mero")]
        public int index { get; set; }
        
        /// <summary>
        /// Conditional clauses, in JSON format.
        /// </summary>
        public string conditionals { get; set; }

        public virtual ParamQuestion Question{ get; set; }
        public virtual ParamType Type{ get; set; }
        public virtual ICollection<ParamAnswer> Answers { get; set; }
    }
}
