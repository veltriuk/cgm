namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Conjunto de preguntas. Dispuesto para organizar los formularios.
    /// </summary>
    [Table("ParamGroup")]
    public partial class ParamGroup
    {
        public int id { get; set; }
        [ForeignKey("Form")]
        public int paramForm_id { get; set; }

        /// <summary>
        /// Nombre del grupo. Puede ser opcional y no mostrarse.
        /// </summary>
        [DisplayName("Nombre")]
        public string name { get; set; }

        [DisplayName("N�mero")]
        public int index { get; set; }

        public virtual ParamForm Form{ get; set; }
        public virtual ICollection<ParamQuestion> Questions{ get; set; }
    }
}
