namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Report_Inform
    {
        public long id { get; set; }
        /// <summary>
        /// Muestra el estado de la adquisici�n. Se debe tener en cuenta el padre del substatus, Status
        /// Status => 0:Pendiente; 1:En proceso; 2:Error; 3:Advertencia; 4:Exitoso 
        /// Substatus => Se mapea en el ID al status, y adem�s tiene una llave for�nea.
        /// 401	4	Llamada termin� inesperadamente
        /// 402	4	El dispositivo no responde
        /// 301	3	Llamada fallida debido a un error de sincronizaci�n de reloj
        /// 403	4	Duraci�n de llamada m�xima excedida
        /// 404	4	Llamada cancelada
        /// 405	4	Conexi�n perdida / L�nea ca�da
        /// 406	4	Tipo de dispositivo diferente
        /// 302	3	Contrase�a inv�lida
        /// 407	4	Problema trayendo datos de uno de los esclavos del condensador
        /// 500	5	Medidas completas
        /// 501	5	Medidas incompletas, no recuperables
        /// 408	4	Error de base de datos interna
        /// 409	4	Error al cargar registros a base de datos principal
        /// </summary>
        
        public int? substatus_id { get; set; }

        [Display(Name = "Prioridad")]
        public int? priority { get; set; }

        [StringLength(1000)]
        public string message { get; set; }
        // Se pens� en asociarse siempre con un evento, pero posiblemente se quite en un futuro.
        public int? event_id { get; set; }

        public int? boundary_id { get; set; }
        public int? meter_id { get; set; }

        public int? reports_id { get; set; }

        /// <summary>
        /// Se usar�a solamente cuando se hacen reportes a XM, de lo contrario ser�a nulo
        /// </summary
        [Display(Name = "Respaldo?")]
        public bool? is_backup { get; set; }

        /// <summary>
        /// Dice cu�ntas veces se ha realizado las llamadas
        /// </summary
        [Display(Name = "Intentos")]
        public int? attempts { get; set; }
        /// <summary>
        /// El tiempo que se demor� en realizarse.
        /// </summary>
        [Display(Name = "Tiempo de realizaci�n")]
        public TimeSpan? time_elapsed { get; set; }
        public int? boundary_source_id { get; set; }
        /// <summary>
        /// Fecha y hora a la que se termin� de realizar el reporte
        /// </summary>
        [Display(Name = "Hora completado")]
        public DateTime? end_time { get; set; }

        [Display(Name = "Hora iniciado")]
        public DateTime? start_time { get; set; }

        public int? job_id { get; set; }

        // Creo que se incluy� para hacer seguimiento a los sucesos respecto a este proceso. Sin embargo todos est�n en cero. 
        // Puede pensar en quitarse
        [ForeignKey("event_id")]
        public virtual Event Event { get; set; }
        [Display(Name = "Frontera")]
        public virtual Boundary Boundary { get; set; }
        public virtual Report Report { get; set; }
        [ForeignKey("meter_id")]
        [Display(Name = "Medidor")]
        public virtual Meter Meter { get; set; }
        /// <summary>
        /// S�lo puede haber una por d�a de operaci�n asociada a una frontera.
        /// Se lo usa en caso de tener varias adquisiciones y se requiere escoger cu�l es la m�s conveniente.
        /// </summary>
        public bool? assignedReading { get; set; } //En los reportes al mercado, no tiene mucho sentido, por lo que se deja nullable

        public virtual ICollection<Measure_Group> Measure_Groups { get; set; }
        [ForeignKey("substatus_id")]
        public virtual Substatus Substatus { get; set; }

        [ForeignKey("boundary_source_id")]
        public virtual Boundary_Source Boundary_Source {get;set;}
        [ForeignKey("job_id")]
        public virtual Job Job {get;set;}
    }
}
