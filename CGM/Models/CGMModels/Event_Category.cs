﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
        public partial class Event_Category
        {
            [Key]
            public int id { get; set; }

            [StringLength(30)]
            public string categoryName { get; set; }
            public virtual ICollection<Event> Events{ get; set; }

    }
}