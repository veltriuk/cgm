﻿using CGM.Areas.Telemetry.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    /// <summary>
    /// Instrucción programada
    /// </summary>
    [Table("Program", Schema = "dbo")]
    public partial class Program
    {
        public int id { get; set; }

        /// <summary>
        /// Nombre asignado. 
        /// </summary>
        [StringLength(100),Column(TypeName = "nvarchar")]
        [Display(Name = "Nombre")]
        public string name { get; set; }
        /// <summary>
        /// 0: Adquisición (Sólo medidores), 1: Adquisición (Fronteras), 2: Carga a XM, 3: Lectura sin registro
        /// </summary>
        [Display(Name = "Destination")]
        [ForeignKey("Destination")]
        public int destination_id { get; set; } 

        [Display(Name = "Tiempo de ejecución")]
        public DateTime? execution_time { get; set; }

        /// <summary>
        /// 0: Una sola adquisición; 1: Múltiples adquisiciones (Modo Continuo)
        /// </summary>
        [Display(Name = "Múltiple")]
        public bool multiple { get; set; }

        /// <summary>
        /// Refers to the hour in the day that the instruction will be executed. It's NOT really a frequency!!
        /// </summary>
        [Display(Name = "Frecuencia")]
        public TimeSpan? frequency { get; set; }

        public bool filed { get; set; }

        [ForeignKey("Filtro")]
        public int IdFiltro { get; set; }

        [ForeignKey("Acquisition")]
        public int acquisition_id{ get; set; }
        /// <summary>
        /// Applies for multiple programs. Makes the report of the current day minus this parameter
        /// </summary>
        public int? daysBack { get; set; }
        /// <summary>
        /// It has a value in the case of multiple-instructions programs. It links the Recurrent Job with the program, so it can be edited or deleted afterwards
        /// </summary>
        public int? job_id { get; set; }


        public virtual Filtro Filtro { get; set; }
        public virtual Acquisition Acquisition { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
        public virtual Destination Destination { get; set; }

    }
}