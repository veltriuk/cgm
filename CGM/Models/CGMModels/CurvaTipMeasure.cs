namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CurvaTipMeasure")]
    public partial class CurvaTipMeasure
    {
        public int id { get; set; }
        [DisplayName("Fecha y hora")]
        public DateTime datetime { get; set; }
        [DisplayName("Valor")]
        public double measure { get; set; }
        [ForeignKey("CurvaTipMeasureGroup")]
        public int group_id { get; set; }
        public virtual CurvaTipMeasureGroup CurvaTipMeasureGroup { get; set; }
    }
}
