﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    /// <summary>
    /// Describe información adicional del estado en el que se encuentra el objeto con Status.
    /// Objetos en esta clase pueden ser: Razones de errores, adquisición completa o incompleta, tipo de advertencia, etc.
    /// </summary>
    public class Substatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        public int idStatus { get; set; }
        public string description { get; set; }
        [ForeignKey("idStatus")]
        public virtual Status Status { get; set; }
    }
}