using CGM.Areas.Telemetry.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace CGM.Models.CGMModels
{
    [Table(name:"Report")]
    public partial class Report
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Report()
        {
            Report_Informs = new HashSet<Report_Inform>();
        }

        public int id { get; set; }
        /// <summary>
        /// Usuario que realiza la operaci�n
        /// </summary>
        [Required]
        [StringLength(128)]
        [Display(Name = "Usuario")]
        public string asp_net_users_id { get; set; }
        /// <summary>
        /// Hora y fecha en el que se program�
        /// </summary>
        [Display(Name = "Fecha y hora (programado)")]
        public DateTime? date_and_time_to_report { get; set; }
        public DateTime? date_and_time_to_report_end { get; set; }

        /// <summary>
        /// Hora y fecha en el que se realiz�
        /// </summary>
        [Display(Name = "Fecha y hora (realizado)")]
        public DateTime? date_and_time_report { get; set; }
        /// <summary>
        /// Sigue activo? o ya termin�
        /// </summary>
        [Display(Name = "Activo?")]
        public bool is_active { get; set; }
        /// <summary>
        /// 1 o null:Pendiente; 2:En proceso; 3:Error; 4:Advertencia; 5:Exitoso
        /// </summary>
        [Display(Name = "Estado")]
        public int status_id { get; set; }

        /// <summary>
        /// El tiempo que se demor� en realizarse.
        /// </summary>
        [Display(Name = "Tiempo de realizaci�n")]
        public TimeSpan? time_elapsed { get; set; } 
        /// <summary>
        /// 0:Manual ; 1:Autom�tico
        /// </summary>
        [Display(Name = "Clase de Reporte")]
        public bool is_auto { get; set; } 

        /// <summary>
        /// Si se recibe ID de XM en el reporte.
        /// </summary>
        [StringLength(50)]
        [Display(Name = "ID externo")]
        public string external_report_id { get; set; }

        /// <summary>
        /// 0: Adquisici�n (S�lo medidores), 1: Adquisici�n (Fronteras), 2: Carga a XM, 3: Lectura sin registro
        /// </summary>
        [DisplayName("Destino")]
        [ForeignKey("Destination")]
        public int destination_id { get; set; }

        /// <summary>
        /// D�a de operaci�n
        /// </summary>
        [DisplayName("Fecha de operaci�n")]
        [Column(TypeName = "date")]
        public DateTime? operation_date { get; set; }

        /// <summary>
        /// Tipo de reporte. Aplica s�lo a reportes a XM. Se piensa en quitarlo. 0: Generaci�n, 1: Comercializaci�n.
        /// </summary>
        [DisplayName("Mercado")]
        [ForeignKey("Report_Type")]
        public int? market_id { get; set; }

        /// <summary>
        /// Proporciona el tipo de registro que se va a adquirir
        /// </summary>
        [DisplayName("Adquisici�n")]
        [ForeignKey("Acquisition")]
        public int? acquisition_id { get; set; }
        [DisplayName("Programa")]
        [ForeignKey("Program")]
        public int? program_id { get; set; }

        /// <summary>
        /// 1 o null:Pendiente; 2:En proceso; 3:Error; 4:Advertencia; 5:Exitoso
        /// </summary>
        [ForeignKey("status_id")]
        public virtual Status Status { get; set; } 
        [Display(Name = "Usuario")]
        public virtual AspNetUsers AspNetUsers { get; set; } 
        public virtual Acquisition Acquisition { get; set; }
        public virtual Program Program { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Report_Inform> Report_Informs { get; set; }
        public virtual Destination Destination { get; set; }
        public virtual Report_Type Report_Type { get; set; }
    }
}
