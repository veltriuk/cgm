﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    /// <summary>
    /// Vista de las medidas de la curva típica. Simplificada para mostrar en la página web.
    /// </summary>
    public class VCurvaTipMeasure
    {
        /// <summary>
        /// Identificador de la curva
        /// </summary>
        public int id { get; set; }
        public int acquisition_item_id { get; set; }
        [DisplayName("Hora")]
        public DateTime datetime { get; set; }
        [DisplayName("Medida")]
        public double measure { get; set; }
        [DisplayName("Código")]
        public string code { get; set; }
        public int id_boundary { get; set; }

    }
}