namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Measure_Group")]
    public partial class Measure_Group
    {
        public long id { get; set; }
        [ForeignKey("Acquisition_Item")]
        public int? acquisition_item_id { get; set; }

        [ForeignKey("Report_Inform")]
        public long? report_inform_id { get; set; }

        public virtual Report_Inform Report_Inform { get; set; }
        public virtual Acquisition_Item Acquisition_Item { get; set; }
        public virtual ICollection<Measure> Measures { get; set; }
    }
}
