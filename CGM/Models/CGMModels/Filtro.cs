﻿using CGM.Areas.Telemetry.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Models.CGMModels
{
    /// <summary>
    /// Grupos de elementos, principalmente de fronteras o medidores
    /// </summary>
    [Table("Filtro", Schema = "dbo")]
    public partial class Filtro
    {
        public Filtro()
        {
            Meters = new HashSet<Meter>();
        }
        public int Id { get; set; }

        /// <summary>
        /// Nombre del filtro
        /// </summary>
        [Column(TypeName = "varchar")]
        [Required]
        [Display(Name = "Nombre")]
        [Index(IsUnique = true)]
        public string Name { get; set; }
        /// <summary>
        /// //1 Fronteras, 0 Medidores
        /// </summary>
        [Display(Name = "Tipo de filtro")]
        [ForeignKey("Destination")]
        public int destination_id { get; set; }
        /// <summary>
        /// 0: Se puede eliminar o modificar, 1: No se puede borrar ni modificar, y se actualiza permanentemente.
        /// 2: Filtro creado automáticamente para instrucciones rápidas
        /// </summary>
        public int defaultFilter { get; set; }
        /// <summary>
        /// El filtro nunca se elimina, simplemente se archiva, para poder rastrear las operaciones previas que los usaron.
        /// </summary>
        public bool isFiled{ get; set; }
        public int? market_id { get; set; }
        public DateTime? dateCreated { get; set; }
        public virtual ICollection<Meter> Meters { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
        public virtual List<Boundary> Boundaries { get; set; }
        public virtual Destination Destination { get; set; }
        public virtual ICollection<Alarm> Alarms { get; set; }



    }
}