namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CurvaTipMeasureGroup")]
    public partial class CurvaTipMeasureGroup
    {
        public int id { get; set; }
        [ForeignKey("Boundary")]
        public int boundary_id { get; set; }
        [ForeignKey("CurvaTip")]
        public int curvaTip_id { get; set; }

        public virtual CurvaTip CurvaTip { get; set; }
        public virtual Boundary Boundary { get; set; }
        public virtual ICollection<CurvaTipMeasure> Measures { get; set; }
    }
}
