

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXDLMS;
namespace CGMPro.DLMS
{
    public class Measure_Value
    {
        public double Value
        {
            get;
            set;
        }
        public DateTime time
        {
            get;
            set;
        }

    }

    public class LoadProfile
    {
        public MeasureLoadProfile Local_Load_Profile
        {
            get;
            set;
        }
        public enum MeasureLoadProfile
        {
            ApparentActivePower,
            PositiveActivePower,
            NegativeActivePower,
            PositiveReactivePower,
            PositiveReactivePowerQ2,
            NegativeReactivePower,
            NegativeReactivePowerQ4,
            VoltageRMS1,
            VoltageRMS2,
            VoltageRMS3,
            Event1,
            Event2,
            clock,
            NotAssigned,

        }
        public MeasureLoadProfile StringtoLoadProfile(string StringProfile)
        {
            MeasureLoadProfile localMLP = new MeasureLoadProfile(); ;
            switch (StringProfile)
            {
                case "Active energy +":
                    localMLP = MeasureLoadProfile.PositiveActivePower;
                    break;
                case "Active energy -":
                    localMLP = MeasureLoadProfile.NegativeActivePower;
                    break;
                case "Reactive energy  Q1":
                    localMLP = MeasureLoadProfile.PositiveReactivePower;
                    break;
                case "Reactive energy  Q2":
                    localMLP = MeasureLoadProfile.PositiveReactivePowerQ2;
                    break;
                case "Reactive energy  Q3":
                    localMLP = MeasureLoadProfile.NegativeReactivePower;
                    break;
                case "Reactive energy  Q4":
                    localMLP = MeasureLoadProfile.NegativeReactivePowerQ4;
                    break;
                case "Free channel":
                    localMLP = MeasureLoadProfile.NotAssigned;
                    break;

                case "Activa Importada Energ�a Acumulada":
                    localMLP = MeasureLoadProfile.PositiveActivePower;
                    break;
                case "Reactiva Importada Energ�a Acumulada":
                    localMLP = MeasureLoadProfile.PositiveReactivePower;
                    break;
                case "Activa Exportada Energ�a Acumulada":
                    localMLP = MeasureLoadProfile.NegativeActivePower;
                    break;
                case "Reactiva Exportada Energ�a Acumulada":
                    localMLP = MeasureLoadProfile.NegativeReactivePower;
                    break;
                case "Valor RMS de tensi�n en Fase 1":
                    localMLP = MeasureLoadProfile.VoltageRMS1;
                    break;
                case "Valor RMS de tensi�n en Fase 2":
                    localMLP = MeasureLoadProfile.VoltageRMS2;
                    break;
                case "Valor RMS de tensi�n en Fase 3":
                    localMLP = MeasureLoadProfile.VoltageRMS3;
                    break;
                case "Aparente Importada Energ�a Acumulada":
                    localMLP = MeasureLoadProfile.ApparentActivePower;
                    break;
                case "Positive Active Power":
                    localMLP = MeasureLoadProfile.PositiveActivePower;
                    break;
                case "Positive Reactive Power":
                    localMLP = MeasureLoadProfile.PositiveReactivePower;
                    break;
                case "Evento 1":
                    localMLP = MeasureLoadProfile.Event1;
                    break;
                case "Evento 2":
                    localMLP = MeasureLoadProfile.Event2;
                    break;
                case "Negative Reactive Power":
                    localMLP = MeasureLoadProfile.NegativeReactivePower;
                    break;
                case "Clock":
                    localMLP = MeasureLoadProfile.clock;
                    break;
                default:
                    throw new Exception("Conversi�n no permitida");
            }
            return localMLP;
        }
        public string LoadProfiletoString(MeasureLoadProfile m_Profile)
        {
            string localMLP="" ;
            switch (m_Profile)
            {

                case MeasureLoadProfile.PositiveActivePower:
                    localMLP = "Potencia Activa Positiva";
                    break;
                case MeasureLoadProfile.PositiveReactivePower:
                    localMLP = "Potencia Reactiva Positiva";
                    break;
                case MeasureLoadProfile.NegativeReactivePower:
                    localMLP = "Potencia Reactiva Negativa";
                    break;
                case MeasureLoadProfile.Event1:
                    localMLP = "Eventos 1";
                    break;
                case MeasureLoadProfile.Event2:
                    localMLP = "Eventos 2";
                    break;
                case MeasureLoadProfile.clock:
                    localMLP = "Reloj";
                    break;
                case MeasureLoadProfile.ApparentActivePower:
                    localMLP = "Potencia Aparente Positiva";
                    break;
                case MeasureLoadProfile.PositiveReactivePowerQ2:
                    localMLP = "Potencia Reactiva Positiva Cuadrante 2";
                    break;

                case MeasureLoadProfile.NegativeReactivePowerQ4:
                    localMLP = "Potencia Reactiva Negativa Cuadrante 4";
                    break;
                case MeasureLoadProfile.VoltageRMS1:
                    localMLP = "Voltaje RMS Fase 1";

                    break;
                case MeasureLoadProfile.NotAssigned:
                    localMLP = "No asignado";
                    break;
                case MeasureLoadProfile.VoltageRMS2:
                    localMLP = "Voltaje RMS Fase 2";

                    break;
                case MeasureLoadProfile.VoltageRMS3:
                    localMLP = "Voltaje RMS Fase 3";

                    break;
                default:
                    throw new Exception("Conversi�n no permitida");

            }

            return localMLP;
        }
        public static List<LocalMeasure> toLoadProfileMeasures(List<string> p_chanelsname, object[][] p_tempLOadProfile2,bool UTCEnable)
        {
            List<LocalMeasure> p_measurestoreturn = new List<LocalMeasure>();
            int hourstoadd = UTCEnable ? 0 : 5;
            for (int iLP = 1; iLP < p_chanelsname.Count(); iLP++)
            {
                LocalMeasure ElsterMeasure = new LocalMeasure();
                ElsterMeasure.Measure_Type.Local_Load_Profile = ElsterMeasure.Measure_Type.StringtoLoadProfile(p_chanelsname[iLP]);
                ElsterMeasure.time = DateTime.Now;
                List<Measure_Value> t_MEasures_Values = new List<Measure_Value>();
                for (int lcolLoadProfile = 0; lcolLoadProfile < p_tempLOadProfile2.GetLength(0); lcolLoadProfile++)
                {
                    t_MEasures_Values.Add(new Measure_Value() { Value = Convert.ToDouble(p_tempLOadProfile2[lcolLoadProfile][iLP]), time = Convert.ToDateTime(p_tempLOadProfile2[lcolLoadProfile][0]).AddHours(hourstoadd) });
                    
                }
                ElsterMeasure.Values = t_MEasures_Values;
                p_measurestoreturn.Add(ElsterMeasure);
            }

            return p_measurestoreturn;
        }

    }


    public class LocalMeasure
    {
        public LocalMeasure()
        {
            Values = new List<Measure_Value>();
            Measure_Type = new LoadProfile();
        }
        public LocalMeasure(string e_Measure_Type)
        {
            Values = new List<Measure_Value>();
            Measure_Type = new LoadProfile();
            Measure_Type.Local_Load_Profile = Measure_Type.StringtoLoadProfile(e_Measure_Type);
        }
        public LoadProfile Measure_Type
        {
            get;
            set;
        }
        public List<Measure_Value> Values
        {
            get;
            set;
        }
        public DateTime time
        {
            get;
            set;
        }
        public Multiplier Multiplier
        {
            get;
            set;
        }
        
        public double MultiplierFactor
        {
            get;
            set;
        }
    }
}
