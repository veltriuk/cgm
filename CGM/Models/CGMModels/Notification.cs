namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Simple sistema de notificaciones para mostrar al usuario. 
    /// </summary>
    [DisplayName("Notificación")]
    [Table("Notification")]
    public partial class Notification
    {
        public int id { get; set; }
        /// <summary>
        /// Not every notification has a message, since many of them are taken from templates in AlarmCases.
        /// </summary>
        [DisplayName("Mensaje")]
        public string message { get; set; }
        [ForeignKey("AlarmFire")]
        public int? alarmFire_id { get; set; }
        /// <summary>
        /// Fecha en que se crea la notificación
        /// </summary>
        public DateTime date_created { get; set; }
        [ForeignKey("NotificationGroup")]
        public int notification_group_id { get;  set; }
        public virtual AlarmFire AlarmFire { get; set; }
        public virtual NotificationGroup NotificationGroup { get; set; }
    }
}
