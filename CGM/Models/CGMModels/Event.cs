namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Event
    {
        public int id { get; set; }

        [Required]
        [StringLength(128)]
        public string asp_net_users_id { get; set; }

        public DateTime event_date { get; set; }

        [Required]
        public string details { get; set; }

        [StringLength(20)]
        public string table { get; set; }

        [StringLength(100)]
        public string object_id { get; set; }
        /// <summary>
        ///1:Creación       ;
        ///2:Modificación   ;
        ///3:Eliminación    ;
        ///4:Reporte        ;
        ///5:Carga de datos ;
        ///6:Archivo        ;
        /// </summary>
        [ForeignKey("Event_Category")]
        public int event_category_id { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual Event_Category Event_Category{ get; set; }
    }
}
