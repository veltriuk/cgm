namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Consigna la respuesta que ingresa el usuario a la información solicitada en el formulario
    /// </summary>
    [Table("ParamAnswer")]
    public partial class ParamAnswer
    {
        public int id { get; set; }

        [ForeignKey("FormInstance")]
        public int formInstance_id { get; set; }

        [ForeignKey("AnswerOption")]
        public int answerOption_id { get; set; }

        /// <summary>
        /// Valor de la respuesta
        /// </summary>
        [DisplayName("value")]
        public string value { get; set; } // Natively string. Changes depending on the answer option during the execution of the code.

        public virtual ParamFormInstance FormInstance { get; set; }
        public virtual ParamAnswerOption AnswerOption{ get; set; }
    }
}
