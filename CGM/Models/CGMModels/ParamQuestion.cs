namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Define una pregunta que puede estar compuesta por una o varias respuestas, dependiendo del tipo. 
    /// No confundir con las opciones de respuesta, que son las que finalmente registran el interrogante.
    /// </summary>
    [Table("ParamQuestion")]
    public partial class ParamQuestion
    {
        public int id { get; set; }

        [ForeignKey("Group")]
        public int group_id { get; set; }

        /// <summary>
        /// Nombre de la pregunta
        /// </summary>
        [DisplayName("Pregunta")]
        public string name { get; set; }
        [DisplayName("Alias")]
        public string alias { get; set; }

        [DisplayName("Tipo")]
        [ForeignKey("Type")]
        public int type_id { get; set; }
        [DisplayName("N�mero")]
        public int index { get; set; }
        /// <summary>
        /// Es una cadena de tipo JSON, que permite flexibilidad respecto a las restricciones que se agregue.
        /// </summary>
        [DisplayName("Condicionales")]
        public string conditionals { get; set; } // This is a JSON string

        public virtual ParamGroup Group{ get; set; }
        public virtual ParamType Type{ get; set; }
        public virtual ICollection<ParamAnswerOption> AnswerOptions{ get; set; }
    }
}
