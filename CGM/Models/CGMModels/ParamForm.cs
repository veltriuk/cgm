namespace CGM.Models.CGMModels
{
    using CGM.Areas.Telemetry.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// Nombre general de un formulario, que alberga un conjunto de preguntas y opciones de respuesta
    /// </summary>
    [Table("ParamForm")]
    public partial class ParamForm
    {
        public int id { get; set; }
        [DisplayName("Nombre")]
        public string name { get; set; }
        public virtual ICollection<ParamFormInstance> Instances{ get; set; }
    }
}
