namespace CGM.Models.CGMModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Config_CM
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }

        [StringLength(50)]
        [Column(TypeName = "NVARCHAR")]

        public string element { get; set; }

        [StringLength(200)]
        [Column(TypeName = "NVARCHAR")]

        public string text { get; set; }

        public float? value { get; set; }

        public bool? boolean { get; set; }
    }
}
