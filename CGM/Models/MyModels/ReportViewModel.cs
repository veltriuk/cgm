﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class ReportViewModel
    {
        public List<VMeasure> listToReport { get; set; }
        public List<VCurvaTipMeasure> listCurvaTips { get; set; }
        public List<Boundary> listNoActive { get; set; }
        public List<Boundary> listNoData { get; set; }
    }
}