﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class ExecutionModel
    {
        public int id;
        public Report executionReport { get; set; }
        public List<Boundary> boundaries { get; set; }
        public string sourceName;
        public string executionProcess;

        public static ExecutionModel ExecuModel { get; set; }
    }
}