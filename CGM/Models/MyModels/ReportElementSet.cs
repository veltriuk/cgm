﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using CGM.Models.CGMModels;

namespace CGM.Models.MyModels
{
    public class ReportElementSet   //Se incluyen todos los campos, aunque a veces pueda ser redundante, pero en muchas ocasiones es muy útil, junto a vMeasure
    {
        [NotMapped]
        public List<ReportElement> reportElements { get; set; }
        public DateTime dateReport { get; set; }
        public int negocio { get; set; }
        public int acquisition_item_id { get; set; }
        public static ReportElementSet reportElementSet;
    }
}