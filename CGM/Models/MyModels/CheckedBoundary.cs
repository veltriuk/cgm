﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class CheckedBoundary
    {
        public int id { get; set; }
        // A read-write instance property:
        public Boundary Frontera { get; set; }
        public bool Checked { get; set; }
    }
}