﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Models.MyModels
{
    public class SimpleBoundary //Simplified
    {
        public string name{ get; set; }
        public string code{ get; set; }
        /// <summary>
        /// Marca y serial
        /// </summary>
        public string Meter{ get; set; }
        /// <summary>
        /// Principal o respaldo
        /// </summary>
        public string Tipo{ get; set; } // Generación o comercialización
        public List<SimpleQuantity> Quantities { get; set; }
        public List<SimpleMeasure> measures { get; set; }
        // Used to appplications where another set of measures is required, like in the charts
        public List<SimpleMeasure> measures2 { get; set; } 
        public List<SimpleMeasure> measures3 { get; set; } 
    }
}