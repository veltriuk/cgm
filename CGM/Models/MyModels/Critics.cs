﻿using CGM.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CGM.Models.MyModels
{
    /// <summary>
    /// Set of measues and common info between them, as quantity and boundary.
    /// </summary>
    public class CriticBoundary
    {
        public string code { get; set; }
        public string name { get; set; }
        public int acq_item_id { get; set; }
        public bool is_backup { get; set; }
        /// <summary>
        /// Tells if a boundary has a critic source, such as a CurvaTip or a Max Value, depending on the critic parameters.
        /// If CurvaTip is chosen, and it does not have it, the GUI should highlight this.
        /// </summary>
        public bool has_critic_source { get; set; }
        public int? curvaTip_id { get; set; }
        public List<CriticMeasure> criticMeasures { get; set; }
        /// <summary>
        /// null: There's no comparison value
        /// 0: No. 1: Some of the measures violate critic condition.
        /// </summary>
        public bool? violates { get; set; }

    }
    /// <summary>
    /// Collects the comparison data from an original measure to a CurvaTip measure. Some elements might be redundant, only for display.
    /// </summary>
    public class CriticMeasure
    {
        /// <summary>
        /// Date and time of the corresponding measurement
        /// </summary>
        public DateTime datetime { get; set; }
        /// <summary>
        /// Original measure from the acquisition
        /// </summary>
        public double original { get; set; }
        /// <summary>
        /// Measure of the current Critic measure of comparison (i.e. CurvaTip measures)
        /// </summary>
        public double critic { get; set; }
        /// <summary>
        /// Percentage of difference with respect to CurvaTip reference. It is positive if original is higher than CurvaTip
        /// </summary>
        public double percentage { get; set; }
        /// <summary>
        /// Original minus CurvaTip.
        /// </summary>
        public double difference { get; set; }
        /// <summary>
        /// null: There's no comparison value
        /// 0: No. 1: Violates upper limit
        /// </summary>
        public bool? violates { get; set; }
    }
    /// <summary>
    /// Elemento que reúne las medidas y la crítica en general. Se guarda una versión serializada en base de datos, 
    /// pero se inclute solamente las fronteras que violan los límites.
    /// </summary>
    public class CriticSummary
    {
        /// <summary>
        // Options in this object:
        // name: compareTo, value: 0 (Valor máximo), 1 (Curva Típica)
        // name: excessValuePick, value: 0 (Valor fijo) , 1 (Porcentaje (%))
        // name: excessValue, value: num
        /// </summary>
        public Dictionary<string, string> parameters { get; set; }
        public List<CriticBoundary> boundaries { get; set; }
        /// <summary>
        /// En general, si ninguna de las medidas viola el valor establecido, esta bandera es falsa. Sino alguna viola, es verdadera.
        /// </summary>
        public bool violates { get; set; }
        public static string parameterReadableNames(string name, string value)
        {
            switch (name)
            {   case "compareTo":
                    if (value == "") return "Comparar con"; // Nombre del nombre, sin valor
                    if (value == "0") return "Valor máximo de cada frontera por defecto.";
                    if (value == "1") return "Curva típica.";
                    break;

                case "excessValuePick":
                    if (value == "") return "Referencia de exceso"; // Nombre del nombre, sin valor
                    if (value == "0") return "Valor fijo.";
                    if (value == "1") return "Porcentaje (%).";
                    break;

                case "excessValue": // Filling field
                    if (value == "") return "Valor de exceso"; // Nombre del nombre, sin valor
                    if (value != "") return value;
                    break;

                case "acq_item_id":
                    if (value == "") return "Medida"; // Nombre del nombre, sin valor
                    if (value != "") return DataAndDB.GetAcquisition_ItemsName(int.Parse(value)); // Get name of the Acq_item element.
                    break;

                default:
                    break;
            }
            
            // If none of the above is chosen
            return value;
        }
    }
}