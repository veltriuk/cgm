﻿using CGM.Helpers;
using CGM.Models.CGMModels;
using Hangfire;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace CGM.Controllers.CGM
{
    public class SettingsController : Controller
    {
        // GET: Telemetry/Settings
        public ActionResult Index()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.newconfig = TempData["newconfig"];

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ViewBag.CorreoCGM = db.Config_CM.Where(config => config.element.Trim().Equals("CorreoCGM")).FirstOrDefault().text;
            }

            return View();
        }
        [HttpPost]
        public ActionResult Index(string CorreoCGM)
        {

            //using (CGM_DB_Entities db = new CGM_DB_Entities())
            //{

            //    db.SaveChanges();
            //}
            TempData["result"] = new KeyValuePair<string, string>("true", "Configuración guardada con éxito.");
            return RedirectToAction("Index");
        }
        public ActionResult MailConfig(string passcurrent, string passcurrent2, string mailnew, string passnew)
        {
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                if (passcurrent == passcurrent2 && (passcurrent != null || passcurrent2 != null) && (passcurrent != "" || passcurrent2 != ""))
                {

                    Config_CM passcurrentdb = db.Config_CM.Where(config => config.element.Trim().Equals("PasswordCorreoCGM")).FirstOrDefault();
                    string decryptedtring = StringCipher.Decrypt(passcurrentdb.text, "nardidelucabothworkatsapienzauniversitadiroma");
                    if (decryptedtring == passcurrent)
                    {
                        Config_CM mail = db.Config_CM.Where(config => config.element.Trim().Equals("CorreoCGM")).FirstOrDefault();
                        mail.text = mailnew;
                        passcurrentdb.text = StringCipher.Encrypt(passnew, "nardidelucabothworkatsapienzauniversitadiroma");
                        db.Entry(mail).State = EntityState.Modified;
                        db.SaveChanges(); 

                        TempData["result"] = new KeyValuePair<string, string>("true", "Cambios realizados con éxito.");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["result"] = new KeyValuePair<string, string>("warning", "Validación no exitosa.");
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["result"] = new KeyValuePair<string, string>("warning", "Surgió un problema de validación. Posiblemente la entrada contraseñas actuales no conciden.");
                    return RedirectToAction("Index");
                }

            }
        }


        #region
        public ActionResult Reports()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.newconfig = TempData["newconfig"];

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ViewBag.WebServiceUser = db.Config_CM.Where(config => config.element.Trim().Equals("WebServiceUser")).FirstOrDefault().text;
            }

            return View();
        }
        #endregion

        #region Telemetry
        public ActionResult Telemetry()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.newconfig = TempData["newconfig"];

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                ViewBag.DiasAtras = db.Config_CM.Where(config => config.element.Trim().Equals("DiasAtras")).FirstOrDefault().value;
                ViewBag.ProcesadoresSimultaneos = db.Config_CM.Where(config => config.element.Trim().Equals("ProcesadoresSimultaneos")).FirstOrDefault().value;
                ViewBag.RetriesDefault = db.Config_CM.Where(config => config.element.Trim().Equals("RetriesDefault")).FirstOrDefault().value;
                ViewBag.Sobreescritura = db.Config_CM.Where(config => config.element.Trim().Equals("Sobreescritura")).FirstOrDefault().boolean;
                ViewBag.fillDays = db.Config_CM.Where(config => config.element.Trim().Equals("fillDays")).FirstOrDefault().value;
                ViewBag.exactQuery = db.Config_CM.Where(config => config.element.Trim().Equals("exactQuery")).FirstOrDefault().boolean;

                var quickProgramAcq = (int)db.Config_CM.Where(c => c.element.Equals("quickProgramAcq")).FirstOrDefault().value.Value; // Value of acquisition group for quick program
                ViewBag.quickProgramAcq = new SelectList(db.Acquisition.Where(ac => !ac.is_filed && ac.type == 0).ToList(),"id","Name", quickProgramAcq);
            }
            //quickProgramAcq
            return View();
        }
        [HttpPost]
        public ActionResult Telemetry(int DiasAtras, int? ProcesadoresSimultaneos, int RetriesDefault, int? Sobreescritura, int? exactQuery, int? fillDays, int quickProgramAcq)
        {
            bool sobreescritura = Sobreescritura == null ? false : true;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                if (ProcesadoresSimultaneos == null)
                {
                    ProcesadoresSimultaneos =Convert.ToInt32( db.Config_CM.Where(config => config.element.Trim().Equals("ProcesadoresSimultaneos")).FirstOrDefault().value);

                }
            }
            bool restartServer = false;
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                bool[] restart = new bool[] { changeConfigValue("ProcesadoresSimultaneos", ProcesadoresSimultaneos, db), //Parámetros que generan reinicio del servidor Hangfire
                                                changeConfigValue("RetriesDefault", RetriesDefault, db)};

                //No generan reinicio del servidor
                bool[] norestart = new bool[] { changeConfigValue("DiasAtras", DiasAtras, db),
                                                changeConfigValue("fillDays", fillDays, db),
                                                changeConfigValue("exactQuery", exactQuery, db),
                                                changeConfigValue("Sobreescritura", Sobreescritura, db),
                                                changeConfigValue("quickProgramAcq", quickProgramAcq, db)

                };

                restartServer = restart.Contains(true) ? true : false; //Se comprueba si alguno de los parámetros cambió. En ese caso se puede generar el reinicio del servidor

                // Si alguno de los parámetros cambió, entonces se guardan los registros en la base de datos.
                if (restart.Contains(true) || norestart.Contains(true)) 
                {
                    db.SaveChanges();
                }



            }
            TempData["newconfig"] = restartServer;
            //System.Web.HttpRuntime.UnloadAppDomain();

            TempData["result"] = new KeyValuePair<string, string>("true", "Configuración guardada con éxito. Los cambios tomarán algunos segundos en aplicarse.");
            return RedirectToAction("Telemetry");
        }

        // Este método comprueba si los valores de configuración en la interfaz fueron modificados. Si lo fueron, se aplica el cambio a la base de datos (fuera de este método)
        private bool changeConfigValue(string configElement, int? formConfig, CGM_DB_Entities db) //Solamente para parámetros que generan reinicio del servidor de Hangfire
        {
            // Discrimina booleanos de int
            if (configElement.Equals("Sobreescritura") || configElement.Equals("exactQuery")) //Se pueden incluir otros booleanos aquí
            {
                bool booleanParam = formConfig == null ? false : true;
                if (db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().boolean.Value != booleanParam)
                {
                    db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().boolean = booleanParam;
                    return true; //Irrelevante
                }
                return false;
            }
            else
            {
                bool restartServer = false; ////Solamente relevante para parámetros que generan reinicio del servidor de Hangfire

                Config_CM config_cm = db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault();

                float value = 0;
                if (config_cm.value != null)
                    value = config_cm.value.Value;

                if (value != formConfig)
                {
                    restartServer = true; //No reinicia para DiasAtras
                    db.Config_CM.Where(config => config.element.Trim().Equals(configElement)).FirstOrDefault().value = formConfig;
                }
                return restartServer;
        }
        }
        // Método para reiniciar el servidor de Hangfire. Se lo llama desde la vista Telemetry
        [HttpPost]
        public string NewConfigActions() //It is called by AJAX
        {
            bool disposeHangfireServers = DisposeHangfireServers();
            HttpRuntime.UnloadAppDomain();
            return disposeHangfireServers.ToString();
        }

        internal static bool DisposeHangfireServers()
        {
            try
            {
                var type = Type.GetType("Hangfire.AppBuilderExtensions, Hangfire.Core", throwOnError: false);
                if (type == null) return false;

                var field = type.GetField("Servers", BindingFlags.Static | BindingFlags.NonPublic);
                if (field == null) return false;

                var value = field.GetValue(null) as ConcurrentBag<BackgroundJobServer>;
                if (value == null) return false;

                var servers = value.ToArray();

                foreach (var server in servers)
                {
                    // Dispose method is a blocking one. It's better to send stop
                    // signals first, to let them stop at once, instead of one by one.
                    server.SendStop();
                }

                foreach (var server in servers)
                {
                    server.Dispose();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}