﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using Newtonsoft.Json;
using System.IO;

namespace CGM.Controllers.CGM
{
    public class AlarmFiresController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        public static CriticSummary criticSummary;
        public static AlarmFire alarmFire;
        // GET: AlarmFires
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="AlarmFires";
            var alarmFires = await db.AlarmFires.Include(a => a.Alarm).Include(a => a.Report_Inform).ToListAsync();
            return View( alarmFires);
        }

        // GET: AlarmFires/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="AlarmFires";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alarmFire = await db.AlarmFires.FindAsync(id);
            criticSummary = JsonConvert.DeserializeObject<CriticSummary>(alarmFire.details);
            if (alarmFire == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("~/Views/AlarmFires/_Details.cshtml", alarmFire);
            }
                        
            return View(alarmFire);
        }

        [HttpGet]
        public PartialViewResult GetCriticPartial()
        {
            // Put the data in the rigght javascript format to display in the chart
            Dictionary<string, List<double[]>> chartData = CriticController.ChartData(criticSummary.boundaries);
            ViewBag.chartData = JsonConvert.SerializeObject(chartData);
            ViewBag.criticSummary = alarmFire.details;

            ViewBag.max = criticSummary.boundaries.Max(b => b.criticMeasures.Max(m => m.original));
            ViewBag.min = criticSummary.boundaries.Min(b => b.criticMeasures.Min(m => m.original));
            ViewBag.fileName = "Critic";
            return PartialView("~/Views/Critic/_Critic.cshtml", criticSummary); // partial view should be typed to data.
        }

        [HttpGet]
        public string DetailsToMail(int id)
        {
            alarmFire = db.AlarmFires.Find(id);
            alarmFire.CriticSummary = JsonConvert.DeserializeObject<CriticSummary>(alarmFire.details);

            List<AlarmFire> alarmFires = new List<AlarmFire>() { alarmFire };

            ViewData.Model = alarmFires;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "_DetailsMail");
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);

                //viewContext.ViewBag.chartData = JsonConvert.SerializeObject(chartData);
                //viewContext.ViewBag.criticSummary = alarmFire.details;

                //viewContext.ViewBag.max = criticSummary.boundaries.Max(b => b.criticMeasures.Max(m => m.original));
                //viewContext.ViewBag.min = criticSummary.boundaries.Min(b => b.criticMeasures.Min(m => m.original));

                viewResult.View.Render(viewContext, sw);

                string pageContent = sw.GetStringBuilder().ToString();

                List<Config_CM> conf = db.Config_CM.ToList();

                Helpers.MailManagement.EnviarCorreoAhora
                                   (
                                     conf.Where(c => c.element.Contains("CorreoCGM")).FirstOrDefault().text,
                                     Helpers.StringCipher.Decrypt(conf.Where(c => c.element == "PasswordCorreoCGM").FirstOrDefault().text, "nardideluca"),
                                     "[Adquisición] Valor en adquisición excedido",
                                     pageContent,
                                     new List<string>() { conf.Where(c => c.element.Contains("DeveloperMail")).FirstOrDefault().text },
                                     null,
                                     true
                                   );
                return pageContent;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

