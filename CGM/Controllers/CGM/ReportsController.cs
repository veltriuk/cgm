﻿
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers;
using System.IO;
using CGM.Models.MyModels;
using System.Globalization;
using System.Threading.Tasks;
using CGM.Helpers.Attributes;
using NLog;
using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using CGM.Areas.Telemetry.Models;

namespace CGM.Controllers.CGM
{
    public class ReportsController : Controller
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private CGM_DB_Entities db = new CGM_DB_Entities();
        NumberFormatInfo provider = new NumberFormatInfo { NumberDecimalSeparator = "." };
        private static List<VMeasure> listToReport;
        private static int[] currentBoundaryIds;
        private static List<Boundary> boundaries; //Se hace un objeto estático, pues se hace esta petición varias veces a la base de datos.
        private static int report_id;
        private static List<VMeasure> vMeasures;
        private static DateTime fecha;
        private static int negocio;
        private static string fileName;
        // GET: Reports
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Index()
        {
            List<Report> reports = db.Reports.ToList();
            ViewBag.Result = TempData["result"];
            ViewBag.navbar1 = "Reportes";
            return View(reports);
        }

        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Manual()
        {
            ViewBag.Result = TempData["result"];

            boundaries = db.Boundary.ToList();

            var quantitiesDB = db.Quantity.Include(q => q.Magnitude).Where(q => q.name == "Energía Reactiva Positiva" || q.name == "Energía Activa (Positiva)").ToList();

            ViewBag.quantity_id = new SelectList(quantitiesDB, "id", "name", 1);

            return View(boundaries);
        }

        public ActionResult GetPreReport(string ids, int report_type, string opday, int quantity_id, string criticParameters, int curvaTipId = 0)
        {
            DateTime dt = DateTime.Parse(opday);
            int[] boundariesids = ids.Split(',').Select(int.Parse).ToArray();

            listToReport = MarketReport.getMeasuresReport(boundariesids, dt, quantity_id);//report_type);

            currentBoundaryIds = listToReport.Select(v => v.id).Distinct().ToArray();

            List<Boundary> listNoActive = boundaries.Where(b => b.report_type_id == report_type && b.is_active == false).ToList();
            List<Boundary> listNoData = boundaries.Where(b => !listToReport.Any(lr => lr.id == b.id) && b.is_active).ToList(); //Se toma las fronteras que no registran datos. Que no están en el reporte.
            //List<VCurvaTipMeasure> ctm = ;
            List<VCurvaTipMeasure> listCurvaTips = new List<VCurvaTipMeasure>();

            int curvaTipSelected = 0;

            if (curvaTipId != 0)
           {
                listCurvaTips = db.VCurvaTipMeasures.AsNoTracking().Where(c => c.id == curvaTipId && currentBoundaryIds.Contains(c.id_boundary)).ToList();
                curvaTipSelected = curvaTipId;
            }
            else
            {
                // Select the CurvaTip set by default
                var curvaTipDefault = db.CurvaTip.Where(c => c.Acquisition_Item.quantity_id == quantity_id
                                                          && c.Acquisition_Item.tag_line_id == 4 // TagLine = 4 => Total. Assume that in the report Total measurements are taken
                                                          && c.isDefault).FirstOrDefault();

                if (curvaTipDefault != null)
                {
                    curvaTipSelected = curvaTipDefault.id;
                }
            }
            

            ViewBag.CurvaTipsSelection = curvaTipId;
            string negocio = report_type == 0 ? "25" : "26"; // "25"= Generación; "26" = Comercialización
            string[] splitDate = dt.ToString().Split('/');
            fileName = "CR" + negocio + splitDate[1] + splitDate[0];
            ViewBag.fileName = fileName;

            var curvatips = db.CurvaTip.ToList();

            ViewBag.curvaTipSelector = new SelectList(db.CurvaTip, "id", "name", curvaTipSelected);

            ReportViewModel rvm = new ReportViewModel()
            {
                listNoActive = listNoActive,
                listNoData = listNoData,
                listToReport = listToReport,
                listCurvaTips = listCurvaTips
            };

            if (listToReport.Count != 0)
            {
                ViewBag.min = listToReport.Min(m => m.measure);
                ViewBag.max = listToReport.Max(m => m.measure);
            }

            // Default form configuration is in json form already
            ViewBag.defaultFormConfig = db.Config_CM.Where(c => c.element.Trim().Equals("defaultParametersVisualConfig")).FirstOrDefault().text;

            return PartialView("_Prereport", rvm);
        }

        /// <summary>
        /// Se ejecuta después de haber generado la primera vista de reporte, pues usa el objeto listToReport, y remplaza la tabla original
        /// con la tabla con la crítica. Esto es, el análiis de las medidas dependiendo de unos valores de comparación.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCurvaTipCriticTable(string ids, int report_type, string opday, int quantity_id, string criticParameters, int curvaTipId = 0)
        {
            List<CriticBoundary> criticBoundaries = new List<CriticBoundary>();
            if (opday == null || opday == "" || curvaTipId == 0 || criticParameters == "" || criticParameters == null ) // Safety check
            {
                ViewBag.Result = new KeyValuePair<string, string>("warning", "Parámetros incompletos, o no se encuentra datos para la solicitud.");
                return PartialView("~/Measures/_Critic.cshtml", criticBoundaries);
            }

            DateTime dt = DateTime.Parse(opday);
            // Varios de los parámetros enviados son inicializados en la solicitud inicial del reporte, en GetPreReport
            if (currentBoundaryIds == null) // En caso de que no se recargue la página o algo. Esto no debería presentarse. Sólo en caso.
            {
                currentBoundaryIds = ids.Split(',').Select(int.Parse).ToArray(); // Posiblemente se agreguen más fronteras de las que se están analizando. 
            }

            // Get the Acquisition item corresponding to that quantity in the particular case of Total measurements (ta_line_id == 4)
            int acq_item_id = db.Acquisition_Item.Where(a => a.quantity_id == quantity_id && a.tag_line_id == 4).Select(a => a.Id).FirstOrDefault();

            // Remove Critic For Boundaries with Alumbrado Público. No sense in doing critics to that
            int[] boundariesAP_ids = db.Boundary.Where(b => b.Boundary_Source.source == "Alumbrado Público" && currentBoundaryIds.Contains(b.id)).Select(b => b.id).ToArray();
            currentBoundaryIds = currentBoundaryIds.Except(boundariesAP_ids).ToArray();

            var measuresToCritic = listToReport.Where(m => currentBoundaryIds.Contains(m.id)).ToList();

            CriticSummary criticSummary = CriticController.GetCriticInReport(currentBoundaryIds, dt, acq_item_id, curvaTipId, criticParameters, measuresToCritic);

            

            // Get the quantity (mainly for display)
            Quantity quantity = db.Quantity.Include(q => q.Magnitude).Where(q => q.id == quantity_id).FirstOrDefault(); 
            ViewBag.quantity_name = quantity.name;
            ViewBag.quantity_unit = quantity.Magnitude.unit_symbol;

            // Put the data in the rigght javascript format to display in the chart
            Dictionary<string, List<double[]>> chartData = CriticController.ChartData(criticSummary.boundaries);
            ViewBag.chartData = JsonConvert.SerializeObject(chartData);
            ViewBag.criticSummary = JsonConvert.SerializeObject(criticSummary);

            if (listToReport.Count != 0)
            {
                ViewBag.min = listToReport.Min(m => m.measure);
                ViewBag.max = listToReport.Max(m => m.measure);
            }
            ViewBag.fileName = fileName;
            ViewBag.curvaTipName = db.CurvaTip.Find(curvaTipId).name;



            return PartialView("~/Views/Critic/_Critic.cshtml", criticSummary);
        }

        public ActionResult SendReportResult()
        {
             return View("Details", db.Reports.Include(r => r.Report_Informs.Select(ri => ri.Boundary))
                                              .Include(r => r.Report_Informs.Select(ri => ri.Substatus))
                                              .Include(r => r.Report_Type)
                                              .FirstOrDefault(r => r.id == report_id));
        }
        public ActionResult Details(int id)
        {
            return View("Details", db.Reports.Include(r => r.Report_Informs.Select(ri => ri.Boundary)).Include(r => r.Report_Type).FirstOrDefault(r => r.id == id));
        }
        public ActionResult SendReport()
        {
            List<VBoundary> vBoundaries = VBoundary.ToVBoundaries(listToReport);
            MarketReport mr = new MarketReport();
            Tuple<string, Report> res = mr.ReportToMarket(vBoundaries);

            if (res.Item2 != null) //No hubo errores y se devolvió un objeto
            {
                report_id = res.Item2.id;
                return RedirectToAction("SendReportResult");
            }
            else
            {
                TempData["result"] = new KeyValuePair<string, string>("error", res.Item1);

                return RedirectToAction("Manual");
            }
        }
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult LoadFile()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.navbar1 = "Reportes";
            ViewBag.navbar2 = "Carga de medidas";
            //Se inicializa el objeto de reporte para tener a mano datos requeridos.
            CMORReportObject.CmorObject = new CMORReportObject { BoundaryList = db.Boundary.Include(b => b.Boundary_Source).Include(b => b.Report_Type).OrderBy(b => b.name).ToList(),
                                                                 ReportList = new List<ReportElement>(),
                                                                 report_types = db.Report_Type.ToList(),
                                                                 ReportInformlist = new List<Report_Inform>() };

            return View();
        }

        // POST: Meter/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult LoadFilePost()
        {
            string[] values = new string[] { };
            int ind = 0;
            List<Boundary> BoundaryList = db.Boundary.ToList();
            vMeasures = new List<VMeasure>();

            try
            {

                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    int negocio = 0;
                    if (file.FileName.Substring(2, 2) == "25") negocio = 0; //Para incluir en Report_Type más adelante, donde se toma valores de 1 o 0 
                    if (file.FileName.Substring(2, 2) == "26") negocio = 1;

                    string rta = FileManagement.LoadFileValidation(file, "CR");
                    if (rta == "OK")
                    {
                        string path = Path.Combine(Server.MapPath("~/Files/Reports/CRFiles/Uploaded"),
                                      Path.GetFileName(file.FileName));
                        file.SaveAs(path);

                        string rt = FileManagement.InsideFileValidatation(path);

                        if (rt != "OK")
                        {
                            
                            ViewBag.Result = new KeyValuePair<string, string>("warning", rt);

                            //return Content("Este es un mensaje de error!");
                            return PartialView("_Tabla_vMedidas");
                        }

                        fecha = FileManagement.getDateFromFileName(file);

                        List<VBoundary> vBoundaries = new List<VBoundary>();

                        List<string> lines = FileManagement.readFileLines(path).ToList();
                        for (ind = 0; ind < lines.Count; ind++)
                        {
                            values = lines[ind].Split('\t');
                            Boundary boundary = BoundaryList.Where(b => b.code.ToLower() == values[0].Trim().ToLower()).First();
                            bool is_backup = values[1] == "R" ? true : false;

                            for (int j = 0; j < 24; j++)
                            {
                                vMeasures.Add(new VMeasure()
                                {
                                    measure = Convert.ToDouble(values[j + 3].Replace(',', '.'), provider), // new NumberFormatInfo { NumberDecimalSeparator = "," }  
                                    hour = fecha.AddHours(j + 1),
                                    code = boundary.code,
                                    name = boundary.name,
                                    is_backup = is_backup,
                                    quantity_id = 1, // Energia Activa (Positiva)
                                    tag_line_id = 4 // Total
                                });
                            }
                        }

                        rt = FileManagement.checkDuplicates(vMeasures);

                        if (rt != "OK")
                        {
                            ViewBag.Result = new KeyValuePair<string, string>("warning", rt);
                            return PartialView("_Tabla_vMedidas");
                        }
                        
                        ViewBag.min = vMeasures.Min(m => m.measure);
                        ViewBag.max = vMeasures.Max(m => m.measure);
                        ViewBag.fileName = file.FileName.Split('.')[0]; //"CR260611.txt" , remove txt
                        
                        return PartialView("_Tabla_vMedidas", vMeasures);


                    }
                    else
                    {
                        //TempData["Error"] = rta;
                        ViewBag.Error = rta;
                        //return Content("Este es un mensaje de error!");
                        return PartialView("_Tabla_Medidas");
                    }
                }
            }
            catch (InvalidOperationException invEx)
            {
                string msg = "No se encontró un elemento en la selección en base de datos. Probablemente la frontera no se haya creado, o tiene un código diferente. " + Environment.NewLine
                    + "Elemento en línea: " + ind + 1 + Environment.NewLine 
                    + "Primer elemento: " + values[0];
                    ;
                logger.Error(invEx, msg);
                ViewBag.Error = msg;
                return PartialView("_Tabla_Medidas");
            }
            ViewBag.Error = "Hubo un error. No se pasó las validaciones del archivo.";
            //return Content("Este es un mensaje de error!");
            return PartialView("_Tabla_Medidas");
        }

        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> UploadDataFromFile()
        {
            Report report = vMeasuresToReport(vMeasures);

            try
            {
                DataAndDB.insertReports(report);
                //db.Reports.Add(report);
                //await db.SaveChangesAsync();
                KeyValuePair<string, string> resultInsert = new KeyValuePair<string, string>("true", "Carga de datos realizada de manera exitosa.");
                //KeyValuePair<bool, string> resultInsert = new KeyValuePair<bool, string>(false, "Esta es una prueba");

                TempData["result"] = resultInsert;
                return RedirectToAction("LoadFile");
            }
            catch (Exception ex)
            {
                TempData["result"] = new KeyValuePair<string,string>("warning", "Hubo un error cargando las medidas a la base de datos. Si el problema persiste, contáctese con los administradores de la página.");
                return RedirectToAction("LoadFile");
            }
            

        }

        public Report vMeasuresToReport(List<VMeasure> vMeasures)
        {
            List<Measure_Group> groups = new List<Measure_Group>();
            List<Boundary> boundaries = db.Boundary.ToList();

            List<Report_Inform> ris = new List<Report_Inform>();

            foreach (var vmgroup in vMeasures.GroupBy(vm => new Tuple<string, bool?>(vm.code, vm.is_backup)))
            {
                List<Measure> measures = new List<Measure>();
                foreach (var measure in vmgroup)
                {
                    measures.Add(new Measure()
                    {
                        measure1 = measure.measure,
                        measure_date = measure.hour
                    });
                }

                Measure_Group group = new Measure_Group()
                {
                    acquisition_item_id = 1, // Puede haber otros casos donde la adquisición sea de otro identificador
                    Measures = measures
                };

                Report_Inform ri = new Report_Inform()
                {
                    assignedReading = true,
                    substatus_id = 500,
                    Measure_Groups = new List<Measure_Group>() { group },
                    boundary_id = boundaries.Where(b => b.code.ToLower() == vmgroup.Key.Item1.ToLower()).Select(b => b.id).FirstOrDefault(),
                    is_backup = vmgroup.Key.Item2,
                };
                ris.Add(ri);
            }

            Report report = new Report()
            {
                date_and_time_report = DateTime.Now,
                operation_date = fecha,
                destination_id = 1, // 1: carga a base de datos
                is_active = false,
                market_id = negocio,
                status_id = 5,
                Report_Informs = ris
            };

            return report;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}