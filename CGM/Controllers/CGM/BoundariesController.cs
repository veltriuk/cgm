﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers;
using CGM.Areas.Telemetry.Models;
using System.Data.Entity.Core.Objects;
using CGM.Helpers.Attributes;
using NLog;

namespace CGM.Controllers.CGM
{
    public class BoundariesController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Boundaries2
        public async Task<ActionResult> Index()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName= "Fronteras";
            var boundary = await db.Boundary.Include(b => b.Boundary_Source).Include(b => b.Report_Type).Where(b=>b.is_filed == false).ToListAsync();
            return View(boundary);
        }

        // GET: Boundaries2/Details/5
        public async Task<ActionResult> Details(int id)
        {
            ViewBag.ControllerName="Fronteras";
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boundary boundary = await db.Boundary.FindAsync(id);
            if (boundary == null)
            {
                return HttpNotFound();
            }
            return View(boundary);
        }

        // GET: Boundaries2/Create
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create()
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName= "Fronteras";
            ViewBag.source_id = new SelectList(db.Boundary_Source, "Id", "source");
            ViewBag.report_type_id = new SelectList(db.Report_Type, "id", "report_type1");
            ViewBag.id_priority = new SelectList(db.Priority.Where(p => p.id != 4 ), "id", "name"); //No se incluye la prioridad Muy Alta
            return View();
        }

        // POST: Boundaries2/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        [UpdateDefaultAttribute]
        public async Task<ActionResult> Create([Bind(Include = "name,code,real_name,has_backup,is_active,min_active,min_reactive,max_active, max_reactive,commentary,source_id,report_type_id, id_priority")] Boundary boundary, int[] energychk)
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName= "Fronteras";
            boundary = setNullBoundaryValues(boundary, energychk);

            if (ModelState.IsValid)
            {
                db.Boundary.Add(boundary);
                await db.SaveChangesAsync();
                DataAndDB.insertEvents(boundary, 1, boundary.id.ToString(), typeof(Boundary).Name.ToString()); //1 = Creación, en Event_Category
                return RedirectToAction("Index");
            }

            ViewBag.source_id = new SelectList(db.Boundary_Source, "Id", "source", boundary.source_id);
            ViewBag.report_type_id = new SelectList(db.Report_Type, "id", "report_type1", boundary.report_type_id);
            ViewBag.id_priority = new SelectList(db.Priority.Where(p => p.id != 4 ), "id", "name", boundary.id_priority); //No se incluye la prioridad Muy Alta
            return View(boundary);
        }


        //GET: Boundaries2/Edit/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.Result = TempData["result"];

            ViewBag.ControllerName = "Fronteras";

            Boundary boundary = await db.Boundary.FindAsync(id);
            if (boundary == null)
            {
                return HttpNotFound();
            }

            ViewBag.source_id = new SelectList(db.Boundary_Source, "Id", "source", boundary.source_id);
            ViewBag.report_type_id = new SelectList(db.Report_Type, "id", "report_type1", boundary.report_type_id);
            ViewBag.id_priority = new SelectList(db.Priority.Where(p => p.id != 4 ), "id", "name", boundary.id_priority); //No se incluye la prioridad Muy Alta
        //    ViewBag.main_meter_id = GetDropDownListCustom(boundary.MeterMain.id);
         //   ViewBag.backup_meter_id = GetDropDownListCustom(boundary.MeterBackup.id);
            ViewBag.Result = TempData["result"];

            return View(boundary);
        }

        //POST: Boundaries2/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse.Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        [UpdateDefaultAttribute]
        public async Task<ActionResult> Edit([Bind(Include = "id,name,code,real_name,has_backup,is_active,min_active,min_reactive,max_active, max_reactive,commentary,source_id,report_type_id, id_priority")] Boundary boundary, int[] energychk)
        {
            db.Configuration.ProxyCreationEnabled = false;
            ViewBag.ControllerName = "Fronteras";

            var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();

            boundary = setNullBoundaryValues(boundary, energychk);

            if (ModelState.IsValid)
            {
                Boundary previousObj = db.Boundary.Include(m => m.Meters).Include(m => m.Report_Type).AsNoTracking().First((m => m.id == boundary.id)); //Se hace una copia del elemento original antes de cambiarlo
               // Meter meterMain = db.Meter.AsNoTracking().FirstOrDefault(m => m.boundary_id == boundary.id && m.is_backup == false); //Se añade los datos del Modelo de medidor, que por defecto no se añade
               // Meter meterBackup = db.Meter.AsNoTracking().FirstOrDefault(m => m.boundary_id == boundary.id && m.is_backup == true); //Se añade los datos del Medidor, que por defecto no se añade
                boundary.Report_Type = db.Report_Type.AsNoTracking().First(m => m.id == boundary.report_type_id); //Se añade los datos del Tipo de Reporte, que por defecto no se añade

                db.Entry(boundary).State = EntityState.Modified;
                await db.SaveChangesAsync();
                DataAndDB.insertEvents(new { elementoPrevio = previousObj, elementoModificado = boundary, cambios = OtherMethods.getObjectChangesFromDB(previousObj, boundary) }, 2, boundary.id.ToString(), boundary.GetType().Name); //2 = Modificación, en Event_Category
                return RedirectToAction("Index");
            }
            ViewBag.source_id = new SelectList(db.Boundary_Source, "Id", "source", boundary.source_id);
            ViewBag.report_type_id = new SelectList(db.Report_Type, "id", "report_type1", boundary.report_type_id);
            ViewBag.id_priority = new SelectList(db.Priority.Where(p => p.id != 4), "id", "name", boundary.id_priority); //No se incluye la prioridad Muy Alta
            TempData["result"] = new KeyValuePair<string, string>("warning", "Hubo un error con la validación del formulario. Verifique que los campos estén llenados correctamente..");
            ViewBag.Result = TempData["result"];

            return View(boundary);
        }

        private Boundary setNullBoundaryValues(Boundary boundary, int[] energychk) //Debe haber una forma menos fea
        {
            if (energychk != null)
            {
                if (!energychk.Contains(0)) //Si la casilla de energía activa está desmarcada
                {
                    boundary.max_active = null;
                    boundary.min_active = null;
                }

                if (!energychk.Contains(1)) //Si la casilla de energía reactiva está desmarcada
                {
                    boundary.max_reactive = null;
                    boundary.min_reactive = null;
                }
            }
            else
            {
                boundary.max_active = null;
                boundary.min_active = null;
                boundary.max_reactive = null;
                boundary.min_reactive = null;
            }
            return boundary;
        }

        // GET: Boundaries2/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]

        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="Fronteras";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boundary boundary = await db.Boundary.FindAsync(id);
            if (boundary == null)
            {
                return HttpNotFound();
            }
            return View(boundary);
        }

        // POST: Boundaries2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        [UpdateDefaultAttribute]
        public async Task<ActionResult> DeleteConfirmed(int id) //No se debe borrar la frontera. Simplemente se la archiva.
        {
            ViewBag.ControllerName="Fronteras";
            Boundary boundary = await db.Boundary.FindAsync(id);
            boundary.is_filed = true;
            boundary.Meters = null; // Desasociar medidores
            db.Entry(boundary).State = EntityState.Modified;

            await db.SaveChangesAsync();

            db.Configuration.ProxyCreationEnabled = false;
            Boundary b = db.Entry(boundary).CurrentValues.ToObject() as Boundary;

            DataAndDB.insertEvents(b, 6, boundary.id.ToString(), typeof(Boundary).Name.ToString()); //6 = Archivo, en DataSourceItems
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult CreateALot(int howmany) //Función de ejemplo para poblar la tabla de medidores
        {
            List<Boundary> boundaries = new List<Boundary>();
            int id = db.Boundary.OrderByDescending(m => m.id).Select(m => m.id).FirstOrDefault();

            for (int i = id + 1; i < id + howmany; i++)
            {
                Boundary boundary = new Boundary() { code = "Frt" + "000" + i, has_backup = false, is_active = true, is_filed = false, name = "Frontera de prueba " + i, source_id = 15, report_type_id = 1 };
                db.Boundary.Add(boundary); //Se registra la frontera en DB
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult BoundaryPicker(int report_type)
        {
            // adds even non active boundaries
            var boundaries = db.Boundary.Where(b => b.report_type_id == report_type && !b.is_filed).ToList();
            return PartialView("_BoundaryPicker", boundaries);
        }

        //private SelectList GetDropDownListCustom(int? selectedValue) //Una frontera sí puede tener un espacio vacío en el medidor
        //{
        //    CGM_DB_Entities db = new CGM_DB_Entities();
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    SelectList lista;

        //    //Esta función prepara la lista de medidores en la vista. Descarta los medidores que ya están siendo utilizados por otras fronteras
        //    var meterList = db.Meter.Where(m => (m.BoundaryMain == null &&
        //                                     m.BoundaryBackup == null) ||
        //                                     m.id == selectedValue).Select(m => m);

        //    foreach (var item in meterList)
        //    {
        //        list.Add(new SelectListItem() { Value = item.id.ToString(), Text = item.Model_Meter.brand + " - " + item.serial });
        //    }

        //    if (selectedValue != null)
        //    {
        //        list.Insert(0, new SelectListItem() { Value = "", Text = "--- Borrar medidor ---" });
        //        lista = new SelectList(list, "Value", "Text", selectedValue);
        //        return lista;
        //    }
        //    else
        //    {
        //        list.Insert(0, new SelectListItem() { Value = "", Text = "--- Seleccione medidor ---" });
        //        lista = new SelectList(list, "Value", "Text");
        //        return lista;
        //    }
        //    return null;
        //}
    }
}

