﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using NLog;
using CGM.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CGM.Controllers.CGM
{
    public class AlarmsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Alarms
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Alarms";
            var alarms = db.Alarms.Include(a => a.AlarmCase).Include(a => a.Priority).Where(a => !a.isFiled);



            ViewBag.Result = TempData["result"];
            return View(await alarms.ToListAsync());

        }

        // GET: Alarms/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="Alarms";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alarm alarm = await db.Alarms.FindAsync(id);
            if (alarm == null)
            {
                return HttpNotFound();
            }
            return View(alarm);
        }

        // GET: Alarms/Create
        public ActionResult Create()
        {
            loadViewBagData(null);

            return View();
        }

        // POST: Alarms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,name,alarmCase_id,priority_id,parameters,mail_params,isActive")] Alarm alarm, int?[] selectList, string[] user_names, string send_mail)
        {
            ViewBag.ControllerName="Alarms";

            if (ModelState.IsValid)
            {

                //int alarmsSameCase = db.Alarms.Where(a => !a.isFiled && a.alarmCase_id == alarm.alarmCase_id).Count();
                //if (alarmsSameCase > 1)
                //{
                //    TempData["result"] = new KeyValuePair<string, string>("warning", "Este caso de alarmas no permite .");

                //}


                // Parse different arguments in the form

                
                if (selectList != null)
                {
                    alarm.Filtros = db.Filtro.Where(f => selectList.Contains(f.Id)).ToList();
                }
                else // This can be the case where no filters are needed, like in the creation PQRS requests.
                {
                    alarm.Filtros = new List<Filtro>();
                }
                
                alarm.Users = db.AspNetUsers.Where(u => user_names.Contains(u.UserName)).ToList();

                alarm.send_mail = bool.Parse(send_mail);

                db.Alarms.Add(alarm);
                await db.SaveChangesAsync();
                
                logger.Info("Se realiza la creación de la Alarma con id = " + alarm.id);

                db.Entry(alarm).State = EntityState.Detached;

                DataAndDB.insertEvents(alarm, 1, alarm.id.ToString(), alarm.GetType().Name); //1 = Creación, en Event_Category

                TempData["result"] = new KeyValuePair<string, string>("true", "Alarma creada con éxito.");

                return RedirectToAction("Index");
            }
            loadViewBagData(alarm);
            //ViewBag.alarmCase_id = new SelectList(db.AlarmCases, "id", "description", alarm.alarmCase_id);
            //ViewBag.priority_id = new SelectList(db.Priority, "id", "name", alarm.priority_id);
            //// To show the quantity and the tagline in the selector
            //var acq_items = db.Acquisition_Item.Select(a => new { a.Id, name = a.Quantity.name + " - " + a.Tag_Line.name, a.Quantity.name_short, name_tag = a.Tag_Line.name }).ToList();
            //ViewBag.acq_item_id = new SelectList(acq_items, "Id", "name", 1); 
            return View(alarm);
        }

        [HttpGet]
        public bool Create_nameCheck([Bind(Include = "id,name,alarmCase_id,priority_id,parameters,mail_params,isActive")] Alarm alarm)
        {
            var nameCheck = db.Alarms.Where(a => a.name == alarm.name && a.id != alarm.id ).Count();
            if (nameCheck > 0) return true;
            else return false;
        }


        // GET: Alarms/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName="Alarms";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alarm alarm = await db.Alarms.AsNoTracking().Include(a => a.Filtros).Where(a => a.id == id).FirstOrDefaultAsync();
            if (alarm == null)
            {
                return HttpNotFound();
            }
            loadViewBagData(alarm);

            return View(alarm);
        }

        // POST: Alarms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,name,alarmCase_id,priority_id,parameters,isActive")] Alarm alarm, int[] selectList, string user_names, string send_mail)
        {
            ViewBag.ControllerName = "Alarms";
            string[] userList = user_names.Split(',');
            
            if (ModelState.IsValid)
            {
                Alarm alarmExisting = db.Alarms.Include(a => a.Filtros).Where(a => a.id == alarm.id).FirstOrDefault();

                // Update parent
                db.Entry(alarmExisting).CurrentValues.SetValues(alarm);

                alarmExisting.Filtros = db.Filtro.Where(f => selectList.Contains(f.Id)).ToList();
                alarmExisting.Users = db.AspNetUsers.Where(u => userList.Contains(u.UserName)).ToList();
                alarmExisting.send_mail = bool.Parse(send_mail);

                //db.Configuration.ProxyCreationEnabled = true; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas

                //Alarm previousObj = db.Alarms.Include(m => m.Filtros).AsNoTracking().First((m => m.id == alarm.id)); //Se hace una copia del elemento original antes de cambiarlo

                db.Entry(alarmExisting).State = EntityState.Modified;
                await db.SaveChangesAsync();
                //DataAndDB.insertEditEvent(previousObj, alarm, alarm.id);
                TempData["Result"] = new KeyValuePair<string, string>("true", "Cambios realizados con éxito.");
                return RedirectToAction("Index");
            }

            Dictionary<string, string> alarmParams = OtherMethods.htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(alarm.parameters));
            //Dictionary<string, string> mailParams = OtherMethods.htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(alarm.mail_params));

            ViewBag.alarmParams = alarmParams;
            ViewBag.alarmCase_id = new SelectList(db.AlarmCases, "id", "description", alarm.alarmCase_id);
            ViewBag.priority_id = new SelectList(db.Priority, "id", "name", alarm.priority_id);


            // To show the quantity and the tagline in the selector
            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, name = a.Quantity.name + " - " + a.Tag_Line.name, a.Quantity.name_short, name_tag = a.Tag_Line.name }).ToList();
            ViewBag.acq_item_id = new SelectList(acq_items, "Id", "name", alarmParams["acq_item_id"]);
            var filtrosTotal = db.Filtro.ToList();
            ViewBag.filtros = filtrosTotal.Except(alarm.Filtros).ToList();
            return View(alarm);
        }

        // GET: Alarms/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="Alarms";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alarm alarm = await db.Alarms.FindAsync(id);
            if (alarm == null)
            {
                return HttpNotFound();
            }
            return View(alarm);
        }

        // POST: Alarms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="Alarms";
            Alarm alarm = await db.Alarms.FindAsync(id);
            alarm.isFiled = true;
            alarm.isActive = false;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Simple method to load all the necessary data of alarms into the ViewBag, in order to not repeat code among methods.
        /// </summary>
        /// <param name="alarm"></param>
        public void loadViewBagData(Alarm alarm)
        {
            var filtros = db.Filtro.ToList();
            ViewBag.filtros = filtros;
            var acq_items = db.Acquisition_Item.Select(a => new { a.Id, name = a.Quantity.name + " - " + a.Tag_Line.name, a.Quantity.name_short, name_tag = a.Tag_Line.name }).ToList();
            var acq_itemsList = new SelectList(acq_items, "Id", "name");
            var alarmCases = new SelectList(db.AlarmCases, "id", "description");
            var priority = new SelectList(db.Priority, "id", "name");
            List<AspNetUsers> users = db.AspNetUsers.ToList();

            if (!User.IsInRole("Desarrollador")) // If the user is not developer, only include non-developer users.
            {
                users = users.Where(u => !u.AspNetRoles.Select(r => r.Name).Contains("Desarrollador")).ToList();
            }

            if (alarm != null)
            {
                int[] filtrosInAlarmIds = alarm.Filtros.Select(f => f.Id).ToArray();
                ViewBag.filtros = filtros.Where(f=> !filtrosInAlarmIds.Contains(f.Id)).ToList();

                Dictionary<string, string> alarmParams = OtherMethods.htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(alarm.parameters));
                ViewBag.alarmParams = alarmParams;
                //Dictionary<string, string> mailParams = OtherMethods.htmlJsonFormToJobjectList(JsonConvert.DeserializeObject<List<JObject>>(alarm.mail_params));
                //ViewBag.mailParams = mailParams;

                // Separate the users that had been selected beofre to include in the mail list

                alarmCases.Where(a => a.Value == alarm.alarmCase_id.ToString()).FirstOrDefault().Selected = true;
                priority.Where(a => a.Value == alarm.priority_id.ToString()).FirstOrDefault().Selected = true;
                acq_itemsList.Where(a => a.Value == alarmParams["acq_item_id"]).FirstOrDefault().Selected = true;
            }

            ViewBag.acq_item_id = acq_itemsList;
            ViewBag.alarmCase_id = alarmCases;
            ViewBag.priority_id = priority;
            
            ViewBag.users = users;


            // To show the quantity and the tagline in the selector
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

