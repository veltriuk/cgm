﻿using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;

namespace CGM.Controllers.CGM
{
    //[DataContract(IsReference = true)]
    public class MapsBoundaryController : Controller
    {

        private CGM_DB_Entities db = new CGM_DB_Entities(); 

        public ActionResult Index()    
        { 
            var boundary = db.Boundary.ToList();
            return View(boundary);
        }

        public JsonResult GetMD()
        {
            return Json(db.Boundary.Select(m => new MyBoundary() {
                id = m.id,
                name = m.name,
                code = m.code,
                is_active = m.is_active,
                max_active = m.max_active,
                max_reactive = (m.max_reactive == null) ? 0 : m.max_reactive,
                latitude = m.latitude, 
                longitude = m.longitude  
            }).Where(m2 => m2.longitude != null && m2.latitude != null ).ToList(), JsonRequestBehavior.AllowGet);
        } 

    }
} 