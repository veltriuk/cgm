﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers.Attributes;

namespace CGM.Controllers.CGM
{
    public class AlarmCasesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: AlarmCases
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="AlarmCases";
            var alarmCases = db.AlarmCases.Include(a => a.Destination).Include(a => a.Priority);
            return View(await alarmCases.ToListAsync());
        }

        // GET: AlarmCases/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="AlarmCases";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlarmCase alarmCase = await db.AlarmCases.FindAsync(id);
            if (alarmCase == null)
            {
                return HttpNotFound();
            }
            return View(alarmCase);
        }

        // GET: AlarmCases/Create
        [CustomAuthorize(Roles = "Desarrollador")]
        public ActionResult Create()
        {
            ViewBag.ControllerName="AlarmCases";
            ViewBag.destination_id = new SelectList(db.Destination, "id", "name");
            ViewBag.priority_id = new SelectList(db.Priority, "id", "name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador")]
        public async Task<ActionResult> Create([Bind(Include = "id,name,destination_id,priority_id,description,parameters")] AlarmCase alarmCase)
        {
            int id = db.AlarmCases.ToList().Count + 1;
            ViewBag.ControllerName="AlarmCases";
            if (ModelState.IsValid)
            {
                alarmCase.id = id;
                db.AlarmCases.Add(alarmCase);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.destination_id = new SelectList(db.Destination, "id", "name", alarmCase.destination_id);
            ViewBag.priority_id = new SelectList(db.Priority, "id", "name", alarmCase.priority_id);
            return View(alarmCase);
        }

        // GET: AlarmCases/Edit/5
        [CustomAuthorize(Roles = "Desarrollador")]
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName="AlarmCases";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlarmCase alarmCase = await db.AlarmCases.FindAsync(id);
            if (alarmCase == null)
            {
                return HttpNotFound();
            }        
            
            ViewBag.destination_id = new SelectList(db.Destination, "id", "name", alarmCase.destination_id);
            ViewBag.priority_id = new SelectList(db.Priority, "id", "name", alarmCase.priority_id);
            return View(alarmCase);
        }

        // POST: AlarmCases/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador")]
        public async Task<ActionResult> Edit([Bind(Include = "id,name,destination_id,priority_id,description,parameters")] AlarmCase alarmCase)
        {
            ViewBag.ControllerName="AlarmCases";
            if (ModelState.IsValid)
            {

                db.Entry(alarmCase).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.destination_id = new SelectList(db.Destination, "id", "name", alarmCase.destination_id);
            ViewBag.priority_id = new SelectList(db.Priority, "id", "name", alarmCase.priority_id);
            return View(alarmCase);
        }

        // GET: AlarmCases/Delete/5
        [CustomAuthorize(Roles = "Desarrollador")]
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="AlarmCases";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlarmCase alarmCase = await db.AlarmCases.FindAsync(id);
            if (alarmCase == null)
            {
                return HttpNotFound();
            }
            return View(alarmCase);
        }

        // POST: AlarmCases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="AlarmCases";
            AlarmCase alarmCase = await db.AlarmCases.FindAsync(id);
            db.AlarmCases.Remove(alarmCase);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

