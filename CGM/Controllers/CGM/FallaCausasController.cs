﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;

namespace CGM.Controllers.CGM
{
    public class FallaCausasController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: FallaCausas
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="FallaCausas";
            return View(await db.FallaCausas.ToListAsync());
        }

        // GET: FallaCausas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="FallaCausas";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FallaCausa fallaCausa = await db.FallaCausas.FindAsync(id);
            if (fallaCausa == null)
            {
                return HttpNotFound();
            }
            return View(fallaCausa);
        }

        // GET: FallaCausas/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="FallaCausas";
            return View();
        }

        // POST: FallaCausas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,causa")] FallaCausa fallaCausa)
        {
            ViewBag.ControllerName="FallaCausas";
            if (ModelState.IsValid)
            {
                db.FallaCausas.Add(fallaCausa);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(fallaCausa);
        }

        // GET: FallaCausas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            ViewBag.ControllerName="FallaCausas";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FallaCausa fallaCausa = await db.FallaCausas.FindAsync(id);
            if (fallaCausa == null)
            {
                return HttpNotFound();
            }        
            
            return View(fallaCausa);
        }

        // POST: FallaCausas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,causa")] FallaCausa fallaCausa)
        {
            ViewBag.ControllerName="FallaCausas";
            if (ModelState.IsValid)
            {

                db.Entry(fallaCausa).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(fallaCausa);
        }

        // GET: FallaCausas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="FallaCausas";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FallaCausa fallaCausa = await db.FallaCausas.FindAsync(id);
            if (fallaCausa == null)
            {
                return HttpNotFound();
            }
            return View(fallaCausa);
        }

        // POST: FallaCausas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="FallaCausas";
            FallaCausa fallaCausa = await db.FallaCausas.FindAsync(id);
            db.FallaCausas.Remove(fallaCausa);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

