﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Controllers.CGM
{
    public class FiltrosController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Telemetry/Filtros
        public async Task<ActionResult> Index()
        {
            ViewBag.Result = TempData["result"];
            return View(await db.Filtro.AsNoTracking().Where(f => !f.isFiled).ToListAsync());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Id del filtro</param>
        /// <param name="display">Con mensaje 'alert'. 0 o null: Con mensaje, 1: Sin mensaje</param>
        /// <returns></returns>
        // GET: Telemetry/Filtros/Details/5
        public async Task<ActionResult> Details(int? id, int? display) 
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas
            ViewBag.ControllerName = "Filtros";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filtro filtro = await db.Filtro.Include(f => f.Meters)
                                           .Include(f => f.Boundaries)
                                           .Include(f => f.Boundaries.Select(b => b.Boundary_Source))
                                           .Include(f => f.Boundaries.Select(b => b.Meters))
                                           .Include(f => f.Boundaries.Select(b => b.Meters.Select(m => m.Model_Meter)))
                                           .Where(f=>f.Id == id).FirstOrDefaultAsync();
            ViewBag.DisplayAlert = display;
            if (filtro == null)
            {
                return HttpNotFound();
            }
            return PartialView(filtro);
        }

        // GET: Telemetry/Filtros/Create
        public ActionResult Create () // 0 = medidores, 1 = fronteras
        {
            ViewBag.returnUrl = Request.Url.AbsolutePath;

            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName="Filtros";
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("Create");
            }
            else
            {
                return RedirectToAction("Index");
            }
            //return View();
        }
        /// <summary>
        /// Se llama desde la página web, por medio de AJAX. Devuelve la lista para escoger el filtro.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="market"></param>
        /// <returns></returns>
        public ActionResult CreatePartial(int id, int? market) // 0 = medidores, 1 = fronteras
        {
            ViewBag.Result = TempData["result"];
            ViewBag.ControllerName = "Filtros";
            Filtro filtro = new Filtro() { destination_id = id , market_id = market};// 0 = medidores, 1 = fronteras
            List<Meter> meters = new List<Meter>();
            List<Boundary> boundaries = new List<Boundary>();

            if (id == 1)
            {
                // Filtra las fronteras que corresponden al negocio seleccionado
                boundaries = db.Boundary.Where(b => b.report_type_id == market).OrderBy(b => b.name).ToList();
                ViewBag.Tipo = "fronteras";
            }
            if (id == 0)
            {
                meters = db.Meter.Include(m => m.Model_Meter).Where(m => !m.is_filed).OrderBy(b => b.boundary_id).ToList();
                ViewBag.Tipo = "medidores";
            }
            ViewBag.suggestedName = getSuggestedName("Filtro de ",id, ViewBag.Tipo); //Se asegura un nombre diferente

            FiltroViewModel filtroViewModel = new FiltroViewModel() { filtro = filtro, boundaries = boundaries, meters = meters };
            return PartialView("_CreatePartial", filtroViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,destination_id,market_id")] Filtro filtro, int[] selectList)
        {
            ViewBag.ControllerName = "Filtros";

            if (db.Filtro.Any(f => f.Name.Trim() == filtro.Name.Trim()))
            {
                TempData["result"] = new KeyValuePair<string, string>("warning","Ya existe un filtro con ese nombre. Favor elija otro nombre de filtro." );
                return RedirectToAction("Create");
            }
            if (selectList == null)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "Filtro vacío. Favor incluya algún elemento.");
                return RedirectToAction("Create");
            }
            if (ModelState.IsValid)
            {

                if (filtro.destination_id == 1)
                    filtro.Boundaries = db.Boundary.Where(m => selectList.Any(s => s == m.id)).ToList();
                if (filtro.destination_id == 0)
                    filtro.Meters = db.Meter.Where(m => !m.is_filed).Where(m => selectList.Any(s => s == m.id)).ToList();
                filtro.dateCreated = DateTime.Now; // Se incluye la estampa de tiempo
                filtro.defaultFilter = 0; // 0=Definido por el usuario. Modificable
                db.Filtro.Add(filtro);
                await db.SaveChangesAsync();
                TempData["result"] = new KeyValuePair<string, string>("true", "Filtro creado exitosamente!");
                return RedirectToAction("Index");
            }
            TempData["result"] = new KeyValuePair<string, string>("warning","Error creando filtro. Datos ingresados no válidos." );
            return RedirectToAction("Create");
        }

        // GET: Telemetry/Filtros/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas
            ViewBag.ControllerName="Filtros";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filtro filtro =await db.Filtro.Include(f => f.Boundaries)
                                     .Include(f => f.Meters)
                                     .Where(b => b.Id == id).FirstOrDefaultAsync();
            if (filtro == null)
            {
                return HttpNotFound();
            }
            FiltroViewModel filtroViewModel = new FiltroViewModel(){filtro = filtro};
            if (filtro.destination_id == 1)
            {
                //filtroViewModel.boundaries = await db.Boundary.Where(b => filtro.Boundaries.Any(bo => bo.id == b.id)).ToListAsync(); //El id de Boundary es string, y no es primitivo, por lo que no deja realizar esta iteración
                List<Boundary> list = await db.Boundary.ToListAsync(); //Si se hace directamente con este query abajo, no deja.
                filtroViewModel.boundaries = list.Where(bb => !filtro.Boundaries.Any(b => b.id == bb.id)).ToList();
            }
            if (filtro.destination_id == 0)
            {
                //List<Meter> list = await db.Boundary.ToListAsync(); //Si se hace directamente con este query abajo, no deja.
                List<Meter> list = await db.Meter.Include(m=>m.Model_Meter).Where(m => !m.is_filed).ToListAsync(); //Si se hace directamente con este query abajo, no deja.
                filtroViewModel.meters = list.Where(mm => !filtro.Meters.Any(b => b.id == mm.id)).ToList();
                //filtroViewModel.meters = await db.Meter.Where(m => filtro.Meters.Any(me => me.id == m.id)).ToListAsync();
            }
            //////david
            /////
            //var tembounfilter=filtro.Boundaries.ToList();
            //tembounfilter.Add(filtroViewModel.boundaries.Where(ij=>ij.id==51).FirstOrDefault());
            //filtro.Boundaries = tembounfilter;
            //db.Entry(filtro).State = EntityState.Modified;
            //await db.SaveChangesAsync();
            return View(filtroViewModel);
        }

        // POST: Telemetry/Filtros/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,destination_id")] Filtro filtro, int[] selectList)
        {
            filtro.Meters = null;
            filtro = await db.Filtro.Include(f => f.Boundaries)
                         .Include(f => f.Meters)
                         .Where(b => b.Id == filtro.Id).FirstOrDefaultAsync();
            ViewBag.ControllerName="Filtros";
            if (selectList == null)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "Filtro vacío. Favor incluya algún elemento.");
                return RedirectToAction("Edit", new { filtro.Id });
            }
            if (filtro.Name == null)
            {
                TempData["result"] = new KeyValuePair<string, string>("warning", "No se ingresó nombre de filtro. Favor ingrese nombre.");
                return RedirectToAction("Edit", new { filtro.Id });
            }
            if (filtro == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (filtro.Name == "Comercialización" || filtro.Name == "Generación") //Filtros que no se pueden eliminar o editar
                {
                    return HttpNotFound();
                }
            }


            //List<Meter> meters = db.Meter.Where(ai => selectList.Any(s => s == ai.boundary_id)).ToList();//added by david//db.Meter.Where(ai => selectList.Any(s => s == ai.id)).ToList();
            List<Boundary> boundaries=db.Boundary.Where(ai=>selectList.Any(s=>s==ai.id)).ToList();//added by david
            //filtro.Meters = meters;
            filtro.Boundaries = boundaries;//added by david
            if (ModelState.IsValid)
            {
                db.Entry(filtro);
                db.Entry(filtro).State = EntityState.Modified;
                await db.SaveChangesAsync();
                TempData["result"] = new KeyValuePair<string, string>("true","Grupo editado exitosamente." );
                return RedirectToAction("Index");
            }
            return View(filtro);
        }

        // GET: Telemetry/Filtros/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName="Filtros";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Filtro filtro = await db.Filtro.Include(f => f.Destination).FirstOrDefaultAsync(f => f.Id ==  id);
            if (filtro == null)
            {
                return HttpNotFound();
            }
            else
            {
                if (filtro.Name == "Comercialización" || filtro.Name == "Generación") //Filtros que no se pueden eliminar o editar
                {
                    TempData["result"] = new KeyValuePair<string, string>("warning", "Este filtro ("+filtro.Name+") no se puede eliminar. Es filtro definido por el sistema.");
                    return RedirectToAction("Index");
                }
            }

            return View(filtro);
        }

        // POST: Telemetry/Filtros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // Un filtro no se elimina de la base de datos, pues los programas que los usaron quedarían sin elemento asociado.
            // Simplemente se los archiva, y luego no se los muestra al usuario.

            Filtro filtro = await db.Filtro.FindAsync(id);
            filtro.isFiled = true;
            db.Entry(filtro).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static string getSuggestedName(string basestring, int type, string typeString) { //Entrega el nombre sugerido para el filtro. La idea es que no se repitan.
            // "Filtro de" + "Instrucción rápida"
            string nameTemplate = basestring + typeString;
            List<Filtro> filtros = new List<Filtro>();
            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                filtros = db.Filtro.ToList();
            }

            int iden = filtros.Where(f => f.destination_id == type).Count()+1;

            string name = nameTemplate + " " + iden;
            foreach (var item in filtros)
            {
                if (filtros.Any(f => f.Name.Equals(name.Trim())))
                    iden++;
                else
                    break;
                name = nameTemplate + " " + iden;
            }

            return name;
        }

        public ActionResult GetFilters(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var filters = db.Filtro.Include(f => f.Meters.Select( m => m.Model_Meter)).Include(ac => ac.Meters.Select(m=>m.Boundary)).Where(a => a.Id == id).FirstOrDefault();
            //var acitems = db.Acquisition.Where(a => a.id == id).Select(a => a.Acquisition_Items).ToList();
            return PartialView("~/Views/Filtros/_FilList.cshtml", filters);
        }


    }
}

