﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using System.IO;
using CGM.Helpers;
using Newtonsoft.Json;
using System.Web.Helpers;

namespace CGM.Controllers.CGM
{
    public class PQRSController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: PQRS
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="PQRS";
            var pQRS = await db.PQRS.Include(p => p.User).ToListAsync();
            return View(pQRS);
        }

        // GET: PQRS/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="PQRS";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PQRS pQRS = await db.PQRS.FindAsync(id);
            if (pQRS == null)
            {
                return HttpNotFound();
            }
            return View(pQRS);
        }

        [HttpPost]
        public ActionResult AddMessage(int id, string message)
        {
            List<string> paths = LoadFiles(Request.Files);

            PQRS_Message pQRS_Message = new PQRS_Message()
            {
                message = message,
                date_created = DateTime.Now,
                files = JsonConvert.SerializeObject(paths),
                user_id = DataAndDB.getCurrentUser(),
                PQRS_id = id
            };

            PQRS pqrs = db.PQRS.Find(id);
            pqrs.PQRS_Messages.Add(pQRS_Message);
            db.SaveChanges();

            return RedirectToAction("Details",new { id = id});
        }

        /// <summary>
        /// This is the little window of Dropzone that pops-up to attach files
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddFiles()
        {
            return PartialView("_AddFiles");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public ActionResult CreatePQRSFromLayout(string title, string message)
        {
            List<string> paths = LoadFiles(Request.Files);

            string user_id = DataAndDB.getCurrentUser();
            // Creates a list of messages, and add the first one (might be more in the future)
            List<PQRS_Message> messages = new List<PQRS_Message>()
            {
                new PQRS_Message()
                {
                    date_created = DateTime.Now,
                    message = message,
                    user_id = user_id,
                    files = JsonConvert.SerializeObject(paths)
                }
            };

            PQRS pQRS = new PQRS()
            {
                PQRS_Messages = messages,
                date_created = DateTime.Now,
                user_id = user_id,
                date_closed = null,
                solved = false,
                title = title
            };

            db.PQRS.Add(pQRS);

            if (db.Alarms.Where(a => a.AlarmCase.name == "Creación de PQRS").Count() > 0)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                dict.Add("Fecha de creación", pQRS.date_created.ToString());
                dict.Add("Usuario", db.AspNetUsers.Where(u => u.Id == user_id).FirstOrDefault().UserName );
                dict.Add("Mensaje", messages.FirstOrDefault().message);
                dict.Add("Archivos cargados", paths.Count.ToString());

                AlarmHelper.GeneralAlarm("Creación de PQRS", dict);
            }

            db.SaveChanges();


            return RedirectToAction("Index");
        }


        /// <summary>
        /// Saves the uploaded files in the corresponding folder.
        /// </summary>
        /// <param name="RequestFiles">HttpFileCollectionBase object withe the files to be uploaded </param>
        /// <returns>A list of string with all the paths.</returns>
        private List<string> LoadFiles(HttpFileCollectionBase RequestFiles)
        {
            List<string> paths = new List<string>();

            // Iterate over the uploaded files
            foreach (string fileName in RequestFiles)
            {
                HttpPostedFileBase file_input = RequestFiles[fileName];


                string path = Path.Combine("Files/PQRS/",
                                    Path.GetFileName(file_input.FileName));

                // Put a timestamp in the file, for no repetition
                string[] pathext = path.Split('.');

                // Relative path, to then show in Details page
                path = string.Join("", pathext.Take(pathext.Count() - 1).ToArray())
                       + "_" + DateTime.Now.ToShortDateString().Replace('/', '-') + "_" + DateTime.Now.ToShortTimeString().Replace(':', '-') + "." + pathext.Last();

                file_input.SaveAs(Path.Combine(Server.MapPath("../"), path)); // Save the file locally
                paths.Add(path);
            }
            return paths;
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PQRS pQRS = db.PQRS.Find(id);
            if (pQRS == null)
            {
                return HttpNotFound();
            }
            return View(pQRS);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PQRS pQRS = await db.PQRS.FindAsync(id);
            db.PQRS.Remove(pQRS);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteMessage(int id)
        {
            PQRS_Message message = db.PQRS_Messages.Find(id);
            db.PQRS_Messages.Remove(message);
            db.SaveChanges();

            return Json(new { res="ok"});
        }

        /// <summary>
        /// Si el hilo está resuelto, lo marca como no resuelto, y viceversa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MarkAsSolved(int id)
        {
            
            PQRS pqrs = db.PQRS.Find(id);
            if (!pqrs.solved)
            {
                pqrs.solved = true;
                pqrs.date_closed = DateTime.Now;
            }
            else
            {
                pqrs.solved = false;
                pqrs.date_closed = null;
            }
            db.SaveChanges();

            return Json(new { res = "ok" });
        }

        [HttpPost]
        public ActionResult MarkAsUnSolved(int id)
        {
            PQRS pqrs = db.PQRS.Find(id);
            pqrs.solved = false;
            pqrs.date_closed = null;
            db.SaveChanges();

            return Json(new { res = "ok" });
        }

        public FileResult DownloadAttachment(string path) {

            return File(Path.Combine(Server.MapPath("../"), path), "text/plain", path.Split('/').Last());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

