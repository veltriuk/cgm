﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using Newtonsoft.Json;
using CGM.Helpers;

namespace CGM.Controllers.CGM
{
    public class NotificationsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        public static List<NotificationGroup> notificationGroups;
        public static string notificationGroupsPrevJson = "";
        // GET: Notifications
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Notifications";
            notificationGroups = await db.NotificationGroups.Include(n => n.Alarm).Include(n => n.Notifications).ToListAsync();

            // Set no not-active to the active groups, and register the datetime.
            var activeGroups = notificationGroups.Where(n => n.is_active).ToList();
            if (activeGroups.Count > 0)
            {
                foreach (var item in activeGroups)
                {
                    item.date_read = DateTime.Now;
                    item.is_active = false;
                }
                db.SaveChangesAsync();
            }
            
            //ViewBag.notificationGroups = Newtonsoft.Json.JsonConvert.SerializeObject(notificationGroups);

            return View( notificationGroups );
        }

        /// <summary>
        /// Get the notifications group to show in page or feed
        /// </summary>
        /// <param name="activeOnly">Set to 1 if want to restrict to active only</param>
        /// <param name="init">When the page starts, return the current notifications</param>
        /// <returns></returns>
        public ActionResult GetNotificationGroups(int activeOnly=0, int init = 0)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var notifGroupsQuery = db.NotificationGroups.AsNoTracking().Include(n => n.Alarm).Include(n => n.Notifications);

            if (activeOnly == 1)
            {
                notificationGroups = notifGroupsQuery.Where(n => n.is_active).ToList();

                // If both elements are equal, send nothing
                string json1 = OtherMethods.objectToJSON(notificationGroups);

                if (json1.Equals(notificationGroupsPrevJson) && init == 0) // Si los dos elementos son iguales, y ya se ha cargado la página por primera vez
                {
                    return Json(new { flag ="false", result = "No notification changes." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    notificationGroupsPrevJson = json1;
                    return Json(new { flag = "true", result = json1 }, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                notificationGroups = notifGroupsQuery.ToList();
            }


            var jsonResult = OtherMethods.objectToJSON(notificationGroups);

            return Content(jsonResult, "application/json");
        }

        // GET: Notifications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="Notifications";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Notification notification = await db.Notification.FindAsync(id);
            if (notification == null)
            {
                return HttpNotFound();
            }
            return View(notification);
        }

        public ActionResult GetNotificationsFeed()
        {
            int isThere = 0;

            List<NotificationGroup> groups = db.NotificationGroups.Where(n => n.is_active).ToList();

            // Check if there's at least one. If so, show it on screen
            if (groups.Count > 0)
            {
                return PartialView("_NotifFeed",groups);
            }
            else
            {
                return Json(new { result = "false"}, JsonRequestBehavior.AllowGet);
            }

        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

