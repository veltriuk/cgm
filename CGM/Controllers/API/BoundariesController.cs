﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;

namespace CGM.Controllers.API
{
    public class BoundariesController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Boundaries
        //public IQueryable<Boundary> GetBoundary()
        //{
        //    return db.Boundary;
        //}
        public List<Boundary> GetBoundaries()
        {
            db.Configuration.ProxyCreationEnabled = false;

            List<Boundary> boundaryList = db.Boundary.OrderBy(b => b.name).ToList();
            return boundaryList;
        }

        public List<Boundary> GetBoundaries(int type) //0 para principal, 1 para respaldo
        {
            db.Configuration.ProxyCreationEnabled = false;

            List<Meter> meters = db.Meter.Where(m => m.boundary_id == null).ToList();

            var boundaryList = db.Boundary.Where(b => !b.is_filed); // boundary not deleted

            if (type == 0) // Se devuelve las fronteras que aún no tienen medidor principal asignado
            {
                boundaryList = boundaryList.Where(b => b.Meters.Count() == 0 || (!b.Meters.Any(m => m.is_backup == false))).OrderBy(b => b.name);
            }
            if (type == 1) // Se devuelve las fronteras que aún no tienen medidor de resplado asignado
            {
                boundaryList = boundaryList.Where(b => b.Meters.Count() == 0 || (!b.Meters.Any(m => m.is_backup == true))).OrderBy(b => b.name);
            }
            return boundaryList.ToList();

        }

        // GET: api/Boundaries/5
        [ResponseType(typeof(Boundary))]
        public async Task<IHttpActionResult> GetBoundary(int id)
        {
            Boundary boundary = await db.Boundary.FindAsync(id);
            if (boundary == null)
            {
                return NotFound();
            }

            return Ok(boundary);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}