﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;

namespace CGM.Controllers.API
{
    public class MetersController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<Meter> GetMeters()
        {
            db.Configuration.ProxyCreationEnabled = false;

            List<Meter> meterList = db.Meter.Include(m => m.Boundary).Include(m => m.Model_Meter).Where(m => !m.is_filed).OrderBy(b => b.Model_Meter.brand).ToList();
            return meterList;
        }

        public List<KeyValuePair<int, string>> GetMeters(int pick) //Se usa para listar las fronteras en lectura rápida, pero seleccionando el medidor.
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Meter> meterList = new List<Meter>();
            if (pick == 0)
            {   //Adquiere medidores con el nombre de frontera.
                meterList = db.Meter.Include(m => m.Boundary).Include(m => m.Model_Meter).Where(m => !m.is_filed).OrderBy(b => b.Model_Meter.brand).ToList();
            }
            if (pick == 2)
            {   //Adquiere medidores con el nombre de frontera, pero sólamente los que tienen frontera.
                meterList = db.Meter.Include(m => m.Boundary).Include(m => m.Model_Meter).Where(m => !m.is_filed).Where(m => m.boundary_id != null).OrderBy(b => b.Boundary.name).ToList();
            }

            return formatOutput(meterList);
        }

        public List<KeyValuePair<int, string>> formatOutput(List<Meter> meters)
        {
            List<KeyValuePair<int, string>> returnlist = new List<KeyValuePair<int, string>>();
            foreach (var item in meters)
            {
                string text = "";
                if (item.is_backup != null)
                {
                    if (item.is_backup == true)
                    {
                        text = item.Boundary.name + " - " + item.Boundary.code + " (R)";
                    }
                    if (item.is_backup == false)
                    {
                        text = item.Boundary.name + " - " + item.Boundary.code + " (P)";
                    }
                }
                else
                {
                    text = item.Model_Meter.brand + " - #" + item.serial;
                }
                returnlist.Add(new KeyValuePair<int, string>(item.id, text));
            }
            return returnlist;
        }

    }
}