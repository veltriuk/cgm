﻿using CGM.Models.CGMModels;
using CGM.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CGM.Controllers.API
{
    public class StatsController : ApiController
    {
        private static CGM_DB_Entities db = new CGM_DB_Entities();
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// Get all the activate boundaries that are taken into account in this day
        /// </summary>
        /// <param name="negocio">0 = Generación, 1 = Comercialización, 10 = Todos</param>
        /// <param name="details"></param>
        /// <returns></returns>
        [HttpGet]
        public List<Object> getActiveBoundaries(int? negocio = null)
        {
            List<Boundary> boundaries = new List<Boundary>();

            if (negocio == null)
                boundaries = db.Boundary.Where(b => b.is_active).ToList();
            else if (negocio == 0 || negocio == 1) //Generación
                boundaries = db.Boundary.Where(b => b.is_active && b.report_type_id == negocio).ToList();
            else boundaries = db.Boundary.Where(b => b.is_active).ToList();

            string response = ""; // Response string to return
            List<SimpleBoundary> simpleBoundaries = new List<SimpleBoundary>(); // Returning object
            List<Object> objs = new List<object>();
            // We execute this cycle to serialize a very simple object, and not the whole entity
            foreach (var b in boundaries)
            {
                var obj = new { b.code , b.name, tipo = b.report_type_id};
                objs.Add(obj);
            }
            return objs;
        }
        /// <summary>
        /// Returns the number of boundaries with measures for some especific day
        /// </summary>
        /// <param name="negocio"></param>
        /// <param name="opDay">Día de operación (dd/mm/yyyy)</param>
        /// <returns></returns>
        [HttpGet]
        public List<Boundary> okActiveBoundaries(int negocio, string opDay)
        {
            DateTime day = DateTime.Parse(opDay);
            List<Boundary> okBoundaries = db.Report_Inform.Where(ri => ri.assignedReading.Value //Si registra medidas
                                                                  && day.CompareTo(ri.Report.operation_date.Value) == 0 // Que sean de la misma fecha                                                        
                                                                  && ri.Report.destination_id == negocio // Que sea del mismo negocio
                                                                  && ri.Boundary.is_active // Que la frontera esté activa
                                                            ).Select(ri => ri.Boundary).ToList();
            return okBoundaries;
        }


        /// <summary>
        /// Se devuelve lo que se requiere para actualizar la pantalla de monitor (resumen).
        /// Son principalmente 4 estadísticas:
        /// - Llamadas que no se han realizado aún
        /// - Llamadas en proceso
        /// - Llamadas fallidas
        /// - Llamadas con reintento
        /// - Llamadas completadas
        /// </summary>
        /// <param name="negocio">0: Generación; 1: Comercialización</param>
        /// <param name="opDay">Día de operación.</param>
        /// <param name="includesAll">0 para el resumen teniendo en cuenta solamente las interrogaciones en el presente día de operación.</param>
        /// <returns></returns>
        /// Debe hacerse otro método que reúna las estadísticas por rango. Es decir, semanal, mensual, incluso anual.
        /// Se parte desde 
        public static List<List<int>> summaryOfCalls(DateTime opDay, int? negocio = null, bool includesAll = true)
        {
            http://localhost:50684/api/Stats/summaryOfCalls?opDay=04-04-2019&negocio=1&includesAll=true
            // Include all
            // false: Se distingue cuando las fronteras registran no registran medidas en cierto día, pero sí en otro. 
            // true: (Preferida) Cuando se incluye todas las interrogaciones; Es decir, si la frontera sí registra medidas en cualquiera de los lotes de interrogaciones, así se mostrará en general.
            List<Boundary> boundariesAll = new List<Boundary>();
            List<List<int>> listsOfBoundaries = new List<List<int>>()
            {
                new List<int>(), // 1 = Programmed
                new List<int>(), // 2 = Processing
                new List<int>(), // 3 = Error
                new List<int>(), // 4 = Advertencia, retry
                new List<int>(), // 5 = Exitoso
                new List<int>(), // 6 = Not even scheduled
            };

            // Se crean los objetos que van a guardar las medidas
            var boundQuery = db.Report_Inform.AsNoTracking().Where(ri => opDay.CompareTo(ri.Report.operation_date.Value) == 0 // Que sean de la misma fecha
                                                           && ri.Boundary.is_active // Que la frontera esté activa
                                                           );

            List<Report_Inform> query = new List<Report_Inform>();
            if (negocio != null) //Se escoge un negocio en particular. Sino, no se distingue y se incluyen todos
            {
                query = boundQuery.Where(ri => ri.Report.destination_id == negocio).ToList(); // Que sea del mismo negocio);
                boundariesAll = db.Boundary.Where(b => b.is_active && b.report_type_id == negocio).ToList(); // Lista de todas las fronteras
            }
            else
            {
                query = boundQuery.ToList();
                boundariesAll = db.Boundary.Where(b => b.is_active).ToList(); // Lista de todas las fronteras
            }
                                     
            var allRisByStatus = query.GroupBy(b => b.Substatus.idStatus).ToList();
            
            List<int> boundariesIdAll = boundariesAll.Select(b => b.id).ToList(); // Lista de los ids de las fronteras, para hacer los procesos más rápido y no pasar entidades.

            if (includesAll)
            {
                foreach (var riGroup in allRisByStatus)
                {
                    switch (riGroup.Key)
                    {
                        case 1: listsOfBoundaries[0] = riGroup.Select(ri => ri.boundary_id.Value).ToList(); break; // Programado
                        case 2: listsOfBoundaries[1] = riGroup.Select(ri => ri.boundary_id.Value).ToList(); break; // En proceso
                        case 4: listsOfBoundaries[3] = riGroup.Select(ri => ri.boundary_id.Value).ToList(); break; // Advertencia
                        case 6: //En cola. Se integran con los programados
                            listsOfBoundaries[0].AddRange(riGroup.Select(ri => ri.boundary_id.Value));
                            listsOfBoundaries[0] = listsOfBoundaries[0].ToList()
                                ; break; // Los que están en cola aparecen como programados
                        case 5: listsOfBoundaries[4] = riGroup.Select(ri => ri.boundary_id.Value).ToList(); break; // Completados exitosamente
                        default:
                            break;
                    }
                }
                for (int i = 0; i < listsOfBoundaries.Count; i++)
                    listsOfBoundaries[i] = listsOfBoundaries[i].Distinct().ToList();

               // listsOfBoundaries.ForEach(l => l = l.Distinct().ToList()); // Pick only one of each boundary in each list

                // El caso de los errores (status=3) es especial, porque puede haber muchos informes que tengan errores, pero que luego se volvieron a interrogar. 
                // Por esta razón, serán seleccionados solamente los que queden después de exlcuir el resto de informes con otros status.

                List<int> remainingBoundaries = boundariesIdAll;
                foreach (var listOfBoundaries in listsOfBoundaries)
                    remainingBoundaries = remainingBoundaries.Except(listOfBoundaries).ToList();

                var errorsTemp = allRisByStatus.Where(g => g.Key == 3).FirstOrDefault(); // Reportes con errores provisional
                
                if (errorsTemp != null) // Si sí existen errores
                {
                    // Las fronteras sobrantes pueden tener fronteras con error, pero otras que ni siquiera se han programado.
                    // Las fronteras de error (temporal) pueden tener fronteras que ya están en otras listas. 
                    // Lo que se busca es la intersección de las dos: Las fronteras con error que no están en las demás listas.
                    listsOfBoundaries[2] = remainingBoundaries.Intersect(errorsTemp.Select(ri => ri.boundary_id.Value).ToList()).ToList();
                }
                listsOfBoundaries[5] = remainingBoundaries.Except(listsOfBoundaries[2]).ToList(); // Las fronteras que no se encuentran en ningún otro grupo. Not even scheduled.
            }
            else
            {
                // Pendiente por desarrollar
            }
            return listsOfBoundaries;
        }

        [HttpGet]
        public static List<Object> summarySimplified(string opday, int? negocio = null, bool includesAll = true)
        {
            List<List<int>> listsOfBoundaries = summaryOfCalls(DateTime.Parse(opday), negocio, includesAll);
            List<Boundary> boundariesAll = new List<Boundary>();

            if (negocio != null) //Se escoge un negocio en particular. Sino, no se distingue y se incluyen todos
                boundariesAll = db.Boundary.Where(b => b.is_active && b.report_type_id == negocio).ToList(); // Lista de todas las fronteras
            else boundariesAll = db.Boundary.Where(b => b.is_active).ToList(); // Lista de todas las fronteras

            //Se crea el objeto de objetos que se retorna a la página, organizado solamente con los datos necesarios.
            List<Object> returnObj = new List<object>();

            for (int i = 1; i <= listsOfBoundaries.Count; i++)
            {
                List<Object> objs = new List<object>();

                foreach (var b_id in listsOfBoundaries[i - 1]) // b_id = boundaryId
                {
                    Boundary b = boundariesAll.Find(bound => bound.id == b_id);
                    var obj = new { b.code, b.name, tipo = b.report_type_id };
                    objs.Add(obj);
                }
                returnObj.Add(new { statusCat = i, fronteras = objs }); // statusCat (Categoría) es similar a status, pero no es lo mismo. Se pone esta distinción para los casos su uso es estadístico
            }

            return returnObj;
        }

        public static List<Object> SimplifiedSummaryPie (string opDate = "", int? negocio = 1, int quantity = 1, bool includesAll = true)
        {

            // The order is very important!
            string[,] statusCatsAndColors = new string[,] {
                                { "Programadas"   , "#7ebde5" },
                                { "Procesando"    , "#418cf4" },
                                { "Error"         , "#ea5263" },
                                { "Reintentos"    , "#ffa551" },
                                { "Completadas"   , "#55cc5d" },
                                { "No programadas", "#b7b7b7" },};

            List<Boundary> boundariesDB = db.Boundary.ToList();
            

            // Día de operación
            DateTime opdate = new DateTime();
            if (opDate == "") opdate = DateTime.Now.Date.AddDays(-1); // Si no se envía parámetros, la fecha por defecto es el día de ayer.
            else opdate = DateTime.Parse(opDate);

            List<List<int>> boundariesIdsLists = summaryOfCalls(opdate, negocio, includesAll);

            List<Object> dataPie = new List<object>();
            int boundsTotal = 0;
            // Mapea el objeto de ints en el objeto para realizar las medidas.
            for (int i = 0; i < boundariesIdsLists.Count; i++)
            {
                List<int> boundList = boundariesIdsLists[i];
                boundsTotal += boundList.Count;

                // data for pie chart
                dataPie.Add(new { name = statusCatsAndColors[i, 0], y = boundList.Count, color = statusCatsAndColors[i, 1] });
            }
            
            return dataPie;

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
