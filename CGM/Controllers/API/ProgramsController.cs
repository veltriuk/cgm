﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Helpers.Attributes;
using CGM.Models.CGMModels;
using Hangfire;

namespace CGM.Controllers.API
{
    public class ProgramsController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Programs
        public IQueryable<Program> GetProgram()
        {
            return db.Program;
        }

        // GET: api/Programs/5
        [ResponseType(typeof(Program))]
        public async Task<IHttpActionResult> GetProgram(int id)
        {
            Program program = await db.Program.FindAsync(id);
            if (program == null)
            {
                return NotFound();
            }

            return Ok(program);
        }

        
        // POST: api/Programs
        [ResponseType(typeof(Program))]
        public async Task<IHttpActionResult> PostProgram(Program program)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Program.Add(program);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = program.id }, program);
        }

        // DELETE: api/Programs/5
        [ResponseType(typeof(Program))]
        public async Task<IHttpActionResult> DeleteProgram(int id)
        {
            Program program = await db.Program.FindAsync(id);
            if (program == null)
            {
                return NotFound();
            }

            db.Program.Remove(program);
            await db.SaveChangesAsync();

            return Ok(program);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //http://localhost:50684/api/Programs/getSuggestedName?destination=1
        public string getSuggestedName(int destination)
        {
            string destString = "";
            if (destination == 0)
            { destString = "Adquisición (medidores)"; }
            if (destination == 1)
            { destString = "Adquisición (fronteras)"; }
            if (destination == 2)
            { destString = "Reporte (fronteras)"; }

            List<Program> programs = db.Program.ToList();
            if (programs.Count != 0)
            {
                int iden = programs.Where(f => f.destination_id == destination).Count() + 1;

                string name = destString + " " + iden;
                foreach (var item in programs)
                {
                    if (programs.Any(f => f.name.Equals(name.Trim())))
                        iden++;
                    else
                        break;
                    name = destString + " " + iden;
                }

                return name;
            }
            else
            {
                return destString + " 1";
            }
        }

        [HttpPost]
        public string TriggerProgram(int id)
        {
            RecurringJob.Trigger(id.ToString());
            return "Triggered";
        }

    }
}