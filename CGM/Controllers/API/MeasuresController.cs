﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;

namespace CGM.Controllers.API
{
    public class MeasuresController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Measures
        public IQueryable<Measure> GetMeasure()
        {
            return db.Measure;
        }

        // GET: api/Measures/5
        [ResponseType(typeof(Measure))]
        public async Task<IHttpActionResult> GetMeasure(int id)
        {
            Measure measure = await db.Measure.FindAsync(id);
            if (measure == null)
            {
                return NotFound();
            }

            return Ok(measure);
        }

        // PUT: api/Measures/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMeasure(int id, Measure measure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != measure.id)
            {
                return BadRequest();
            }

            db.Entry(measure).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeasureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Measures
        [ResponseType(typeof(Measure))]
        public async Task<IHttpActionResult> PostMeasure(Measure measure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Measure.Add(measure);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = measure.id }, measure);
        }

        // DELETE: api/Measures/5
        [ResponseType(typeof(Measure))]
        public async Task<IHttpActionResult> DeleteMeasure(int id)
        {
            Measure measure = await db.Measure.FindAsync(id);
            if (measure == null)
            {
                return NotFound();
            }

            db.Measure.Remove(measure);
            await db.SaveChangesAsync();

            return Ok(measure);
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeasureExists(int id)
        {
            return db.Measure.Count(e => e.id == id) > 0;
        }
    }
}