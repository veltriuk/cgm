﻿Grafica1_columnas = (data, numFront) => {

    let d = new Array()
    let dri = new Array()

    for (i = 0; i < data.length; i++) {

        //ACTIVA

        let driData = new Array

        d.push({
            "name": data[i].boundary.name,
            "drilldown": data[i].boundary.id,
            "y": data[i].sumatoria_activa
        })

        for (f = 0; f < data[i].measure_activa.length; f++) {
            driData.push([data[i].measure_activa[f].measure_date, data[i].measure_activa[f].measure])
        }

        dri.push({
            "name": data[i].boundary.name,
            "id": data[i].boundary.id,
            "data": driData
        })

        //REACTIVA
        if (numFront == 1) {

            let driData_R = new Array

            d.push({
                "name": data[i].boundary.name + " - REACTIVA",
                "drilldown": data[i].boundary.id + "-r",
                "y": data[i].sumatoria_reactiva
            })

            for (f = 0; f < data[i].measure_reactive.length; f++) {
                driData_R.push([data[i].measure_reactive[f].measure_date, data[i].measure_reactive[f].measure])
            }

            dri.push({
                "name": data[i].boundary.name + " - REACTIVA",
                "id": data[i].boundary.id + "-r",
                "data": driData_R
            })

        }
    }

    Highcharts.chart('grafica1', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Informe de Mediciones de Fronteras (' + $("#fechaIni").val() + ' - ' + $("#fechaFin").val() + ')'
        },
        subtitle: {
            text: 'Información basada en cosnumo por dia - frontera, de clic en el valor para ver mas detalladamente la medición <br> (Si la frontera no aparece es porque tiene de valor 0)'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total consumo por frontera'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y: .1f}'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y: .2f}</b> <br />'
        },
        "series": [
            {
                "name": "Fronteras",
                "colorByPoint": true,
                "data": d
            }
        ],
        "drilldown": {
            "series": dri
        }
    });
}

Grafica2_torta = (data, numFront) => {

    let total = 0

    // ACTIVA
    for (i = 0; i < data.length; i++) {
        total += data[i].sumatoria_activa
    }

    // REACTIVA
    if (numFront == 1) { total += data[0].sumatoria_reactiva }

    let d = new Array()

    //ACTIVA
    for (i = 0; i < data.length; i++) {

        let por = intlRound(((data[i].sumatoria_activa * 100) / total))

        d.push([
            data[i].boundary.name,
            parseFloat(por),
            false])
    }

    // REACTIVA
    if (numFront == 1) {

        let por_R = intlRound(((data[0].sumatoria_reactiva * 100) / total))

        d.push([
            data[0].boundary.name + ' - REACTIVA',
            parseFloat(por_R),
            false])
    }

    Highcharts.chart('grafica2', {

        title: {
            text: 'Porcentaje de consumos - Torta (' + $("#fechaIni").val() + ' - ' + $("#fechaFin").val() + ')'
        },
        series: [{
            type: 'pie',
            allowPointSelect: true,
            keys: ['name', 'y', 'selected', 'sliced'],
            data: d,
            showInLegend: true
        }]
    });
}

Grafica3_diamante = (data, numFront) => {

    let nombres = new Array()
    let minimos = new Array()
    let maximos = new Array()

    for (i = 0; i < data.length; i++) {
        nombres.push(data[i].boundary.name)
        maximos.push(data[i].boundary.max_active)
        minimos.push(data[i].boundary.min_active)
    }

    if (numFront == 1) {
        nombres.push(data[0].boundary.name + ' - REACTIVA')
        maximos.push(data[0].boundary.max_reactive)
        minimos.push(data[0].boundary.min_reactive)
    }

    Highcharts.chart('grafica3', {

        chart: {
            polar: true,
            type: 'line'
        },

        title: {
            text: 'Minimos vs Maximos (' + $("#fechaIni").val() + ' - ' + $("#fechaFin").val() + ')',
            x: -80
        },

        pane: {
            size: '90%'
        },

        xAxis: {
            categories: nombres,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.2f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: [{
            name: 'Valor Minimo',
            data: minimos,
            pointPlacement: 'on'
        }, {
            name: 'Valor Maximo',
            data: maximos,
            pointPlacement: 'on'
        }]

    });

}

Grafica4_stock = (data, numFront) => {

    var maxRate = {}, minRate = {}
    var seriesOptions = [];

    for (i = 0; i < data.length; i++) {

        let dataAux = []
        let fechaMilSeg = 0

        for (f = 0; f < data[i].measure_activa.length; f++) {

            let dateForm = data[i].measure_activa[f].measure_date.split("/")
            let tmpDate = new Date(dateForm[2] + "-" + dateForm[1] + "-" + dateForm[0])
            //let tmpDate = new Date(dateForm[0])
            fechaMilSeg = tmpDate.getTime();

            dataAux.push([fechaMilSeg, data[i].measure_activa[f].measure])

        }

        seriesOptions.push({ name: data[i].boundary.name, data: dataAux, showInNavigator: false })

    }

    if (numFront == 1) {

        var maxRate =
        {
            value: data[0].boundary.max_measure_active,
            color: 'red',
            dashStyle: 'shortdash',
            width: 3,
            label: {
                text: `Rango Maximo (${data[0].boundary.max_measure_active})`
            }
        }

        var minRate =
        {
            value: data[0].boundary.min_measure_active,
            color: 'green',
            dashStyle: 'shortdash',
            width: 3,
            label: {
                text: `Rango Minimo (${data[0].boundary.min_measure_active})`
            }
        }

        let dataAux_R = []
        let fechaMilSeg_R = 0

        for (f = 0; f < data[0].measure_reactive.length; f++) {

            let dateForm = data[0].measure_reactive[f].measure_date.split("/")
            let tmpDate_R = new Date(dateForm[2] + "-" + dateForm[1] + "-" + dateForm[0])
            fechaMilSeg_R = tmpDate_R.getTime();

            dataAux_R.push([fechaMilSeg_R, data[0].measure_reactive[f].measure])

        }

        seriesOptions.push({ name: data[0].boundary.name + ' - REACTIVA', data: dataAux_R, showInNavigator: false })

    }

    Highcharts.stockChart('grafica4', {

        rangeSelector: {
            selected: 1
        },

        yAxis: {
            labels: {
                formatter: function () {
                    return (this.value > 0 ? ' + ' : '') + this.value;
                }
            },
            plotLines: [{
                value: 0,
                width: 2,
                color: 'silver'
            }, maxRate, minRate]
        },

        legend: {
            enabled: true
        },

        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            valueDecimals: 2,
            split: true
        },

        series: seriesOptions
    });
}

// =================================== Methods ===========================================

function intlRound(numero, decimales = 2, usarComa = false) {
    var opciones = {
        maximumFractionDigits: decimales,
        useGrouping: false
    };
    usarComa = usarComa ? "es" : "en";
    let ret = new Intl.NumberFormat(usarComa, opciones).format(numero);
    return parseFloat(ret)
}

Highcharts.setOptions({
    lang: {
        months: [
            'Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
        ],
        weekdays: [
            'Domingo', 'Lunes', 'Martes', 'Miércoles',
            'Jueves', 'Viernes', 'Sábado'
        ]
    }
});