﻿using CGM.Controllers.CGM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using CGM.Models.CGMModels;
using NLog;

namespace CGM
{
    
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters
                .Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter); 
            Database.SetInitializer<CGM_DB_Entities>(null);

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                USER.CLIENT = db.Config_CM.Where(c => c.element.Equals("CLIENT")).FirstOrDefault().text;
                USER.ROLES = db.AspNetRoles.Select(a => a.Name).ToArray();
                USER.URIBASE = db.Config_CM.Where(c => c.element.Equals("currentUriBase")).FirstOrDefault().text;
            }

        }

        protected void Application_PreRequestHandlerExecute() //No funciona!!! :@@@ //Se supone que iba a poner los erres de formulario en español
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-ES", true); //Sirve para hacer parsing datetime también
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-ES");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            Exception ex = Server.GetLastError();
            RouteData route = new RouteData();
            Response.Clear();
            if (ex.Message.Contains("browserLink")) //Errores para ignorar
            {
                route.Values.Add("controller", "Home");
                route.Values.Add("action","Index");
                return;
            }
            else
                {
                logger.Error(ex, "Non-handled error catch by global configuration.");
                //ErrorHandlerController.LoggerDB(ex);

                route.Values.Add("controller", "ErrorHandler");
                switch (ex.GetType().Name)
                {
                    case "NullReferenceException":
                        route.Values.Add("action", "Error500");
                        break;

                    case "HttpException":
                        HttpException httpEx = ex as HttpException;
                        if (httpEx != null)
                        {
                            switch (httpEx.GetHttpCode())
                            {
                                case 401:
                                    route.Values.Add("action", "Error401");
                                    break;
                                case 404:
                                    route.Values.Add("action", "Error404");
                                    break;
                                case 500:
                                    route.Values.Add("action", "Error500");
                                    break;
                                default:
                                    route.Values.Add("action", "Error500");
                                    break;
                            }
                        }
                        break;

                    default:
                        route.Values.Add("action", "Error500");
                        break;
                }
            }

            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;
            Response.ContentType = "text/html";
            IController errorController = new ErrorHandlerController();

            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), route));

        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            CultureInfo culture = new CultureInfo("es-CO");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }
    }
}
