﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Helpers;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using CGM.Areas.SurveyApp.Models;
using CGM.Models.MyModels;
using System.Threading.Tasks;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Areas.Telemetry.Models;
using CGM.Helpers.Attributes;
using NLog;
using System.IO;
using CGM.Controllers.General;
using System.Text;

namespace CGM.Areas.Telemetry.Controllers
{
    public class MeterController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static List<Tuple<string, Meter, List<ParamAnswer>>> meterRowData = new List<Tuple<string, Meter, List<ParamAnswer>>>();


        // GET: Meter
        public ActionResult Index()
        {
            ViewBag.ControllerName = "Meter";
            ViewBag.Result = TempData["result"];
            List<Meter> meters = db.Meter.Include(m => m.Model_Meter).Where(m => !m.is_filed).ToList();

            var parametersAll = SelectHelper.getMeterParameters(meters.Select(m => m.id).ToArray(), false); 

            foreach (var meter in meters)
            {
                meter.parameters = parametersAll.Where(p => p.Item1 == meter.id).FirstOrDefault().Item2; // item1 = meter_id
            }

            ViewBag.emptyModels = db.Model_Meter.Count() == 0 ? 1 : 0;
            return View(meters);
        }

        // GET: Meter/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.ControllerName = "Meter";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meter meter = db.Meter.Find(id);
            if (meter == null)
            {
                return HttpNotFound();
            }
            return View(meter);
        }

        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        // GET: ParamFormInstances/Edit/5
        public async Task<ActionResult> Configuration(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meter meter = await db.Meter.FindAsync(id);
            if (meter == null)
            {
                return HttpNotFound();
            }
            return View(meter);
        }

        // GET: Meter/Create
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create()
        {
            ViewBag.ControllerName = "Meter";
            ViewBag.model_meter_id = GetDropDownListCustom(null);
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("_Create");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Create([Bind(Include = "id,serial,multiplicative_factor,info,model_meter_id")] Meter meter)
        {
            meter.is_backup = null;
            var listMeters = db.Meter.Where(m => !m.is_filed).Select(m => m.serial.Trim());
            if (listMeters.Contains(meter.serial.Trim()))
            {
                ViewBag.Error = "El medidor con número serial " + meter.serial + " ya existe o es nulo";
                ViewBag.model_meter_id = GetDropDownListCustom(null);
                return View(meter);
            }

            ViewBag.ControllerName = "Meter";
            if (ModelState.IsValid)
            {
                db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas

                ParamFormInstance formInstance = new ParamFormInstance() { paramForm_id = 1 }; // 1: Configuración de 
                Model_Meter model = db.Model_Meter.Find(meter.model_meter_id);
                var modelAns = new ParamAnswer() { answerOption_id = db.ParamAnswerOptions.Where(a => a.name == model.model).FirstOrDefault().id };
                var protocolAns = new ParamAnswer() { answerOption_id = db.ParamAnswerOptions.Where(a => a.alias == model.defaultProtocol).FirstOrDefault().id };

                formInstance.Answers = new List<ParamAnswer>() { modelAns, protocolAns };

                meter.FormConfiguration = formInstance;
                db.Meter.Add(meter); //Se registra el medidor en DB
                db.SaveChanges();

                DataAndDB.insertEvents(meter, 1, meter.id.ToString(), meter.GetType().BaseType.Name); //1= Creación, en Event_Category
                return RedirectToAction("Index");
            }
            ViewBag.model_meter_id = GetDropDownListCustom(null);

            return View(meter);
        }

        // GET: Meter/Edit/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Edit(int? id)
        {
            ViewBag.model_meter_id = GetDropDownListCustom(id);
            ViewBag.ControllerName = "Meter";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meter meter = db.Meter.Find(id);

            if (meter == null)
            {
                return HttpNotFound();
            }
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("~/Areas/Telemetry/Views/Meter/_Edit.cshtml", meter);
            }
            return View(meter);
        }

        // POST: Meter/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Edit([Bind(Include = "id,serial,multiplicative_factor,info,model_meter_id, Model_Meter,formConfiguration_id, is_backup, boundary_id")] Meter meter)
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas

            ViewBag.ControllerName = "Medidor";
            if (meter.boundary_id == null)
                meter.is_backup = null;
            if (ModelState.IsValid)
            {
                Meter previousObj = db.Meter.Include(m => m.Model_Meter).AsNoTracking().First((m => m.id == meter.id)); //Se hace una copia del elemento original antes de cambiarlo
                meter.Model_Meter = db.Model_Meter.AsNoTracking().First(m => m.id == meter.model_meter_id); //Se añade los datos del Modelo de medidor, que por defecto no se añade

                #region Cambiar la información del formulario 
                // Cambiar la información del formulario cuando se cambia el modelo. Este código se debe cambiar en el futuro, pues la estructura es difícil de mantener
                if (meter.model_meter_id != previousObj.model_meter_id)
                {
                    var modeloptionsIds = db.ParamAnswerOptions.Where(a => a.Question.name == "Modelo de medidor").Select(a => a.id).ToList();
                    var protocoloptionsIds = db.ParamAnswerOptions.Where(a => a.Question.name == "Protocolo").Select(a => a.id).ToList();
                    if (modeloptionsIds.Count != 0)
                    {
                        ParamAnswer modelAnswer = db.ParamAnswers.Where(a => modeloptionsIds.Contains(a.answerOption_id) && a.formInstance_id == meter.formConfiguration_id).FirstOrDefault();
                        db.ParamAnswers.Remove(modelAnswer);
                        ParamAnswer protocolAnswer = db.ParamAnswers.Where(a => protocoloptionsIds.Contains(a.answerOption_id) && a.formInstance_id == meter.formConfiguration_id).FirstOrDefault();
                        db.ParamAnswers.Remove(modelAnswer);

                        // create a different value just in case
                        ParamAnswer modelAnswerNew = new ParamAnswer { answerOption_id = db.ParamAnswerOptions.Where(a => a.alias == meter.Model_Meter.model).FirstOrDefault().id, formInstance_id = meter.formConfiguration_id.Value };
                        db.ParamAnswers.Add(modelAnswerNew);

                        ParamAnswer protocolAnswerNew = new ParamAnswer { answerOption_id = db.ParamAnswerOptions.Where(a => a.alias == meter.Model_Meter.defaultProtocol).FirstOrDefault().id, formInstance_id = meter.formConfiguration_id.Value };
                        db.ParamAnswers.Add(protocolAnswerNew);
                    }
                    else
                    {
                        throw new Exception("[CGM] No se obtuvo resultados de la lista de Modelo de Medidor, u otro error relacionado en la edición de los parámetros del medidor. Verificar consistencia de formulario con base de datos");
                    }
                }
                #endregion

                db.Entry(meter).State = EntityState.Modified;
                db.SaveChanges();
                DataAndDB.insertEvents(new { elementoPrevio = previousObj, elementoModificado = meter, cambios = OtherMethods.getObjectChangesFromDB(previousObj, meter) }, 2, meter.id.ToString(), meter.GetType().BaseType.Name); //2 = Modificación, en Event_Category
                return RedirectToAction("Index");
            }
            ViewBag.model_meter_id = GetDropDownListCustom(null);
            return View();
        }
        public ActionResult EditPartial(int? id, int? boundary_id, bool? is_backup)
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas
            Meter meter = db.Meter.First((m => m.id == id)); //Se hace una copia del elemento original antes de cambiarlo
            if (meter.boundary_id == null)
            {
                meter.is_backup = null;
            }
            meter.boundary_id = boundary_id;
            meter.is_backup = is_backup;
            if (ModelState.IsValid)
            {
                db.Entry(meter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.model_meter_id = GetDropDownListCustom(null);
            return RedirectToAction("Index");
        }

        // GET: Meter/Delete/5
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult Delete(int? id)
        {
            ViewBag.ControllerName = "Medidor";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meter meter = db.Meter.Find(id);
            if (meter == null)
            {
                return HttpNotFound();
            }
            return View(meter);
        }

        /// <summary>
        /// The meter does not really gets deleteted, but archived
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // POST: Meter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.ControllerName = "Medidor";
            Meter meter = db.Meter.Find(id);
            
            meter.is_filed = true; /// The meter does not really gets deleteted, but archived, because there are many dependent measures with it.
            meter.boundary_id = null; // Desasociar la frontera en la que está enlazado

            db.Entry(meter).State = EntityState.Modified;
            db.SaveChanges();

            DataAndDB.insertEvents(meter, 3, meter.id.ToString(), meter.GetType().BaseType.Name); //3 = Eliminación, en Event_Category
            return RedirectToAction("Index");
        }

        public FileResult DownloadConfiguration()
        {
            List<Tuple<Meter, Dictionary<string, string>>> meterConfigs = new List<Tuple<Meter, Dictionary<string, string>>>();

            //List<Dictionary<string, string>> meterConfigs = new List<Dictionary<string, string>>(); ;

            // Get the individual configuration of each meter. (UseS the same method of the API).
            List<Meter> meters = db.Meter.Where(m => !m.is_filed).Include(m => m.Model_Meter).Include(m => m.Boundary).ToList();
            List<Meter> metersIncluded = new List<Meter>();
            foreach (var item in meters)
            {
                var meterParams = SelectHelper.getMeterParameters(item.id, false);
                if (meterParams != null)
                {
                    meterConfigs.Add(new Tuple<Meter,Dictionary<string,string>>(item,meterParams));
                }
            }

            // Get the names of questions (they would be on the titles)
            // <alias, name>
            List<Tuple<string, string>> questionTitles = new List<Tuple<string, string>>() {
                                                                    new Tuple<string, string>("SERIE","SERIE"),
                                                                    new Tuple<string, string>("SIC","SIC"),
                                                                    new Tuple<string, string>("MODELO","MODELO"),
                                                                    new Tuple<string, string>("RESPALDO","RESPALDO")
                                                                    }; // Hay que hacerlo manualmente

            questionTitles.AddRange(ParamFormsController.getQuestionTitles());
            questionTitles.Remove(new Tuple<string, string>("modelo_medidor", "Modelo de medidor")); // Ya se incluye el principal arriba
            questionTitles.Remove(new Tuple<string, string>("protocolo", "Protocolo")); // Ya se incluye el principal arriba
            

            List<string> linesStr = new List<string>();
            linesStr.Add(string.Join(",", questionTitles.Select(tup => tup.Item2).ToList()));

            List<string> lines = new List<string>();
            Meter meter;
            string line = "";
            foreach (Tuple<Meter,Dictionary<string, string>> meterConfig in meterConfigs) // do noit include the items manualy defined
            {
                lines = new List<string>();
                meter = meterConfig.Item1;

                // Parámetros manejados manualmente
                //1. SERIAL
                lines.Add(meter.serial.Trim());

                //2. BOUNDARY
                if (meter.Boundary != null) lines.Add(meter.Boundary.code);
                else lines.Add("");

                //3. MODELO
                lines.Add(meter.Model_Meter.model);

                //4. RESPALDO
                if (meter.is_backup != null) // Si no es nulo, añadir la P o R como corresponda.
                    if (meter.is_backup.Value)
                        lines.Add("R");
                    else
                        lines.Add("P");
                else
                    lines.Add("");

                foreach (var title in questionTitles.Skip(4))
                {
                    if (meterConfig.Item2.ContainsKey(title.Item1)) // If the key exists (that parameter has been configured)
                    {
                        line = meterConfig.Item2[title.Item1]; // get the parameter with alias
                    }
                    else
                    {
                        line = "";
                    }
                    lines.Add(line);
                }
                linesStr.Add(string.Join(",", lines));
            }

            //string filePath = "~//Files//Temp//";

            string fileName = "ConfiguracionMedidores_" + DateTime.Now.ToShortDateString().Replace('/', '-')+".csv";

            //FileManagement.WriteTXT(string.Join("\n", linesStr), System.Web.HttpContext.Current.Server.MapPath(filePath+fileName));

            return File(Encoding.Unicode.GetBytes(string.Join("\n", linesStr)), "text/plain", fileName);
        }

        #region Load Configuration
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult LoadConfiguration()
        {
            return View();
        }

        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult LoadConfigurationPreview()
        {
            string[] values = new string[] { };
            List<Boundary> BoundaryList = db.Boundary.ToList();
            int controw = 0;
            List<Tuple<string, VMeter>> vMeterRowData = new List<Tuple<string, VMeter>>();

            try
            {
                // Usually only one file
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];

                    string path = Path.Combine(Server.MapPath("~/Files/Config/Meter/"),
                                    Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    List<string> lines = FileManagement.readFileLines(path).ToList();
                    string[] line_titles = lines[0].Split(',');
                    // Get Answers objects from the cells
                    Cells2Data f2a0 = new Cells2Data(0, line_titles); // No radio buttons
                    Cells2Data f2a1 = new Cells2Data(1, line_titles); // Radio buttons

                    // Meter information // The definitions of these strings appear inside the class as well. If a change is needed, hace to be changed inside as well
                    Cells2Data metersData = new Cells2Data(5, line_titles , new string[] { "MODELO", "SERIE", "SIC", "RESPALDO" });

                    // This is the varible that contains all relevant information
                    meterRowData = new List<Tuple<string, Meter, List<ParamAnswer>>>();

                    List<string> errors = new List<string>();
                    controw = 1;
                    // Iterate over all the rows
                    foreach (var line in lines.Skip(1)) // First line is the titles
                    {
                        
                        try
                        {
                            var lineItems = line.Replace("\0", "").Split(',');

                            List<ParamAnswer> answers = new List<ParamAnswer>();
                            Tuple<string, object> meterTuple = metersData.getObject(lineItems);

                            foreach (var colIndex in f2a0.index2Id.Keys)
                                answers.Add((ParamAnswer)f2a0.getObject(colIndex, lineItems[colIndex]).Item2);

                            foreach (var colIndex in f2a1.index2Id.Keys)
                                answers.Add((ParamAnswer)f2a1.getObject(colIndex, lineItems[colIndex]).Item2);

                            #region add model answer and protocol
                            if (meterTuple.Item1 == "new") // Existing meters do not modify their models, or protocols
                            {
                                var modelLoad = ((Meter)meterTuple.Item2).Model_Meter;
                                // Note that the value we are looking for it's on the alias!
                                ParamAnswer modelAnswer = new ParamAnswer() { answerOption_id = f2a1.answerOptions.Where(ao => ao.name == modelLoad.model).FirstOrDefault().id };
                                if(!answers.Any(a => a.answerOption_id == modelAnswer.answerOption_id)) answers.Add(modelAnswer);

                                ParamAnswer protocAnswer = new ParamAnswer() { answerOption_id = f2a1.answerOptions.Where(ao => ao.alias == modelLoad.defaultProtocol).FirstOrDefault().id };
                                if(!answers.Any(a => a.answerOption_id == protocAnswer.answerOption_id)) answers.Add(protocAnswer);
                                answers.Add(protocAnswer);
                            }
                            #endregion

                            meterRowData.Add(new Tuple<string, Meter, List<ParamAnswer>>(meterTuple.Item1, (Meter)meterTuple.Item2, answers)); // item1="new" || "existing";

                            controw++;
                        }
                        catch (Exception ex)
                        {
                            errors.Add("[CGM] Fila: " + controw + ". Error: " + ex.Message);
                            controw++;
                            continue;
                        }
                    }

                    // To build the table later
                    List<string> ansNames = new List<string>();

                    // Organize the information to display in the webpage

                    foreach (var tup in meterRowData)
                    {
                        VMeter vMeter = VMeter.parseMeter(tup.Item2);
                        vMeter.vAnswers = VAnswer.parseVAnswers(tup.Item3);
                        ansNames.AddRange(vMeter.vAnswers.Select(a => a.answerOptionName));
                        vMeterRowData.Add(new Tuple<string, VMeter>(tup.Item1, vMeter));
                    }
                    ViewBag.errors = errors;
                    ansNames = ansNames.Distinct().ToList();
                    ViewBag.ansNames = ansNames;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return PartialView("~/Areas/Telemetry/Views/Meter/_ConfigurationPreview.cshtml", vMeterRowData);
        }

        public ActionResult LoadConfigurationPost()
        {
            List<Meter> metersOriginal = db.Meter.Where(m => !m.is_filed).ToList();
            List<string> errors = new List<string>();
            if (meterRowData.Count != 0)
            {
                int cont = 0;
                string serial = "";
                try
                {
                    foreach (var tup in meterRowData)
                    {
                        //tup: item1:"new" or "existing"; item2:MeterObject; item3:List<Answers>
                        cont++;
                        serial = tup.Item2.serial;
                        if (tup.Item1 == "new")
                        {
                            Meter meter = tup.Item2;
                            meter.Model_Meter = null; // To avoid repetition of models
                            meter.Boundary = null; // To avoid repetition of boundaries. Somehow it adds new repeated elements
                            meter.FormConfiguration.Answers = tup.Item3;

                            db.Meter.Add(meter);
                        }
                        else if (tup.Item1 == "existing")
                        {
                            Meter meterInForm = tup.Item2;
                            Meter meter = metersOriginal.Find(m => m.id == meterInForm.id);

                            // If the meter already exists, editing the serial or the boundary where it's connected should not be an option

                            // For safety, remove all configuration parameters before.
                            // If I only replace them, doesn't work
                            // Gotta keep the model and the protocol and model! (Manually... :( )
                            var meteranswers = meter.FormConfiguration.Answers.ToList();

                            var ansmodel = meteranswers.Where(a => a.AnswerOption.Question.alias.Equals("modelo_medidor")).FirstOrDefault();
                            var ansprotocol = meteranswers.Where(a => a.AnswerOption.Question.alias.Equals("protocolo")).FirstOrDefault();

                            tup.Item3.Add(new ParamAnswer() { answerOption_id = ansmodel.answerOption_id });
                            tup.Item3.Add(new ParamAnswer() { answerOption_id = ansprotocol.answerOption_id});
                            //db.ParamAnswers.Remove(ansmodel);
                            //db.ParamAnswers.Remove(ansprotocol);
                            //meteranswers.Remove(ansmodel);

                            //var metersanswersremove  = meteranswers.Except(new List<ParamAnswer>() { ansmodel, ansprotocol });

                            //if(ansprotocol!=null) meteranswers.Remove(ansprotocol);



                            db.ParamAnswers.RemoveRange(meteranswers);
                            db.SaveChanges();

                            //var ansprotocolnew = tup.Item3.Where(i => i.answerOption_id == ansprotocol.answerOption_id).FirstOrDefault();
                            //if (ansprotocolnew != null) tup.Item3.Remove(ansprotocolnew);
                            meter.FormConfiguration.Answers = tup.Item3;
                        };
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    errors.Add("[CGM] Error en elemento con serial " + serial + ". Mensaje: " + ex.Message);
                }
            }
            if (errors.Count == 0)
            {
                TempData["result"] = new KeyValuePair<string, string>("true", "Carga de configuración realizada exitosamente.");
            }
            else
            {
                string errorStr = "";
                foreach (var error in errors)
                    errorStr += error + Environment.NewLine;

                TempData["result"] = new KeyValuePair<string, string>("warning", errorStr);
            }

            return Json(new { result = "Redirect", url = Url.Action("Index", "Meter", new { Area = "Telemetry" }) });
            //return RedirectToAction("Index");
        }

        public FileResult DownloadConfigTemplate() // Se llama desde 
        {
            return File("~/Files/System/PlantillaConfiguraciónMedidores.csv", "text/plain", "PlantillaConfiguraciónMedidores.csv");
        }

        #endregion

        //===============================================================
        //------------------------ Other Methods ------------------------
        //===============================================================

        // [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Desarrollador,Administrador")]
        public ActionResult DesasociarFrontera(int id)
        {
            Meter meter = db.Meter.Find(id);
            meter.boundary_id = null;
            meter.is_backup = null;
            db.SaveChanges();
            //            DataAndDB.insertEvents(meter, 3, meter.id.ToString(), meter.GetType().Name); //3 = Eliminación, en Event_Category
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult CreateALot() //Función de ejemplo para poblar la tabla de medidores
        {
            List<Meter> meters = new List<Meter>();
            int id = db.Meter.OrderByDescending(m => m.id).Select(m => m.id).FirstOrDefault();

            for (int i = 0; i < 50; i++)
            {
                id++;
                int serial = i * 1000;

                Meter newMeter = new Meter() { id = id, serial = serial.ToString(), multiplicative_factor = i * 15, is_backup = null, model_meter_id = 4 };

                newMeter.FormConfiguration = new ParamFormInstance() { paramForm_id = 1 }; // 1:Meter configuration form
                db.Meter.Add(newMeter); //Se registra el medidor en DB
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult CreateALotWithBoundaries()
        {
            List<Meter> meters = new List<Meter>();
            List<Boundary> boundaries = db.Boundary.ToList();
            int id = db.Meter.OrderByDescending(m => m.id).Select(m => m.id).FirstOrDefault();
            int i = 1;
            foreach (Boundary boundary in boundaries)
            {
                id++;
                int serial = i * 1000; i++;

                Meter newMeter = new Meter() { id = id, serial = serial.ToString(), multiplicative_factor = i * 15, is_backup = null, model_meter_id = 4 };

                newMeter.FormConfiguration = new ParamFormInstance() { paramForm_id = 1 }; // 1:Meter configuration form

                newMeter.boundary_id = boundary.id;
                newMeter.is_backup = false;
                db.Meter.Add(newMeter); //Se registra el medidor en DB
            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private SelectList GetDropDownListCustom(int? id) //Retorna la lista de modelos de medidor, seleccionando el que se encuentra en el medidor
        {
            CGM_DB_Entities db = new CGM_DB_Entities();

            List<SelectListItem> list = new List<SelectListItem>();
            SelectList lista;
            string tipoString = "";

            foreach (var item in db.Model_Meter)
            {
                list.Add(new SelectListItem() { Value = item.id.ToString(), Text = item.brand + " - " + item.model });
            }

            if (id != null)
            {
                int selectedValue = db.Meter.First(m => m.id == id).model_meter_id;
                lista = new SelectList(list, "Value", "Text", selectedValue);
                return lista;
            }
            else
            {
                list.Insert(0, new SelectListItem() { Value = "", Text = "--- Seleccione " + tipoString + " ---" });
                lista = new SelectList(list, "Value", "Text");
                return lista;
            }
        }
        /// <summary>
        /// Add form configuration for single time. This method is thought to be executed just once, to initialize the configurations.
        /// Later on, everytime a meter is added, the configuration form is added together.
        /// </summary>
        /// <returns></returns>
        public JsonResult AddFormConfigurationToMeters()
        {
            try
            {
                var meters = db.Meter.Where(m => !m.is_filed).ToList();
                int addedForms = 0;
                foreach (var meter in meters)
                {
                    ParamFormInstance formInstance = new ParamFormInstance()
                    {
                        paramForm_id = 1, // 1:Meter Configuration
                    };
                    if (meter.formConfiguration_id == null) // Si ya tiene
                    {
                        meter.FormConfiguration = formInstance;
                        addedForms++;
                    }
                }
                db.SaveChanges();
                return new JsonResult { Data = "Todo bien, todo bien! \n Número de medidores: " + meters.Count + "\n Formularios añadidos: " + addedForms, ContentType = "application/json", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception)
            {
                return Json("Hubo un error en el procedimiento!");
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

