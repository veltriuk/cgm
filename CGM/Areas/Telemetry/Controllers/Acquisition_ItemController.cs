﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;

namespace CGM.Areas.Telemetry.Controllers
{
    public class Acquisition_ItemController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Telemetry/Acquisition_Item
        public ActionResult Index()
        {
            ViewBag.ControllerName="Acquisition_Item";
            var acquisition_Item = db.Acquisition_Item.Include(a => a.Quantity).Include(a => a.Tag_Line);
            return View(acquisition_Item.ToList());
        }

        // GET: Telemetry/Acquisition_Item/Details/5
        public ActionResult Details(int? id)
        {
            ViewBag.ControllerName="Acquisition_Item";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition_Item acquisition_Item = db.Acquisition_Item.Find(id);
            if (acquisition_Item == null)
            {
                return HttpNotFound();
            }
            return View(acquisition_Item);
        }

        // GET: Telemetry/Acquisition_Item/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="Acquisition_Item";
            ViewBag.quantity_id = new SelectList(db.Quantity, "id", "name");
            ViewBag.tag_line_id = new SelectList(db.Tag_Line, "id", "name");
            return View();
        }

        // POST: Telemetry/Acquisition_Item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,tag_line_id,quantity_id,register_id,waveform_id,meter_events_id")] Acquisition_Item acquisition_Item)
        {
            ViewBag.ControllerName="Acquisition_Item";
            if (ModelState.IsValid)
            {
                db.Acquisition_Item.Add(acquisition_Item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.quantity_id = new SelectList(db.Quantity, "id", "name", acquisition_Item.quantity_id);
            ViewBag.tag_line_id = new SelectList(db.Tag_Line, "id", "name", acquisition_Item.tag_line_id);
            return View(acquisition_Item);
        }

        // GET: Telemetry/Acquisition_Item/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ControllerName="Acquisition_Item";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition_Item acquisition_Item = db.Acquisition_Item.Find(id);

            if (acquisition_Item == null)
            {
                return HttpNotFound();
            }        
            
            ViewBag.quantity_id = new SelectList(db.Quantity, "id", "name", acquisition_Item.quantity_id);
            ViewBag.tag_line_id = new SelectList(db.Tag_Line, "id", "name", acquisition_Item.tag_line_id);
            return View(acquisition_Item);
        }

        // POST: Telemetry/Acquisition_Item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,tag_line_id,quantity_id,register_id,waveform_id,meter_events_id")] Acquisition_Item acquisition_Item)
        {
            ViewBag.ControllerName="Acquisition_Item";
            if (ModelState.IsValid)
            {

                db.Entry(acquisition_Item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.quantity_id = new SelectList(db.Quantity, "id", "name", acquisition_Item.quantity_id);
            ViewBag.tag_line_id = new SelectList(db.Tag_Line, "id", "name", acquisition_Item.tag_line_id);
            return View(acquisition_Item);
        }

        // GET: Telemetry/Acquisition_Item/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.ControllerName="Acquisition_Item";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition_Item acquisition_Item = db.Acquisition_Item.Find(id);
            if (acquisition_Item == null)
            {
                return HttpNotFound();
            }
            return View(acquisition_Item);
        }

        // POST: Telemetry/Acquisition_Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewBag.ControllerName="Acquisition_Item";
            Acquisition_Item acquisition_Item = db.Acquisition_Item.Find(id);
            db.Acquisition_Item.Remove(acquisition_Item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult GetAcquisition_Items(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var acitems = db.Acquisition_Item.Include(ac => ac.Tag_Line).Include(ac => ac.Quantity).Where(a => a.Acquisitions.Any(ac => ac.id == id)).ToList();
            //var acitems = db.Acquisition.Where(a => a.id == id).Select(a => a.Acquisition_Items).ToList();
            return PartialView("~/Areas/Telemetry/Views/Reading/_AdqList.cshtml",acitems);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}

