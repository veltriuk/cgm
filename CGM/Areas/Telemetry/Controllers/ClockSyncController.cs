﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.Telemetry.Models;
using System.Threading;
using NLog;
using System.Net.Http;
using System.Net.Http.Headers;
using CGM.Helpers;
using Newtonsoft.Json;
using System.Text;

namespace CGM.Areas.Telemetry.Controllers
{
    public class ClockSyncController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Telemetry/ClockSyncs
        public async Task<ActionResult> Index()
        {
            ViewBag.ControllerName="Telemetry/ClockSyncs";
            var clockSync = db.ClockSync.Include(c => c.Meter).Include(c => c.Substatus);
            return View(await clockSync.ToListAsync());
        }

        // GET: Telemetry/ClockSyncs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.ControllerName="Telemetry/ClockSyncs";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClockSync clockSync = await db.ClockSync.FindAsync(id);
            if (clockSync == null)
            {
                return HttpNotFound();
            }
            return View(clockSync);
        }

        // GET: Telemetry/ClockSyncs/Create
        public ActionResult Create()
        {
            ViewBag.ControllerName="Telemetry/ClockSync";
            List<VMeter> meters = db.VMeter.ToList();
            return View(meters);
        }

        /// <summary>
        /// Crea una instrucción para sincronización de reloj. Envía la petición por medio del protocolo al dispositivo dependiendo del caso.
        /// </summary>
        /// <param name="serial">Número de serie del medidor</param>
        /// <param name="sync_time_exact">Fecha y hora exacta definida por el usuario para modo manual</param>
        /// <param name="sync_shift">Incremento o decremento de segundos (ex. para EPQS)</param>
        /// <param name="radioServerTime">0:Manual; 1:ServerTime</param>
        /// <returns></returns>
        // POST: Telemetry/ClockSyncs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(string serial, string sync_time_exact, string sync_shift, string radioServerTime)
        {
            // Make an HTTP QUery to the API, that returns a OK or BAD state regarding the synchronization process
            Meter meter = db.Meter.Where(m => m.serial.Trim().Equals(serial.Trim())).FirstOrDefault();
            int sync_type = 0; // 0:Server time; 1:Defined manually by user; 2:Time shift(ex. in EPQS)
            if (meter.Model_Meter.model == "EPQS") //
                sync_type = 2; // Time shift
            else
                sync_type  = int.Parse(radioServerTime);

            #region Create url from parameters
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("id", meter.id.ToString());
            parameters.Add("sync_type", sync_type.ToString());
            parameters.Add("sync_time_exact", sync_time_exact);
            parameters.Add("sync_shift", sync_shift);

            string urlParams = OtherMethods.CreateUrlWithParameters("api/Clock/Sync", parameters);
            #endregion

            List<KeyValuePair<string, string>> paramPairs = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("id", meter.id.ToString()),
                new KeyValuePair<string, string>("sync_type", sync_type.ToString()),
                new KeyValuePair<string, string>("sync_time_exact", sync_time_exact),
                new KeyValuePair<string, string>("sync_shift", sync_shift)
            };

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(db.Config_CM.Where(c => c.element.Trim().Equals("ipapi")).FirstOrDefault().text);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    logger.Info("Se envía solicitud de hora actual en medidor.");
                    //var response = client.GetAsync("api/Protocol/RunIndividualAcquisition" + "/?" +
                    //                                      "repinf_id=" + ri.id // Se agrega más argumentos con "&" 
                    //                                      );

                   // var stringContent = new StringContent(parameters, Encoding.UTF8, "application/json");

                    var content = new FormUrlEncodedContent(paramPairs);

                    var response2 = client.GetAsync(urlParams);
                    //var response = client.PostAsync("api/Clock/Sync", content);

                    //response.Wait();
                    response2.Wait();

                    //var result = response.Result;
                    var result2 = response2.Result;


                    //if (result.IsSuccessStatusCode)
                    //{
                    //    logger.Debug("Conexión con servicio interno exitosa.");
                    //    string r = await result.Content.ReadAsStringAsync();
                    //    var check = r.Split(new char[] { ',', '"' });
                    //    switch (statusDict[int.Parse(check[1])])
                    //    {
                    //        case 5: //Adquisición exitosa
                    //            return "Lectura realizada con éxito";
                    //        case 4: //Advertencia. Error que genera repetición
                    //            throw new CGMResultException("[" + ri.Report.operation_date.Value.ToShortDateString() + "] " + check[2]); //Se devuelve el mensaje de error, de modo que se haga reintentos.
                    //        case 3: //Error que no genera repetición, como una contraseña errada.
                    //                //Este tipo de error hace que la frontera no se vuelva a leer. Por esta razón no devuelve una excepción.
                    //            if (ri.job_id != null)
                    //            {
                    //                BackgroundJobClient jobclient = new BackgroundJobClient();
                    //                FailedState failedState = new FailedState(new CGMResultException("Error (no reintentar): " + check[2]));
                    //                var toFailed = jobclient.ChangeState(ri.job_id.Value.ToString(), failedState, ProcessingState.StateName);
                    //            }
                    //            return "Error de no repetición de adquisición. " + check[2];
                    //        default:
                    //            throw new CGMResultException(check[2]); //No entró a ninguno de los casos determinados, y se devuelve lo que quiera que se recibió, pero con reintento.
                    //    }
                    //}
                    //else
                    //{
                    //    logger.Warn("Conexión con servicio interno no exitosa.");
                    //    Exception ex = new Exception("CGM non handled Error " + result.RequestMessage.RequestUri, null);
                    //    throw ex;
                    //}
                    
                }
                catch (Exception ex)
                {

                    throw;
                }
            }

          //  DateTime syncTime = DateTime.Parse(sy);

            ClockSync clockSync = new ClockSync()
            {
                id_meter = db.Meter.Where(m => m.serial.Trim().Equals(serial)).FirstOrDefault().id,
                id_substatus = 100, // Programado para ejecución
                time_current = DateTime.Now, //El tiempo del reloj ahora. Debería cambiarse por el tiempo del medidor respecto a cierta hora fija.
               // time_assigned = syncTime, // Tiempo que se asigna al medidor
                sync_date = DateTime.Now // Estampa de tiempo de ahora, para registro
            };

            if (ModelState.IsValid)
            {
                db.ClockSync.Add(clockSync);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.id_meter = new SelectList(db.Meter, "id", "serial", clockSync.id_meter);
            return View(clockSync);
        }


        public ActionResult GetHour(string serial)
        {
            DateTime serverTime = DateTime.Now;
            DateTime meterTime= DateTime.Now.AddSeconds(-30); // Test
            Thread.Sleep(1000);

            TimeSpan desfase = serverTime.Subtract(meterTime);

            return Json(new { meterTime = meterTime.ToString(), serverTime = serverTime.ToString() , desfase=desfase.ToString()} , JsonRequestBehavior.AllowGet);
        }

        public double getJSDate(DateTime dateTime)
        {
            return dateTime.Subtract(new DateTime(1970,1,1)).TotalMilliseconds;
        }


        // GET: Telemetry/ClockSyncs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.ControllerName = "Telemetry/ClockSyncs";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClockSync clockSync = await db.ClockSync.FindAsync(id);
            if (clockSync == null)
            {
                return HttpNotFound();
            }
            return View(clockSync);
        }

        // POST: Telemetry/ClockSyncs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.ControllerName = "Telemetry/ClockSyncs";
            ClockSync clockSync = await db.ClockSync.FindAsync(id);
            db.ClockSync.Remove(clockSync);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

