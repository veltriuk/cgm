﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Areas.Telemetry.Models;
using CGM.Models.CGMModels;

namespace CGM.Areas.Telemetry.Controllers.API
{
    public class Acquisition_ItemController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Acquisition_Item
        public IQueryable<Acquisition_Item> GetAcquisition_Item()
        {
            
            return db.Acquisition_Item;

        }

        public List<Acquisition_Item> GetAcquisition_Items(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var acitems = db.Acquisition_Item.Include(ac => ac.Tag_Line).Include(ac => ac.Quantity).Where(a => a.Acquisitions.Any(ac => ac.id == id)).ToList();
            //var acitems = db.Acquisition.Where(a => a.id == id).Select(a => a.Acquisition_Items).ToList();
            return acitems;
        }

        // GET: api/Acquisition_Item/5
        [ResponseType(typeof(Acquisition_Item))]
        public async Task<IHttpActionResult> GetAcquisition_Item(int id)
        {
            Acquisition_Item acquisition_Item = await db.Acquisition_Item.FindAsync(id);
            if (acquisition_Item == null)
            {
                return NotFound();
            }

            return Ok(acquisition_Item);
        }

        // PUT: api/Acquisition_Item/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAcquisition_Item(int id, Acquisition_Item acquisition_Item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != acquisition_Item.Id)
            {
                return BadRequest();
            }

            db.Entry(acquisition_Item).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Acquisition_ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Acquisition_Item
        [ResponseType(typeof(Acquisition_Item))]
        public async Task<IHttpActionResult> PostAcquisition_Item(Acquisition_Item acquisition_Item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Acquisition_Item.Add(acquisition_Item);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = acquisition_Item.Id }, acquisition_Item);
        }

        // DELETE: api/Acquisition_Item/5
        [ResponseType(typeof(Acquisition_Item))]
        public async Task<IHttpActionResult> DeleteAcquisition_Item(int id)
        {
            Acquisition_Item acquisition_Item = await db.Acquisition_Item.FindAsync(id);
            if (acquisition_Item == null)
            {
                return NotFound();
            }

            db.Acquisition_Item.Remove(acquisition_Item);
            await db.SaveChangesAsync();

            return Ok(acquisition_Item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Acquisition_ItemExists(int id)
        {
            return db.Acquisition_Item.Count(e => e.Id == id) > 0;
        }
    }
}