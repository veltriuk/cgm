﻿using System.Web.Mvc;

namespace CGM.Areas.Telemetry
{
    public class TelemetryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Telemetry";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                name:       "Telemetry_default",
                url:        "Telemetry/{controller}/{action}/{id}",
                defaults:   new { action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "CGM.Areas.Telemetry.Controllers"}
            );
        }
    }
}