﻿using CGM.Models.CGMModels;
using Hangfire;
using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class TableViewModel
    {
        public long id { get; set; }
        [DisplayName("Hora programado")]
        public DateTime? dateProgrammed { get; set; }
        [DisplayName("Hora completado")]
        public DateTime? dateDone { get; set; }
        [DisplayName("Código")]
        public string code { get; set; }
        [DisplayName("Frontera")]
        public string name { get; set; }
        [DisplayName("Prioridad")]
        public int? priority { get; set; }
        [DisplayName("Medidor")]
        public string meter { get; set; }
        [DisplayName("Tipo")]
        public bool? is_backup { get; set; }
        [DisplayName("Marca")]
        public string brand { get; set; }

        [DisplayName("Tiempo de realización")]
        public TimeSpan? time_elapsed { get; set; }
        [DisplayName("Completado")]
        public DateTime? end_time { get; set; }
        [DisplayName("Fuente")]
        public int? boundary_source { get; set; }
        [DisplayName("Reintentos")]
        public int? retries { get; set; }
        public int? jobId { get; set; }
        [DisplayName("Para ejecución")]
        public DateTime enqueueAt { get; set; }

        public static List<TableViewModel> ToTableViewModels(List<Report_Inform> ris)
        {
            List<TableViewModel> tvs = new List<TableViewModel>();
            var scheduled = JobStorage.Current.GetMonitoringApi().ScheduledJobs(0, 10000);
            foreach (var ri in ris)
            {
                TableViewModel tv = new TableViewModel()
                {
                    id = ri.id,
                    code = ri.Boundary.code,
                    dateProgrammed = ri.Report.date_and_time_to_report.Value,
                    dateDone = ri.Report.date_and_time_report,
                    name = ri.Boundary.name,
                    priority = ri.Boundary.id_priority,
                    is_backup = ri.is_backup,
                    time_elapsed = ri.time_elapsed,
                    end_time = ri.end_time,
                    jobId = ri.job_id
                };
                if (ri.Meter != null)
                {
                    tv.meter = ri.Meter.serial;
                    if (ri.Meter.Model_Meter != null)
                    { tv.brand = ri.Meter.Model_Meter.brand; }
                }
                try
                {tv.retries = int.Parse(ri.Job.JobParameters.Where(j => j.Name.Equals("RetryCount")).LastOrDefault().Value);}
                catch (Exception ex)
                {//                    El parámetro no existe
                    tv.retries = 0;}
                try
                {
                    tv.enqueueAt = scheduled.Where(s => s.Key == ri.job_id.ToString())
                                          .FirstOrDefault().Value.EnqueueAt.AddHours(1); //Para la hora de Italia. Esto hay que cambiarlo.
                }
                catch (Exception ex)
                {
                    //No es un parámetro que tiene reintentos.
                }
                tvs.Add(tv);
            }

            return tvs;
        }
    }
}