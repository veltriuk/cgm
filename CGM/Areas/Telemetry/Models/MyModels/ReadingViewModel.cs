﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CGM.Areas.Telemetry.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class ReadingViewModel
    {
        private static CGM_DB_Entities db = new CGM_DB_Entities();
        public List<Meter> Meters { get; set; }
        public List<Filtro> Filtros { get; set; }
        public List<Acquisition> Acquisitions { get; set; }
        public DateTime dateStart { get; set; }
        public DateTime dateEnd { get; set; }
        async public static Task<ReadingViewModel> BuildReadingViewModel()
        {
            db.Configuration.ProxyCreationEnabled = false; //Para que las lecturas de las entidades sean meras copias, y no se queden conectadas
            List<Meter>       mts = await db.Meter.Where(m => !m.is_filed).Include(x => x.Boundary).Include(x => x.Model_Meter).ToListAsync();
            foreach (var item in mts.Where(m => m.boundary_id == null))
            {
                item.Boundary = new Boundary() { code = "" , name = ""};
            }
            //mts.Where(m => m.boundary_id == null).Select(m => { m.Boundary = new Boundary() { code = "" }; return ""; });
            List<Filtro>      flt = await db.Filtro.Include(f => f.Meters).Include(f => f.Boundaries).ToListAsync();
            List<Acquisition> acquisitions = await db.Acquisition.Include(ac => ac.Acquisition_Items).Include(ac => ac.Acquisition_Items.Select(ai => ai.Quantity)).Include(ac => ac.Acquisition_Items.Select(ai => ai.Tag_Line)).ToListAsync();

            return new ReadingViewModel(mts, flt, acquisitions);

        }

        private ReadingViewModel(List<Meter> mts, List<Filtro> flt, List<Acquisition> acquisitions)
        {
            Meters = mts;
            Filtros = flt;
            Acquisitions = acquisitions;
        }
    }
}