﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class StateView
    {
        public int idjob { get; set; }
        public string message { get; set; }
        //public string messageSubstatus { get; set; }
        private DateTime time;

        public DateTime Time
        {
            get { 
                return time.AddHours(1);
            }
            set { time = value; }
        }

    }
}