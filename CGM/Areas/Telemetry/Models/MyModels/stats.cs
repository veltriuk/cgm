﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models.MyModels
{
    public class stats
    {
        public int value { get; set; }
        public int intValue { get; set; }
        public string style { get; set; }
        public bool highlighted { get; set; }
        public string title { get; set; }
    }
}