﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    [Table("Register", Schema = "Telemetry")]
    public class Register
    {
        public int id { get; set; }
        public int? quantity_id { get; set; }
        public int? model_id { get; set; }
        public string name { get; set; }

        [ForeignKey("quantity_id")]
        public virtual Quantity Quantity { get; set; }
        [ForeignKey("model_id")]
        public virtual Model_Meter Model_Meter { get; set; }
    }
}