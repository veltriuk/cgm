namespace CGM.Areas.Telemetry.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Acquisition_Item", Schema = "Telemetry")]
    public partial class ExceptionLogReading
    {
        public Guid id { get; set; }

        public string msg { get; set; }

        [StringLength(100)]
        public string type { get; set; }

        public string source { get; set; }

        [StringLength(100)]
        public string url { get; set; }

        public DateTime? logDate { get; set; }

        [StringLength(100)]
        public string innerExcMsg { get; set; }
    }
}
