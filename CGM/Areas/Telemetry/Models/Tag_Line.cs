﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.Telemetry.Models
{
    [Table("Tag_Line", Schema = "Telemetry")]
    public class Tag_Line
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int id { get; set; }
        [DisplayName("Línea")]
        public string name { get; set; }
        public string nameShort { get; set; }
        public int type { get; set; } //0: line, 1: Analog Output
        public int? numval { get; set; }
    }
}