﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class ResponsesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Responses
        public async Task<ActionResult> Index(int id)
        {
            ResponseList rl = new ResponseList();
            rl.Question = await db.Questions.FindAsync(id);
            rl.Responses= await db.Responses.Where(m=>m.Id_Question==id).OrderBy(m=>m.OrderResponse).Include(r => r.Question).Include(r => r.Question_Next).Include(r => r.Response_Types).ToListAsync();            
            return View(rl);
        }

        // GET: Responses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response response = await db.Responses.FindAsync(id);
            if (response == null)
            {
                return HttpNotFound();
            }
            return View(response);
        }

        // GET: Responses/Create
        public ActionResult Create(int id)
        {
            Response r = new Response();
            r.Id_Question = id;
            r.Question =  db.Questions.Find(id);
            int? or = db.Responses.Where(m => m.Id_Question == id).OrderByDescending(m=>m.OrderResponse).Select(m=>m.OrderResponse).FirstOrDefault();
            r.OrderResponse = or.HasValue ? or.Value + 1 : 1;
            r.EnumLabel = r.OrderResponse + ".";
            r.Question_Next = db.Questions.Where(m => m.Id > id).FirstOrDefault()??r.Question;
            r.Id_Question_Next = r.Question_Next.Id;
            r.MaxLenght = -1;
            r.MinValue = -1;
            r.MaxValue = -1;
            if (r.Question.Id_ControlGroup != 3)
            {
                r.Index_Row = -1;
                r.Index_Column = -1;
            }

            var ds = db.Questions.Where(m => m.Id_SurveyTemplate == r.Question.Id_SurveyTemplate && m.OrderQuestion>=r.Question.OrderQuestion).OrderBy(m=>m.OrderQuestion);

            
            ViewBag.Id_Question_Next = new SelectList(ds, "Id", "Title", r.Id_Question_Next);
            ViewBag.Id_Response_Type = new SelectList(db.Response_Types, "Id", "Name");
            ViewBag.Id_TypeData = new SelectList(db.TypeData, "Id", "Name");
            ViewBag.Id_Datasource = new SelectList(db.DataSource, "Id", "Name");

            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("_Create", r);
            }
            return View(r);
        }

        // POST: Responses/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Response response)
        {
            int? id = await db.Responses.OrderByDescending(m => m.Id).Select(m => m.Id).FirstOrDefaultAsync();
            response.Id = id.HasValue ? id.Value + 1 : 1;
            if (ModelState.IsValid)
            {
                db.Responses.Add(response);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new {id=response.Id_Question });
            }
            
            response.Question = await db.Questions.FindAsync(response.Id_Question);
            var ds = db.Questions.Where(m => m.Id_SurveyTemplate == response.Question.Id_SurveyTemplate  && m.OrderQuestion>=response.Question.OrderQuestion).OrderBy(m=>m.OrderQuestion);
            
            ViewBag.Id_Question_Next = new SelectList(ds, "Id", "Title", response.Id_Question_Next);            
            ViewBag.Id_Response_Type = new SelectList(db.Response_Types, "Id", "Name", response.Id_Response_Type);
            ViewBag.Id_TypeData = new SelectList(db.TypeData, "Id", "Name",response.Id_TypeData);
            ViewBag.Id_Datasource = new SelectList(db.DataSource, "Id", "Name",response.Id_Datasource);
            return View(response);
        }

        // GET: Responses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response response = await db.Responses.FindAsync(id);
            if (response == null)
            {
                return HttpNotFound();
            }
            var ds = db.Questions.Where(m => m.Id_SurveyTemplate == response.Question.Id_SurveyTemplate && m.OrderQuestion>=response.Question.OrderQuestion).OrderBy(m=>m.OrderQuestion);            
            ViewBag.Id_Question_Next = new SelectList(ds, "Id", "Title", response.Id_Question_Next);
            ViewBag.Id_Response_Type = new SelectList(db.Response_Types, "Id", "Name", response.Id_Response_Type);
            ViewBag.Id_TypeData = new SelectList(db.TypeData, "Id", "Name",response.Id_TypeData);
            ViewBag.Id_Datasource = new SelectList(db.DataSource, "Id", "Name",response.Id_Datasource);
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("_Edit", response);
            }
            return View(response);
        }

        // POST: Responses/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Response response)
        {
            if (ModelState.IsValid)
            {
                db.Entry(response).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", new { id = response.Id_Question });
            }
            response.Question = await db.Questions.FindAsync(response.Id_Question);
            var ds = db.Questions.Where(m => m.Id_SurveyTemplate == response.Question.Id_SurveyTemplate && m.OrderQuestion>=response.Question.OrderQuestion).OrderBy(m=>m.OrderQuestion);

            
            ViewBag.Id_Question_Next = new SelectList(ds, "Id", "Title", response.Id_Question_Next);
            ViewBag.Id_Response_Type = new SelectList(db.Response_Types, "Id", "Name", response.Id_Response_Type);
            ViewBag.Id_TypeData = new SelectList(db.TypeData, "Id", "Name", response.Id_TypeData);
            ViewBag.Id_Datasource = new SelectList(db.DataSource, "Id", "Name", response.Id_Datasource);
            return View(response);
        }

        // GET: Responses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Response response = await db.Responses.FindAsync(id);
            if (response == null)
            {
                return HttpNotFound();
            }
            return View(response);
        }

        // POST: Responses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Response response = await db.Responses.FindAsync(id);
            db.Responses.Remove(response);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", new { id = response.Id_Question });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
