﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class ControlGroupsController : Controller
    {
        
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: ControlGroups
        public async Task<ActionResult> Index()
        {
            return View(await db.ControlGroup.ToListAsync());
        }

        // GET: ControlGroups/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControlGroup controlGroup = await db.ControlGroup.FindAsync(id);
            if (controlGroup == null)
            {
                return HttpNotFound();
            }
            return View(controlGroup);
        }

        // GET: ControlGroups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ControlGroups/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] ControlGroup controlGroup)
        {
            if (ModelState.IsValid)
            {
                db.ControlGroup.Add(controlGroup);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(controlGroup);
        }

        // GET: ControlGroups/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControlGroup controlGroup = await db.ControlGroup.FindAsync(id);
            if (controlGroup == null)
            {
                return HttpNotFound();
            }
            return View(controlGroup);
        }

        // POST: ControlGroups/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] ControlGroup controlGroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(controlGroup).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(controlGroup);
        }

        // GET: ControlGroups/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ControlGroup controlGroup = await db.ControlGroup.FindAsync(id);
            if (controlGroup == null)
            {
                return HttpNotFound();
            }
            return View(controlGroup);
        }

        // POST: ControlGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ControlGroup controlGroup = await db.ControlGroup.FindAsync(id);
            db.ControlGroup.Remove(controlGroup);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
