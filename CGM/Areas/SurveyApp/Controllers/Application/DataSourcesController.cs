﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Areas.SurveyApp.Models;
using CGM.Models.CGMModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class DataSourcesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: DataSources
        public async Task<ActionResult> Index(string full="0")
        {
            if (full == "1")
            {
                return View(await db.DataSource.ToListAsync());
            }
            return View(await db.DataSource.Where(m => m.Group == 0).ToListAsync());

        }

        // GET: DataSources/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSource dataSource = await db.DataSource.FindAsync(id);
            if (dataSource == null)
            {
                return HttpNotFound();
            }
            return View(dataSource);
        }

        // GET: DataSources/Create
        public ActionResult Create(string dataset="0")
        {
            ViewBag.dataset = dataset;
            return View();
        }

        // POST: DataSources/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Group,Name")] DataSource dataSource)
        {
            if (ModelState.IsValid)
            {
                int id = 1;
                if (db.DataSource.Count()>0)
                {
                    id = db.DataSource.OrderByDescending(m => m.Id).FirstOrDefault().Id + 1;
                }                
                dataSource.Id = id;
                db.DataSource.Add(dataSource);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dataSource);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create1([Bind(Include = "Id,Group,Name")] DataSource dataSource)
        {
            if (ModelState.IsValid)
            {
                int id = 1;
                if (db.DataSource.Count() > 0)
                {
                    id = db.DataSource.OrderByDescending(m => m.Id).FirstOrDefault().Id + 1;
                }
                dataSource.Id = id;
                db.DataSource.Add(dataSource);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dataSource);
        }
        // GET: DataSources/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSource dataSource = await db.DataSource.FindAsync(id);
            if (dataSource == null)
            {
                return HttpNotFound();
            }
            return View(dataSource);
        }

        // POST: DataSources/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Group")] DataSource dataSource)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dataSource).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(dataSource);
        }

        // GET: DataSources/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataSource dataSource = await db.DataSource.FindAsync(id);
            if (dataSource == null)
            {
                return HttpNotFound();
            }
            return View(dataSource);
        }

        // POST: DataSources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DataSource dataSource = await db.DataSource.FindAsync(id);
            db.DataSource.Remove(dataSource);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
