﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.IO;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class SurveyTemplatesController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: SurveyTemplates
        public async Task<ActionResult> Index()
        {
            string iduser = User.Identity.GetUserId();
            if (User.IsInRole("Creator"))
            {
                var surveyTemplates = db.SurveyTemplates.Where(m => m.Id_UserCreator == iduser).Include(s => s.ApplicationUser);
                return View(await surveyTemplates.ToListAsync());
            }
            else if(User.IsInRole("Administrator"))
            {
                var surveyTemplates = db.SurveyTemplates.Include(s => s.ApplicationUser);
                return View(await surveyTemplates.ToListAsync());

            }
            return null;
            
        }

        public ActionResult QR()
        {
            return View();
        }

        // GET: SurveyTemplates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            if (surveyTemplate == null)
            {
                return HttpNotFound();
            }
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate.Id_UserCreator == iduser || User.IsInRole("Administrator"))
            {
                return View(surveyTemplate);
            }
            else
            {
                return View();
            }
        }

        // GET: SurveyTemplates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SurveyTemplates/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,Presentation,url_Logo,url_Image,")] SurveyTemplate surveyTemplate)
        {
            if (ModelState.IsValid)
            {
                if (!db.SurveyTemplates.Select(m => m.Name).Contains(surveyTemplate.Name))
                {
                    string dbPath = "Images/UploadImages/SurveyTemplates/";
                    string folderPath = Server.MapPath("~/" + dbPath);
                    string myname = getMyFileName(surveyTemplate.Name);

                    foreach (string file in Request.Files)
                    {
                        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                        if (hpf.ContentLength != 0)
                        {
                            hpf.SaveAs(folderPath + file + hpf.FileName);
                            switch (file)
                            {
                                case "url_Logo":
                                    surveyTemplate.url_Logo = dbPath + file + hpf.FileName;
                                    break;
                                case "url_Image":
                                    surveyTemplate.url_Image = dbPath + file + hpf.FileName;
                                    break;
                            }
                        }
                    }
                    int? id = await db.SurveyTemplates.OrderByDescending(m => m.Id).Select(m => m.Id).FirstOrDefaultAsync();
                    surveyTemplate.Id = id.HasValue ? id.Value + 1 : 1;
                    surveyTemplate.Id_UserCreator = User.Identity.GetUserId();
                    surveyTemplate.Is_Active = true;
                    surveyTemplate.CreationDate = DateTime.Now;

                    db.SurveyTemplates.Add(surveyTemplate);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");

                }

                ViewBag.Error = "Ya existe una plantilla de encuesta con el nombre " + surveyTemplate.Name;
            }

            return View(surveyTemplate);
        }

        private string getMyFileName(string name)
        {
            string rta = name.Replace(" ", "_").
                Replace("Ñ","N").Replace("ñ","n").                
                Replace("Á", "A").Replace("á", "a").
                Replace("É", "E").Replace("á", "a").
                Replace("Í", "I").Replace("á", "a").
                Replace("Ó", "O").Replace("á", "a").
                Replace("Ú", "U").Replace("ú", "u");
            return rta;
        }

        // GET: SurveyTemplates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }          
            
            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            
            if (surveyTemplate == null)
            {
                return HttpNotFound();
            }
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate.Id_UserCreator == iduser || User.IsInRole("Administrator"))
            {
                return View(surveyTemplate);
            }            
            return View();

            
        }

        // POST: SurveyTemplates/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]        
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,Presentation,url_Logo,url_Image,CreationDate,Is_Active,Id_UserCreator")] SurveyTemplate surveyTemplate)
        {
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate.Id_UserCreator == iduser || User.IsInRole("Administrator"))
            {
                if (ModelState.IsValid)
                {
                    var surveyTemplateold = db.SurveyTemplates.Where(m => m.Id == surveyTemplate.Id).Select(m => new { m.url_Image, m.url_Logo, m.Is_Active, m.Id_UserCreator, m.CreationDate }).First();

                    if (!db.SurveyTemplates.Where(m => m.Id != surveyTemplate.Id).Select(m => m.Name).Contains(surveyTemplate.Name))
                    {
                        surveyTemplate.Is_Active = surveyTemplateold.Is_Active;
                        surveyTemplate.CreationDate = surveyTemplateold.CreationDate;
                        surveyTemplate.Id_UserCreator = surveyTemplateold.Id_UserCreator;
                        surveyTemplate.url_Image = surveyTemplateold.url_Image;
                        surveyTemplate.url_Logo = surveyTemplateold.url_Logo;


                        string dbPath = "Images/UploadImages/SurveyTemplates/";
                        string folderPath = Server.MapPath("~/" + dbPath);

                        foreach (string file in Request.Files)
                        {
                            HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                            if (hpf.ContentLength != 0)
                            {
                                switch (file)
                                {
                                    case "url_Logo":
                                        string smpl = Server.MapPath("~/" + surveyTemplateold.url_Logo);
                                        if (System.IO.File.Exists(smpl))
                                        {
                                            System.IO.File.Delete(smpl);
                                        }
                                        surveyTemplate.url_Logo = dbPath + file + hpf.FileName;
                                        break;
                                    case "url_Image":
                                        string smpi = Server.MapPath("~/" + surveyTemplateold.url_Image);
                                        if (System.IO.File.Exists(smpi))
                                        {
                                            System.IO.File.Delete(smpi);
                                        }
                                        surveyTemplate.url_Image = dbPath + file + hpf.FileName;
                                        break;
                                }
                                hpf.SaveAs(folderPath + file + hpf.FileName);
                            }
                        }
                        db.Entry(surveyTemplate).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ViewBag.Error = "Ya existe una plantilla de encuesta con el nombre " + surveyTemplate.Name;
                }
                return View(surveyTemplate);
            }
            return View();

            
        }

        //// GET: SurveyTemplates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            if (surveyTemplate == null)
            {
                return HttpNotFound();
            }
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate.Id_UserCreator == iduser || User.IsInRole("Administrator"))
            {
                return View(surveyTemplate);
            }
            else
            {
                return View();
            }

        }

        //// POST: SurveyTemplates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {

            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            string smpi = Server.MapPath("~/" + surveyTemplate.url_Logo);
            string smpl = Server.MapPath("~/" + surveyTemplate.url_Image);
            if (System.IO.File.Exists(smpi))
            {
                System.IO.File.Delete(smpi);
            }
            if (System.IO.File.Exists(smpl))
            {
                System.IO.File.Delete(smpl);
            }
            string iduser = User.Identity.GetUserId();
            if (surveyTemplate.Id_UserCreator == iduser || User.IsInRole("Administrator"))
            {
                db.SurveyTemplates.Remove(surveyTemplate);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View();


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
