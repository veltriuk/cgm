﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.Application
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class QuestionsController : Controller
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: Questions
        public async Task<ActionResult> Index(int id)
        {
            QuestionList ql = new QuestionList();
            ql.SurveyTemplate =  await db.SurveyTemplates.FindAsync(id);            
            ql.Questions =await db.Questions.OrderBy(m=>m.OrderQuestion).Where(m=>m.Id_SurveyTemplate==id).Include(q => q.SurveyTemplate).ToListAsync();
            
            return View(ql);
        }

        // GET: Questions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question2 = await db.Questions.FindAsync(id);
            if (question2 == null)
            {
                return HttpNotFound();
            }
            return View(question2);
        }

        // GET: Questions/Create
        public ActionResult Create(int id)
        {
            Question q = new Question();
            q.Id_SurveyTemplate = id;
            int? oq = db.Questions.Where(m => m.Id_SurveyTemplate == id).OrderByDescending(m => m.OrderQuestion).Select(m => m.OrderQuestion).FirstOrDefault();
            q.OrderQuestion = oq.HasValue ? oq.Value + 1 : 1;
            q.EnumLabel = q.OrderQuestion + ".";
            q.Is_VerticalOrientation = true;
            q.Is_Requerid = true;
            q.Num_Row_Response = -1;
            q.Num_Column_Response = -1;

            q.SurveyTemplate= db.SurveyTemplates.Find(id);
            var ds = db.ControlGroup.OrderBy(m=>m.Id);
            ViewBag.Id_ControlGroup = new SelectList(ds, "Id", "Name");
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("_Create", q);
            }
            return View(q);
        }

        // POST: Questions/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,OrderQuestion,EnumLabel,Title,Subtitle,Anotation,Is_VerticalOrientation,Id_ControlGroup,Is_Requerid,Num_Row_Response,Num_Column_Response,Id_SurveyTemplate,Is_Images")] Question question)
        {
            //La primera pregunta de esa encuesta se ubica en el siguiente múltiplo de 100

            List<int> questionsIdInSurvey= await db.Questions.Where(q => q.Id_SurveyTemplate == question.Id_SurveyTemplate).OrderByDescending(m => m.Id).Select(m => m.Id).ToListAsync();
            int? idinSurvey = questionsIdInSurvey.FirstOrDefault();
            int? id = null;
            if (!idinSurvey.HasValue)
            {
                id = question.Id_SurveyTemplate * 100; 
            }
            else
            {
                if (questionsIdInSurvey.Count == 1)
                    id = (question.Id_SurveyTemplate - 1) * 100 + 1;
                else
                    id = questionsIdInSurvey[1] + 1; //Segundo valor después del último
            }
            //int? id = await db.Questions.OrderByDescending(m => m.Id).Select(m => m.Id).FirstOrDefaultAsync();
            question.Id = id.HasValue ? id.Value + 1 : 1;
            if (ModelState.IsValid)
            {
                db.Questions.Add(question);
                await db.SaveChangesAsync();
                return RedirectToAction("Index",new { id=question.Id_SurveyTemplate});
            }
            var ds = db.ControlGroup.OrderBy(m => m.Id);
            
            question.SurveyTemplate = await db.SurveyTemplates.FindAsync(question.Id_SurveyTemplate);
            ViewBag.Id_ControlGroup = new SelectList(ds, "Id", "Name",question.Id_ControlGroup);
            return View(question);
        }

        // GET: Questions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            var ds = db.ControlGroup.OrderBy(m => m.Id);
            ViewBag.Id_ControlGroup = new SelectList(ds, "Id", "Name",question.Id_ControlGroup);
            if (Request.IsAjaxRequest() || ControllerContext.IsChildAction)
            {
                return PartialView("_Edit", question);
            }
            return View(question);
        }

        // POST: Questions/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,OrderQuestion,EnumLabel,Title,Subtitle,Anotation,Is_VerticalOrientation,Id_ControlGroup,Is_Requerid,Num_Row_Response,Num_Column_Response,Id_SurveyTemplate,Is_Images")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index",new { id=question.Id_SurveyTemplate });
            }
            var ds = db.ControlGroup.OrderBy(m => m.Id);
            question.SurveyTemplate = await db.SurveyTemplates.FindAsync(question.Id_SurveyTemplate);
            ViewBag.Id_ControlGroup = new SelectList(ds, "Id", "Name", question.Id_ControlGroup);
            return View(question);
        }

        // GET: Questions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Question question = await db.Questions.FindAsync(id);
            db.Questions.Remove(question);
            await db.SaveChangesAsync();
            return RedirectToAction("Index",new { id = question.Id_SurveyTemplate });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
