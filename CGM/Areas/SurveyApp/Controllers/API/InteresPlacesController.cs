﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    public class InteresPlacesController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/InteresPlaces
        public IQueryable<InteresPlace> GetInteresPlaces()
        {
            return db.InteresPlaces;
        }

        // GET: api/InteresPlaces/5
        [ResponseType(typeof(InteresPlace))]
        public async Task<IHttpActionResult> GetInteresPlace(int id)
        {
            InteresPlace interesPlace = await db.InteresPlaces.FindAsync(id);
            if (interesPlace == null)
            {
                return NotFound();
            }

            return Ok(interesPlace);
        }

        // PUT: api/InteresPlaces/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInteresPlace(int id, InteresPlace interesPlace)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != interesPlace.Id)
            {
                return BadRequest();
            }

            db.Entry(interesPlace).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InteresPlaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InteresPlaces
        [ResponseType(typeof(InteresPlace))]
        public async Task<IHttpActionResult> PostInteresPlace(InteresPlace interesPlace)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InteresPlaces.Add(interesPlace);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = 201 }, interesPlace);
        }

        // DELETE: api/InteresPlaces/5
        [ResponseType(typeof(InteresPlace))]
        public async Task<IHttpActionResult> DeleteInteresPlace(int id)
        {
            InteresPlace interesPlace = await db.InteresPlaces.FindAsync(id);
            if (interesPlace == null)
            {
                return NotFound();
            }

            db.InteresPlaces.Remove(interesPlace);
            await db.SaveChangesAsync();

            return Ok(interesPlace);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InteresPlaceExists(int id)
        {
            return db.InteresPlaces.Count(e => e.Id == id) > 0;
        }
    }
}