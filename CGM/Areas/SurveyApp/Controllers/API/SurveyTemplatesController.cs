﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class SurveyTemplatesController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/SurveyTemplates
        public List<MySurveyTemplate> GetSurveyTemplates()
        {
            List<MySurveyTemplate> mySurveyTemplates = new List<MySurveyTemplate>();
            foreach (var item in db.SurveyTemplates)
            {
                mySurveyTemplates.Add(getMySurveyTemplate(item));
            }
            return mySurveyTemplates;
        }

        // GET: api/SurveyTemplates/5
        [ResponseType(typeof(MySurveyTemplate))]
        public async Task<IHttpActionResult> GetSurveyTemplate(int id)
        {
            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            
            if (surveyTemplate == null)
            {
                return NotFound();
            }

            return Ok(getMySurveyTemplate(surveyTemplate));
        }

        private MySurveyTemplate getMySurveyTemplate(SurveyTemplate surveyTemplate)
        {
            
            MySurveyTemplate st = new MySurveyTemplate()
            {
                Id = surveyTemplate.Id,
                Name = surveyTemplate.Name,
                Presentation = surveyTemplate.Presentation != null ? surveyTemplate.Presentation : "",
                Description = surveyTemplate.Description != null ? surveyTemplate.Description : "",
                CreationDate = surveyTemplate.CreationDate,
                Id_UserCreator = surveyTemplate.Id_UserCreator,
                Is_Active = surveyTemplate.Is_Active ? 1 : 0,
                url_Image = surveyTemplate.url_Image,
                url_Logo = surveyTemplate.url_Logo,
            };

            List<MyQuestion> questions = new List<MyQuestion>();
            foreach (var question in surveyTemplate.Questions)
            {
                List<MyResponse> responses = new List<MyResponse>();

                foreach (var response in question.Responces_Question)
                {
                    responses.Add(
                        new MyResponse()
                        {
                            Id = response.Id,
                            Title = response.Title,
                            Index_Row = response.Index_Row.HasValue ? response.Index_Row.Value : -1,
                            Index_Column = response.Index_Column.HasValue ? response.Index_Column.Value : -1,
                            Value = response.Value != null ? response.Value : "",
                            Id_Question = response.Id_Question,
                            Id_Response_Type = response.Id_Response_Type,
                            Id_Question_Next = response.Id_Question_Next.HasValue ? response.Id_Question_Next.Value : response.Id_Question,
                            OrderResponse = response.OrderResponse,
                            EnumLabel = response.EnumLabel != null ? response.EnumLabel : "",
                            MaxLenght = response.MaxLenght.HasValue ? response.MaxLenght.Value : -1,
                            MinValue = response.MinValue.HasValue ? response.MinValue.Value : -1,
                            MaxValue = response.MaxValue.HasValue ? response.MaxValue.Value : -1,
                            RexExpresion = response.RexExpresion != null ? response.RexExpresion : "",
                            Id_TypeData = response.Id_TypeData.HasValue ? response.Data_Type.Name : "",
                            AditionalResponse = response.AditionalResponse != null ? response.AditionalResponse : "",
                            ValueAditionalResponse = response.ValueAditionalResponse != null ? response.ValueAditionalResponse : "",
                            Colspan = response.Colspan.HasValue ? response.Colspan.Value : -1,
                            Is_Required = response.Is_Required ? 1 : 0,
                            Id_Datasource = response.Id_Datasource
                        });
                }

                questions.Add(
                    new MyQuestion()
                    {
                        Id = question.Id,
                        Title = question.Title,
                        Subtitle = question.Subtitle != null ? question.Subtitle : "",
                        Anotation = question.Anotation != null ? question.Anotation : "",
                        Is_VerticalOrientation = question.Is_VerticalOrientation ? 1 : 0,
                        Is_Requerid = question.Is_Requerid ? 1 : 0,
                        Is_Images = question.Is_Images ? 1 : 0,
                        Num_Row_Response = question.Num_Row_Response.HasValue ? question.Num_Row_Response.Value : -1,
                        Num_Column_Response = question.Num_Column_Response.HasValue ? question.Num_Column_Response.Value : -1,
                        Id_SurveyTemplate = question.Id_SurveyTemplate,
                        OrderQuestion = question.OrderQuestion,
                        EnumLabel = question.EnumLabel != null ? question.EnumLabel : "",
                        ControlGroup = question.ControlGroup.Name,
                        Observation = question.Observation != null ? question.Observation : "",
                        Responses = responses

                    });



            }
            st.Questions = questions;

            return st;
        }

        // PUT: api/SurveyTemplates/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSurveyTemplate(int id, SurveyTemplate surveyTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != surveyTemplate.Id)
            {
                return BadRequest();
            }

            db.Entry(surveyTemplate).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SurveyTemplateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SurveyTemplates
        [ResponseType(typeof(SurveyTemplate))]
        public async Task<IHttpActionResult> PostSurveyTemplate(SurveyTemplate surveyTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SurveyTemplates.Add(surveyTemplate);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = surveyTemplate.Id }, surveyTemplate);
        }

        // DELETE: api/SurveyTemplates/5
        [ResponseType(typeof(SurveyTemplate))]
        public async Task<IHttpActionResult> DeleteSurveyTemplate(int id)
        {
            SurveyTemplate surveyTemplate = await db.SurveyTemplates.FindAsync(id);
            if (surveyTemplate == null)
            {
                return NotFound();
            }

            db.SurveyTemplates.Remove(surveyTemplate);
            await db.SaveChangesAsync();

            return Ok(surveyTemplate);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SurveyTemplateExists(int id)
        {
            return db.SurveyTemplates.Count(e => e.Id == id) > 0;
        }
    }
}