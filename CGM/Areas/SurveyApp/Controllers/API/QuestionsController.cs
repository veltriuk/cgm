﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class QuestionsController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        
            
        // GET: api/Questions
        public IQueryable<Question> GetQuestions()
        {
            return db.Questions;
        }

        // GET: api/Questions/5
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> GetQuestion(int id)
        {
            Question question2 = await db.Questions.FindAsync(id);
            if (question2 == null)
            {
                return NotFound();
            }

            return Ok(question2);
        }

        // PUT: api/Questions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutQuestion(int id, Question question2)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question2.Id)
            {
                return BadRequest();
            }

            db.Entry(question2).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Questions
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> PostQuestion(Question question2)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Questions.Add(question2);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = question2.Id }, question2);
        }

        // DELETE: api/Questions/5
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> DeleteQuestion(int id)
        {
            Question question2 = await db.Questions.FindAsync(id);
            if (question2 == null)
            {
                return NotFound();
            }

            db.Questions.Remove(question2);
            await db.SaveChangesAsync();

            return Ok(question2);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuestionExists(int id)
        {
            return db.Questions.Count(e => e.Id == id) > 0;
        }
    }
}