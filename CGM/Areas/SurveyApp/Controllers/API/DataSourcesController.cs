﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Areas.SurveyApp;
using CGM.Areas.SurveyApp.Models.MyModels;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class DataSourcesController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/DataSources
        public List<MyDataSource> GetDataSource()
        {
            
            List<MyDataSource> datasources = new List<MyDataSource>();
            foreach (var datasource in db.DataSource)
            {
                List<MyDataSourceItem> datasourceitems = new List<MyDataSourceItem>();
                foreach (var item in datasource.DataSourceItems)
                {
                    datasourceitems.Add(
                        new MyDataSourceItem()
                        {
                            Id=item.Id,
                            key=item.key,
                            Value=item.Value,
                            Id_DataSource=item.Id_DataSource
                        });
                }

                datasources.Add(
                    new MyDataSource()
                    {
                        Id=datasource.Id,
                        Name=datasource.Name,
                        DataSourceItems= datasourceitems

                    });
            }
            return datasources;
        }

        // GET: api/DataSources/5
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> GetDataSource(int id)
        {
            DataSource dataSource = await db.DataSource.FindAsync(id);
            if (dataSource == null)
            {
                return NotFound();
            }

            return Ok(dataSource);
        }

        // PUT: api/DataSources/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDataSource(int id, DataSource dataSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dataSource.Id)
            {
                return BadRequest();
            }

            db.Entry(dataSource).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DataSourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DataSources
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> PostDataSource(DataSource dataSource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DataSource.Add(dataSource);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DataSourceExists(dataSource.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dataSource.Id }, dataSource);
        }

        // DELETE: api/DataSources/5
        [ResponseType(typeof(DataSource))]
        public async Task<IHttpActionResult> DeleteDataSource(int id)
        {
            DataSource dataSource = await db.DataSource.FindAsync(id);
            if (dataSource == null)
            {
                return NotFound();
            }

            db.DataSource.Remove(dataSource);
            await db.SaveChangesAsync();

            return Ok(dataSource);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DataSourceExists(int id)
        {
            return db.DataSource.Count(e => e.Id == id) > 0;
        }
    }
}