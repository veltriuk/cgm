﻿using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;
using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class FileResponseController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        [HttpPost]
        public KeyValuePair<bool, string> UploadFile()
        {
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["file_data"];
                    int id_Question = int.Parse(HttpContext.Current.Request.Form.Get("Id_Question"));//Artefact
                    int Id_Response = int.Parse(HttpContext.Current.Request.Form.Get("Id_Response"));
                    Guid Id_ShareQuestion = new Guid(HttpContext.Current.Request.Form.Get("Id_ShareQuestion"));
                    string Name = httpPostedFile.FileName;
                    string archivo = (DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + Name).ToLower();
                    if (httpPostedFile != null)
                    {
                        ResponseFile rf= db.ResponseFile.Where(m => m.Id_ShareQuestion == Id_ShareQuestion && m.Id_Question == id_Question && m.Id_Response == Id_Response).FirstOrDefault();
                        String Url_Last = "";
                        if (rf!=null)
                        {
                            Url_Last = rf.Url;
                            rf.Name = archivo;
                            rf.Url = "/UploadedFiles/Responses/" + archivo;
                            rf.UpdateDate = DateTime.Now;
                            db.Entry(rf).State = System.Data.Entity.EntityState.Modified;                            
                        }
                        else
                        {
                            rf = new ResponseFile()
                            {
                                Id = 0,
                                CreateDate = DateTime.Now,
                                UpdateDate = DateTime.Now,
                                Id_Question = id_Question,
                                Id_Response = Id_Response,
                                Id_ShareQuestion = Id_ShareQuestion,
                                Name = archivo,
                                Url = "/UploadedFiles/Responses/" + archivo

                            };
                            db.ResponseFile.Add(rf);
                        }
                        db.SaveChanges();
                        if (!String.IsNullOrEmpty(Url_Last))
                        {
                            var fileRemovePath = HttpContext.Current.Server.MapPath("~/"+Url_Last);
                            File.Delete(fileRemovePath);
                        }                                                         
                        var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles/Responses"), archivo);
                        httpPostedFile.SaveAs(fileSavePath);                        
                        return new KeyValuePair<bool, string>(true, "File uploaded successfully.");
                    }

                    return new KeyValuePair<bool, string>(true, "Could not get the uploaded file.");
                }

                return new KeyValuePair<bool, string>(true, "No file found to upload.");
            }
            catch (Exception ex)
            {
                return new KeyValuePair<bool, string>(false, "An error occurred while uploading the file. Error Message: " + ex.Message);
            }
        }

        [HttpGet]
        public KeyValuePair<bool, string> DeleteFile(String url)
        {            
            try
            {
                var rf=db.ResponseFile.Where(m => m.Url == url).FirstOrDefault();
                if (rf != null)
                {                    
                    db.ResponseFile.Remove(rf);
                    db.SaveChanges();
                    File.Delete(HttpContext.Current.Server.MapPath("~"+url));
                }
                return new KeyValuePair<bool, string>(true, "El archivo se ha eliminado satisfactoriamente.");
            }
            catch (Exception ex)
            {
                return new KeyValuePair<bool, string>(false, "An error occurred while deleting the file. Error Message: " + ex.Message);
            }
        }
    }
}
