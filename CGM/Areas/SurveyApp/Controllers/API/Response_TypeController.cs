﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class Response_TypeController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();

        // GET: api/Response_Type
        public Object GetResponse_Types()
        {
            var rt=db.Response_Types.Select(m=>new {m.Id,m.Name }).ToList();
            return rt;
        }

        // GET: api/Response_Type/5
        [ResponseType(typeof(Response_Type))]
        public async Task<IHttpActionResult> GetResponse_Type(int id)
        {
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            if (response_Type == null)
            {
                return NotFound();
            }

            return Ok(response_Type);
        }

        // PUT: api/Response_Type/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutResponse_Type(int id, Response_Type response_Type)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != response_Type.Id)
            {
                return BadRequest();
            }

            db.Entry(response_Type).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Response_TypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Response_Type
        [ResponseType(typeof(Response_Type))]
        public async Task<IHttpActionResult> PostResponse_Type(Response_Type response_Type)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Response_Types.Add(response_Type);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = response_Type.Id }, response_Type);
        }

        // DELETE: api/Response_Type/5
        [ResponseType(typeof(Response_Type))]
        public async Task<IHttpActionResult> DeleteResponse_Type(int id)
        {
            Response_Type response_Type = await db.Response_Types.FindAsync(id);
            if (response_Type == null)
            {
                return NotFound();
            }

            db.Response_Types.Remove(response_Type);
            await db.SaveChangesAsync();

            return Ok(response_Type);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Response_TypeExists(int id)
        {
            return db.Response_Types.Count(e => e.Id == id) > 0;
        }
    }
}