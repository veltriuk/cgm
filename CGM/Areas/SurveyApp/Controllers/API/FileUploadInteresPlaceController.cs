﻿using CGM.Models.CGMModels;
using CGM.Areas.SurveyApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CGM.Helpers.Attributes;

namespace CGM.Areas.SurveyApp.Controllers.API
{
    [CustomAuthorize(Roles = "Desarrollador,Administrador")]
    public class FileUploadInteresPlaceController : ApiController
    {
        private CGM_DB_Entities db = new CGM_DB_Entities();
        [HttpPost]
        public KeyValuePair<bool, string> UploadFile()
        {
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];
                    int id = int.Parse(HttpContext.Current.Request.Form.Get("Id"));//Artefact
                    int Id_InteresPlace = int.Parse(HttpContext.Current.Request.Form.Get("Id_InteresPlace"));                                      
                    string DateInteresPlace = HttpContext.Current.Request.Form.Get("DateInteresPlace");

                    string relative_path_artefact = Path.DirectorySeparatorChar+"UploadedFiles"+Path.DirectorySeparatorChar+"InteresPlaces"+Path.DirectorySeparatorChar+"Artefact_" + id;
                    string relative_path_interesplace = relative_path_artefact + Path.DirectorySeparatorChar + "InteresPlace_" + Id_InteresPlace + Path.DirectorySeparatorChar;
                    string path_artefact = HttpContext.Current.Server.MapPath("~"+relative_path_artefact);
                    string path_interesplace = path_artefact + Path.DirectorySeparatorChar + "InteresPlace_" + Id_InteresPlace + Path.DirectorySeparatorChar;
                    
                    
                    if (!Directory.Exists(path_artefact))
                    {
                        Directory.CreateDirectory(path_artefact);
                    }

                    if (!Directory.Exists(path_interesplace))
                    {
                        Directory.CreateDirectory(path_interesplace);
                    }                    


                    if (httpPostedFile != null)
                    {
                        
                        string filename = httpPostedFile.FileName;                        
                        var fileSavePath = Path.Combine(path_interesplace, filename);
                        
                        httpPostedFile.SaveAs(fileSavePath);
                        InteresPlaceImage pi = new InteresPlaceImage()
                        {
                            Id = 0,
                            Id_InteresPlace = Id_InteresPlace,
                            Path = Path.Combine(relative_path_interesplace, filename),
                            DateCreate = DateInteresPlace,
                            Id_mobileArtefact = id
                        };
                        db.InteresPlaceImages.Add(pi);
                        db.SaveChanges();                        

                        return new KeyValuePair<bool, string>(true, "File uploaded successfully.");
                    }

                    return new KeyValuePair<bool, string>(true, "Could not get the uploaded file.");
                }

                return new KeyValuePair<bool, string>(true, "No file found to upload.");
            }
            catch (Exception ex)
            {
                return new KeyValuePair<bool, string>(false, "An error occurred while uploading the file. Error Message: " + ex.Message);
            }
        }
    }
}
