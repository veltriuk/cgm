﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class MySurveyMaster
    {
        public String Title { get; set; }
        public List<QuestionMaster> Questions { get; set; }
        public int Id { get; internal set; }
    }
    public class MySurveyMasterRequest
    {
        public Guid Id_ShareQuestion { get; set; }
        public int Id_Question { get; set; }
        public int Id_QuestionLast { get; set; }
        public bool Is_Last { get; set; }
        public Dictionary<int, MyResponseForm> Responses { get; set; }        
    }
    public class MyResponseForm
    {
        public string Value { get; set; }
        public string AditionalResponse { get; set; }
        public string ValueAditionalResponse { get; set; }
    }

    public class MySurveyMasterResponse
    {
        public Guid Id_ShareQuestion { get; set; }
        public int Id_Question { get; set; }
        public int Id_QuestionLast { get; set; }
        public bool Is_Last { get; set; }
        public bool Is_Validate { get; set; }
        public List<QuestionMaster> Questions { get; set; }
        public string gratMessage { get; set; }
    }
    public class QuestionMaster
    {
        public String EnumLabel { get; set; }
        public String Title { get; set; }
        public int Id_ControlGroup { get; set; }
        public List<ResponseMaster> Responses { get; set; }
        public bool Is_Images { get; internal set; }
        public int Id { get; internal set; }
        public bool Is_ResponseUser { get; internal set; }
        public int? Num_Column_Response { get; internal set; }
        public int? Num_Row_Response { get; internal set; }
    }
    
    public class ResponseMaster
    {        
        public String Title { get; set; }
        public String Value { get; set; }
        public int? Index_Row { get; set; }
        public int? Index_Column { get; set; }
        public int Id_Response_Type { get; set; }
        public int? Id_TypeData { get; set; }
        public String ValueSelect { get; set; }
        public int Id { get; internal set; }
        public int? Colspan { get; internal set; }
        public bool Is_File { get; set; }
        public String Url_File { get; set; }
        public List<DataSourceItem> DataSourceItems { get; set; }
    }

    
}