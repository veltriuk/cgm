﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class ShareModel
    {
        public string Name { get; set; }
        public string ContactName { get; set; }
        public Guid Id_ShareQuestion { get; set; }
        public int FirstQuestion { get; set; }
        

    }
}