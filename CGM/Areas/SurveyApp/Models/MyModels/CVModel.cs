﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class CVModel
    {
        public Guid Id { get; set; }
        public String Identification { get; set; }
        public String NiuCode { get; set; }
        public String LasUpdate { get; set; }
        public String Municipality { get; set; }
        //Tensión
        public String Kv { get; set; }
        //Capacidad Instalada
        public String Capacity { get; set; }
        public StateCV StateCV { get; set; }
    }

    public enum StateCV{
        inicio,
        Ejecucion,
        Fin
    }
}