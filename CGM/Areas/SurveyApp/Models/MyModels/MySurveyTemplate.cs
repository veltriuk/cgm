﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{    

    public class MySurveyTemplate
    {        
        public int Id { get; set; }        
        public string Name { get; set; }        
        public string Description { get; set; }        
        public string Presentation { get; set; }        
        public string url_Logo { get; set; }        
        public string url_Image { get; set; }
        public DateTime CreationDate { get; set; }        
        public int Is_Active { get; set; }
        public string Id_UserCreator { get; set; }
        public List<MyQuestion> Questions { get; set; }
    }

    public class MyQuestion
    {        
        public int Id { get; set; }        
        public string Title { get; set; }        
        public string Subtitle { get; set; }        
        public string Anotation { get; set; }        
        public int Is_VerticalOrientation { get; set; }        
        public int Is_Requerid { get; set; }        
        public int Is_Images { get; set; }        
        public int? Num_Row_Response { get; set; }        
        public int? Num_Column_Response { get; set; }
        public int Id_SurveyTemplate { get; set; }        
        public int OrderQuestion { get; set; }        
        public String EnumLabel { get; set; }        
        public String ControlGroup { get; set; }        
        public string Observation { get; set; }

        public List<MyResponse> Responses { get; set; }

    }


    public class MyResponse
    {
        
        public int Id { get; set; }        
        public string Title { get; set; }        
        public int? Index_Row { get; set; }        
        public int? Index_Column { get; set; }       
        public string Value { get; set; }
        public int Id_Question { get; set; }        
        public int Id_Response_Type { get; set; }        
        public int? Id_Question_Next { get; set; }        
        public int OrderResponse { get; set; }        
        public string EnumLabel { get; set; }        
        public int? MaxLenght { get; set; }       
        public int? MinValue { get; set; }        
        public int? MaxValue { get; set; }        
        public string RexExpresion { get; set; }        
        public String Id_TypeData { get; set; }        
        public string AditionalResponse { get; set; }        
        public string ValueAditionalResponse { get; set; }        
        public int? Colspan { get; set; }        
        public int Is_Required { get; set; }        
        public int? Id_Datasource { get; set; }

    }

    public class MyDataSource
    {        
        public int Id { get; set; }        
        public string Name { get; set; }
        public List<MyDataSourceItem> DataSourceItems { get; set; }
    }
    public class MyDataSourceItem
    {
        public int Id { get; set; }        
        public int key { get; set; }        
        public string Value { get; set; }        
        public int Id_DataSource { get; set; }
    }

}