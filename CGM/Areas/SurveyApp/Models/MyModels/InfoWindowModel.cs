﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models.MyModels
{
    public class InfoWindowModel
    {
        public int Id { get; set; }
        public string SurveyTemplateName { get; set; }
        public string SurveyTemplateImage { get; set; }
        public int Id_SurveyTemplate { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public string DateCreate { get; set; }
        public string DateFinish { get; set; }
        public int Id_Artefact { get; set; }

    }
}