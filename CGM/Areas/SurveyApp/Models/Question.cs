﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("Question", Schema = "Survey")]
    public class Question
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        [StringLength(500)]
        [Display(Name = "Título")]
        public string Title { get; set; }
        [StringLength(300)]
        [Display(Name = "Subtítulo")]
        public string Subtitle { get; set; }
        [StringLength(200)]
        [Display(Name = "Anotación")]
        public string Anotation { get; set; }
        [Display(Name = "Orientación vertical")]
        public bool Is_VerticalOrientation { get; set; }
        [Display(Name = "Pregunta requerida")]
        public bool Is_Requerid { get; set; }
        [Display(Name = "Imagen requerida")]
        public bool Is_Images { get; set; }
        [Display(Name = "Número de filas")]
        public int? Num_Row_Response { get; set; }
        [Display(Name = "Número de columnas")]
        public int? Num_Column_Response { get; set; }
        public int Id_SurveyTemplate { get; set; }
        [Required]
        [Display(Name ="Orden de pregunta")]
        public int OrderQuestion { get; set; }
        [Display(Name ="Etiqueta de enumeración")]
        public String EnumLabel { get; set; }                                    
        [Required]
        [Display(Name = "Tipos de control de agrupación")]
        public int Id_ControlGroup { get; set; }                
        [StringLength(500)]
        public string Observation { get; set; }



        [ForeignKey("Id_SurveyTemplate")]
        public virtual SurveyTemplate SurveyTemplate { get; set; }
        [ForeignKey("Id_ControlGroup")]
        public virtual ControlGroup ControlGroup { get; set; }

        [InverseProperty("Question_Next")]
        public virtual ICollection<Response> Responces_Question_Next { get; set; }
        [InverseProperty("Question")]
        public virtual ICollection<Response> Responces_Question { get; set; }



    }
}