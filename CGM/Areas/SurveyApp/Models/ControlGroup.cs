﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("ControlGroup", Schema = "Survey")]
    public class ControlGroup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [StringLength(200)]
        [Index("IX_ControlGroupName", 1, IsUnique = true)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
    }
}