﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("Response", Schema = "Survey")]
    public class Response
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [StringLength(500)]
        [Display(Name ="Título")]
        public string Title { get; set; }
        [Display(Name = "Índice de filas")]
        public int? Index_Row { get; set; }
        [Display(Name = "Índice de columnas")]
        public int? Index_Column { get; set; }
        [StringLength(500)]
        [Display(Name = "Valor")]
        public string Value { get; set; }        
        public int Id_Question { get; set; }
        [Display(Name ="Tipo de respuesta")]
        public int Id_Response_Type { get; set; }
        [Display(Name = "Pregunta siguiente")]
        public int? Id_Question_Next { get; set; }
        [Display(Name = "Orden Respuesta")]
        public int OrderResponse { get; set; }
        [StringLength(500)]
        [Display(Name = "Etiqueta de enumeración")]
        public string EnumLabel { get; set; }
        [Display(Name = "Longitud máxima")]
        public int? MaxLenght { get; set; }
        [Display(Name = "Valor mínimo")]
        public int? MinValue { get; set; }
        [Display(Name = "Valor máximo")]
        public int? MaxValue { get; set; }
        [Display(Name = "Expresión regular")]
        public string RexExpresion { get; set; }
        [Display(Name ="Tipo de datos")]
        public int? Id_TypeData { get; set; }
        [StringLength(500)]
        [Display(Name ="Respuesta adicional")]
        public string AditionalResponse { get; set; }
        [Display(Name ="Valor de respuesta adicional")]
        public string ValueAditionalResponse { get; set; }
        [Display(Name ="Expansión de columnas")]
        public int? Colspan { get; set; }
        [Display(Name ="Es requerido")]
        public bool Is_Required { get; set; }
        [Display(Name ="Conjunto de datos")]
        public int? Id_Datasource { get; set; }
        [Display(Name = "Requiere archivo")]
        public bool Is_Files { get; set; }        



        [ForeignKey("Id_TypeData")]
        public virtual TypeData Data_Type { get; set; }
        [ForeignKey("Id_Response_Type")]
        public virtual Response_Type Response_Types { get; set; }
        [ForeignKey("Id_Question")]
        public virtual Question Question { get; set; }
        [ForeignKey("Id_Question_Next")]
        public virtual Question Question_Next { get; set; }
        [ForeignKey("Id_Datasource")]
        public virtual DataSource DataSource { get; set; }
        //public virtual Dependency Dependency { get; set; }



    }
}