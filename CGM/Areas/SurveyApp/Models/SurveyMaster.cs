﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{    
    //Id INT PRIMARY KEY, Id_SurveyTemplate INT NOT NULL, is_active BOOLEAN, latitude REAL, longitude REAL, altitude REAL, datecreate NVARCHAR(255), datefinish NVARCHAR(255), is_send BOOLEAN, Id_MobileArtefact INT
    [Table("SurveyMaster", Schema = "Survey")]
    public class SurveyMaster
    {
        public int Id { get; set; }
        public int Id_Artefact { get; set; }
        public int Id_Survey { get; set; }
        public int Id_SurveyTemplate { get; set; }
        public int Is_active { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public String DateCreate { get; set; }
        public String DateFinish { get; set; }
        public int Id_MobileArtefact { get; set; }

    }
}