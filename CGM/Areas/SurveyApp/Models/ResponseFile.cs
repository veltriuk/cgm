﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("ResponseFile", Schema = "Survey")]
    public class ResponseFile
    {
        public int Id { get; set; }
        public int Id_Question { get; set; }
        public int Id_Response { get; set; }
        public Guid Id_ShareQuestion { get; set; }
        public String Name { get; set; }
        public String Url { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        [ForeignKey("Id_ShareQuestion")]
        public virtual ShareSurvey ShareSurvey { get; set; }        

    }
}