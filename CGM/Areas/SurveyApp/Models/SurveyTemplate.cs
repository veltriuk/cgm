﻿using CGM.Models.CGMModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("SurveyTemplate", Schema = "Survey")]
    public class SurveyTemplate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }   
        [Required]       
        [StringLength(200)]
        [Index("IX_Name", 1, IsUnique = true)]
        [Display(Name="Nombre")]
        public string Name { get; set; }
        [Required]
        [StringLength(500)]
        [Display(Name = "Descripción")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [StringLength(1000)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Presentación")]
        public string Presentation { get; set; }
        [StringLength(200)]
        [Display(Name = "Logo a la izquierdo")]
        [DataType(DataType.Upload)]
        public string url_Logo { get; set; }
        [StringLength(200)]
        [Display(Name = "Logo a la derecha")]
        [DataType(DataType.Upload)]
        public string url_Image { get; set; }
        public DateTime CreationDate { get; set; }
        [Display(Name = "Está activa")]
        public bool Is_Active { get; set; }
        
        public string Id_UserCreator { get; set; }


        [ForeignKey("Id_UserCreator")]
        public virtual AspNetUsers ApplicationUser { get; set; }

        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<ShareSurvey> ShareSurveys { get; set; }
    }
}