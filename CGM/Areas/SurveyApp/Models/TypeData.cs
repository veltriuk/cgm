﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("TypeData", Schema = "Survey")]
    public class TypeData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [StringLength(200)]
        [Index("IX_ControlGroupName", 1, IsUnique = true)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        public virtual ICollection<Response> Responses { get; set; }
    }
}