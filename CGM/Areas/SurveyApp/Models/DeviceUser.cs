namespace CGM.Areas.SurveyApp.Models
{
    using CGM.Models.CGMModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DeviceUser", Schema = "Survey")]
    public partial class DeviceUser
    {
        public int Id { get; set; }

        public string DeviceId { get; set; }

        [StringLength(128)]
        public string Id_Creator { get; set; }

        [ForeignKey("Id_Creator")]
        public virtual AspNetUsers AspNetUsers { get; set; }
    }
}
