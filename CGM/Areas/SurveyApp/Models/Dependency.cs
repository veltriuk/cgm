﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("Dependency", Schema = "Survey")]
    public class Dependency
    {
        public int id { get; set; }
        public int id_question { get; set; }
        public int id_response { get; set; }
        public int id_response_value { get; set; } //Para dependencias desde DropDownMenus
        public int id_response_dependant { get; set; }
        [StringLength(500)]
        public string possible_values { get; set; } //Deben escribirse separados por coma.

    }
}