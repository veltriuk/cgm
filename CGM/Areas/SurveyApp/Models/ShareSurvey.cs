﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("ShareSurvey", Schema = "Survey")]
    public class ShareSurvey
    {        
        public Guid Id { get; set; }
        [Display(Name ="Encuesta a aplicar")]
        public int Id_SurveyTemplate { get; set; }      
        [Display(Name ="Número de identificación")]        
        public string ContactName { get; set; }
        public bool Is_Accessed { get; set; }
        public bool Is_Finished { get; set; }
        public int Id_Survey { get; set; }
        //[Display(Name ="Hoja de vida número")]
        //public long? Id_CV { get; set; }
        //[Display(Name = "Código SIC de la frontera IMP-(Servicio eléctrico)")]
        //public string SIC_CodeFrtImp { get; set; }
        //[Display(Name = "Código SIC de la frontera EXP")]
        //public string SIC_CodeFrtExp { get; set; }
        //[Display(Name = "Código NIU de la frontera EXP")]
        //public string NIU_Code { get; set; }
        //[Display(Name ="Cédula catastral del predio")]
        //public string Catastral_Code { get; set; }


        [ForeignKey("Id_SurveyTemplate")]
        public virtual SurveyTemplate SurveyTemplate { get; set; }
    }
}