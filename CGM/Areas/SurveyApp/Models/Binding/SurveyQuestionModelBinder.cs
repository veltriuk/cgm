﻿using CGM.Areas.SurveyApp.Models.MyModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CGM.Areas.SurveyApp.Models.Binding
{
    public class SurveyQuestionModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var request = controllerContext.HttpContext.Request;
            MySurveyMasterRequest ms = new MySurveyMasterRequest();
            
            Dictionary<int, MyResponseForm> myResponseForms = new Dictionary<int, MyResponseForm>();

            foreach (var item in request.Form.AllKeys)
            {
                switch (item)
                {
                    case "Id_ShareQuestion":
                        Guid g = new Guid(request["Id_ShareQuestion"]);
                        ms.Id_ShareQuestion = g;
                        break;                   
                    case "Id_Question":
                        ms.Id_Question = int.Parse(request["Id_Question"]);
                        break;
                    case "Id_QuestionLast":
                        ms.Id_QuestionLast = int.Parse(request["Id_QuestionLast"]);
                        break;
                    case "Is_Last":
                        ms.Is_Last = bool.Parse(request["Is_Last"]);
                        break;
                    default:
                        string key = item;
                        string response=key.Replace("Response[" + ms.Id_Question + "][", "").Replace("]","");
                        MyResponseForm mrf = new MyResponseForm();
                        mrf.AditionalResponse = "";
                        mrf.ValueAditionalResponse = "";
                        mrf.Value = request[item];
                        if (response == "rd")
                        {
                            string[] dictionaryresponse = request[item].Split(':');
                            if (dictionaryresponse.Length > 1)
                            {
                                mrf.Value = dictionaryresponse[1];
                                myResponseForms.Add(int.Parse(dictionaryresponse[0]), mrf);
                            }
                            else
                            {
                                myResponseForms.Add(int.Parse(request[item]), mrf);
                            }
                        }                        
                        else {
                            myResponseForms.Add(int.Parse(response), mrf);
                        }
                        break;

                }
            }
            ms.Responses = myResponseForms;
            return ms;
        }
    }
}