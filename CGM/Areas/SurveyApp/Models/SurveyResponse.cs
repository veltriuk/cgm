﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CGM.Areas.SurveyApp.Models
{
    //Id,Id_Survey INT NOT NULL, ID_Question INT NOT NULL,
    //ID_QuestionNext INT NOT NULL, ID_QuestionLast INT NOT NULL, ID_Response INT NOT NULL, 
    //Value NVARCHAR(255), AditionalResponse NVARCHAR(200),ValueAditionalResponse NVARCHAR(255), 
    //Observation NVARCHAR(50)
  
    [Table("SurveyResponse", Schema = "Survey")]
    public class SurveyResponse
    {
        public int Id { get; set; }
        public int Id_Survey { get; set; }
        public int Id_Artefact { get; set; }
        public int Id_Quesion { get; set; }
        public int ID_QuestionNext { get; set; }
        public int ID_QuestionLast { get; set; }
        public int ID_Response { get; set; }
        [StringLength(255)]
        public String Value { get; set; }
        [StringLength(200)]
        public String AditionalResponse { get; set; }
        [StringLength(255)]
        public String ValueAditionalResponse { get; set; }
        [StringLength(500)]
        public String Observation { get; set; }
    }
}