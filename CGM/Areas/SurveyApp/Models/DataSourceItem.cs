﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CGM.Areas.SurveyApp.Models
{
    [Table("DataSourceItem", Schema = "Survey")]
    public class DataSourceItem
    {
        public int Id { get; set; }
        [Display(Name = "Clave")]
        public int key { get; set; }
        [Display(Name = "Valor")]
        public string Value { get; set; }
        [Display(Name = "Conjunto de datos")]
        public int Id_DataSource { get; set; }

        [ForeignKey("Id_DataSource")]
        public virtual DataSource DataSource { get; set; }
    }
}