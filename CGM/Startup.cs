﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Hangfire;
using CGM.Helpers;
using CGM.Models.CGMModels;
using CGM.Helpers.BackgroundManager;

[assembly: OwinStartup(typeof(CGM.Startup))]

namespace CGM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configuration.UseSqlServerStorage("CGM_DB_Entities", 
                new Hangfire.SqlServer.SqlServerStorageOptions {
                    JobExpirationCheckInterval = TimeSpan.FromMinutes(1)
                });

            app.UseHangfireDashboard();

            int workers = 4;
            int retries = 10;

            using (CGM_DB_Entities db = new CGM_DB_Entities())
            {
                workers = (int) db.Config_CM.Where(config => config.element.Trim().Equals("ProcesadoresSimultaneos")).FirstOrDefault().value.Value;
                retries = (int) db.Config_CM.Where(config => config.element.Trim().Equals("RetriesDefault")).FirstOrDefault().value.Value;
            }

            
            var options = ServerManager.GetBackgroundJobServerOptions();// Environment.ProcessorCount};

            ServerManager.RestartServer(0);
            //app.UseHangfireServer(options);

            //OperationManager.LoadRecurringPrograms();

            #region Hangfire custom retry
            { 
                /* set an override on the automaticRetryAttribute
                 */
                object automaticRetryAttribute = null;

                System.Diagnostics.Trace.TraceError("Search hangfire automatic retry");
                foreach (var filter in GlobalJobFilters.Filters)
                {
                    if (filter.Instance is AutomaticRetryAttribute)
                    {
                        // found it
                        automaticRetryAttribute = filter.Instance;
                        System.Diagnostics.Trace.TraceError("Found hangfire automatic retry");

                    }

                }
                System.Diagnostics.Trace.TraceError("Not found hangfire unless previous log says found");
                // ok now let's remove it
                if (automaticRetryAttribute == null)
                {
                    throw new System.Exception("Didn't find hangfire automaticRetryAttribute something very wrong");

                }

                System.Diagnostics.Trace.TraceError("remove hangefire automaticRetryAttribute");
                GlobalJobFilters.Filters.Remove(automaticRetryAttribute);

                GlobalJobFilters.Filters.Add(new CustomAutoRetryJobFilterAttribute(retries));
            }
            #endregion

        }
    }

    public class USER
    {
        public static string CLIENT { get; set; }
        public static string URIBASE { get; set; }
        public static string USERNAME { get; set; }
        public static string ROLE { get; set; }
        public static string[] ROLES { get; set; }

    }
}
