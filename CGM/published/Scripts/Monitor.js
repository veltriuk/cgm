﻿// %%%%%%%%%%%%%%%% Scripts for Monitor

$(function () {
    $('.btnState').click(function () {
            var ids = table.column(0).checkboxes.selected().join(',')
            if (ids == "") { alert("Favor seleccione elementos"); }
            else {
                var r = confirm("¿Confirma la instrucción?");
                if (r == true) {
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        url: ROOT + "api/Monitor/ToState",
                        data: { ids: ids, value: $(this).val() },
                        success: function (res) {
                            console.log(res)
                            location.reload();
                        }
                    }
                )
            }
        }
    }
    )

    $(".retryBadge").click(function (e) {
        $("#detailsWrapper").empty()
        $('#loaderModal').show();
        var id = $(this).prop('id');
        console.log(id);
        $.ajax({
            type: "GET",
            url: ROOT + "Monitor/RetryDetails",
            data: { id: id, },
            success: function (res) {
                $('#loaderModal').hide();
                $("#detailsWrapper").html(res);
                console.log(res)
                // location.reload();
            }
        })
        e.preventDefault();
    })
})