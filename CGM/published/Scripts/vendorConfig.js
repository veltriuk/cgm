﻿$('.nego').click(function () {
    var $this = $(this);
    event.preventDefault();
    document.getElementById("Negocio").value = $this.text();
});

function getNegocio(num) {
    var negocio = "";
    if (num == 0) {
        negocio = "Generación"
    }
    if (num == 1) {
        negocio = "Comercialización"
    }
    return negocio;
}

//AddAntiForgeryToken = function (data) {
//    data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
//    return data;
//};

Messenger.options = {
    extraClasses: 'messenger-fixed messenger-on-top',
    theme: 'air'
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// For the slider to change the range of allowed values in Boundaries


function createSlider(slider, inputs, minmax) {
    // var inputs = [input0, input1]; //Lo asigno desde antes
    var barmax = minmax[1] > 100 ? 500 : 100
    noUiSlider.create(slider, {
        start: minmax,
        connect: true,
        range: {
            'min': [0, 1],
            'max': [barmax, 1]
        },
        format: wNumb({
            decimals: 0,
        }),
        tooltips: [true, wNumb({ decimals: 0 })]

    });

    slider.noUiSlider.on('update', function (values, handle) {
        inputs[handle].value = values[handle];
    });
    // Listen to keydown events on the input field.
    inputs.forEach(function (input, handle) {

        input.addEventListener('change', function () {
            slider.noUiSlider.setHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {

            var values = slider.noUiSlider.get();
            var value = Number(values[handle]);

            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            var steps = slider.noUiSlider.steps();

            // [down, up]
            var step = steps[handle];

            var position;

            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch (e.which) {

                case 13:
                    slider.noUiSlider.setHandle(handle, this.value);
                    break;

                case 38:

                    // Get step to go increase slider value (up)
                    position = step[1];

                    // false = no step is set
                    if (position === false) {
                        position = 1;
                    }

                    // null = edge of slider
                    if (position !== null) {
                        slider.noUiSlider.setHandle(handle, value + position);
                    }

                    break;

                case 40:

                    position = step[0];

                    if (position === false) {
                        position = 1;
                    }

                    if (position !== null) {
                        slider.noUiSlider.setHandle(handle, value - position);
                    }

                    break;
            }
        });
    });

}


$(".updateSlider").click(function () {
    slider = $('#slider' + $(this).val())[0];
    slider.noUiSlider.updateOptions({
        range: {
            'min': [0 , 1],
            'max': [parseInt(this.textContent), 1]
        }
    });
})

