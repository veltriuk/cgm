﻿//import { error } from "util";

$(function chkAll() {
    $("input[name$='chkcol']").click(function () {
        var col = $(this).prop("value")
        $('[id ^= chkCol' + col + ']').prop("checked", $(this).prop("checked"))
        console.log(col)
    })
})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function getListParameters(p) {
    var response
    if (p == 0 || p == 2) { var url = ROOT + "api/Meters/GetMeters"; }
    if (p == 1) { var url = ROOT + "api/Boundaries/GetBoundaries"; }

    $.ajax({
        'async': false,
        type: "GET",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { pick: p },
        success: function (data) {
            response = data
        }
    })
    return response;
}


function successCoords(pos) {
    clat = "" + pos.coords.latitude;
    clon = "" + pos.coords.longitude;
    $.get("https://ipapi.co/json/", function (response) {
        //alert(response.ip);
        cip = response.ip;
    })
};

function errorCoords(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
    $.get("https://ipinfo.io/json", function (response) {

        //alert(cip);
        cip = response.ip;
        if (clat == "" || clong == "") {
            coords = response.loc.split(',')
            clat = coords[0]
            clong = coords[1]
        }
    })
};


function saveLog(clat, clong, cip) {
    $.ajax({
        type: 'GET',
        url: ROOT + 'LogUser/SaveLog',
        data: {
            ip: cip,
            lat: clat,
            lon: clong
        },
        success: function (response) {
            console.log("LogExitoso")
        },
        error: function (error) {
            console.log("Log con Error")
        }
    });
}

//%%%%%%%%%%%%%%

//Automatically fade alerts
$(".fadeauto").fadeTo(5000, 1000).slideUp(1000, function () {
    $(this).slideUp(500);
});


//##########################

// Colorbar

function getGradient(min, max, rgbs) {

    // Genera un vector de este tipo (Usado para la barra de color)

    //gradient = [[
    //    inrange(0),
    //    [184, 255, 181]
    //],
    //[
    //    inrange(0.28),
    //    [0, 128, 0]
    //],
    //[
    //    inrange(0.7),
    //    [0, 0, 255]
    //],
    //[
    //    inrange(1),
    //    [249, 174, 182]
    //    ]]


    // rgbs = 
    var range = max - min

    function inrange(x) {
        return x * range + min
    }
    var gradient = []
    rgbslen = rgbs.length - 1

    $.each(rgbs, function (index, value) {
        gradient = gradient.concat([
            [
                inrange(index * 1 / rgbslen),
                value
            ]]) // Rango Tipo 0,25,50,70,100
        //console.log(gradient)
    })

    return gradient;
};

function getScale(min1, max1) {
    // Calculates a linear function that wraps the original values, with max and min as bounds.
    min2 = (min1 + 0.01) * (1.01)
    max2 = max1 * (0.99)
    m = (max2 - min2) / (max1 - min1);
    b = max2 - m * max1
    return function (x) {
        return x * m + b;
    }
}


function getColorbar(min, max) {
    var range = (max - min);
    rgbs =
        [
            [249, 255, 239],
            [255, 226, 153],
            [255, 145, 99],
        ]
    gradient = getGradient(min, max, rgbs);
    scale = getScale(min, max)
    return function (num) { //value of the measure
        num1 = scale(num)
        if (num1 == 0)
            var colorRange = []
        $.each(gradient, function (index, value) { // Get the closest values in the colorbar
            //console.log("Num: " + num + "\t value[0]: " + value[0])
            if (num1 <= value[0]) {
                colorRange = [index - 1, index]
                //return false;
            }
        });

        //Get the two closest colors
        var firstcolor = gradient[colorRange[0]][1];
        var secondcolor = gradient[colorRange[1]][1];

        //Calculate ratio between the two closest colors
        var firstcolor_x = gradient[colorRange[0]][0];
        var secondcolor_x = gradient[colorRange[1]][0] - firstcolor_x;
        var slider_x = (num1) - firstcolor_x;
        var ratio = slider_x / secondcolor_x

        //Get the color with pickHex(thx, less.js's mix function!)
        var result = pickHex(secondcolor, firstcolor, ratio);

        return result
    }
}

function pickHex(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w / 1 + 1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
    Math.round(color1[1] * w1 + color2[1] * w2),
    Math.round(color1[2] * w1 + color2[2] * w2)];
    return rgb;
}

//------------------------------------------- Tables -------------------------

function renderReportTable(min, max, urlLanguage, title) {
    var colorBar = getColorbar(min, max)
    table = $('#tblBoundaries').DataTable(
        {
            "language": { "url": urlLanguage },
            "columnDefs": [{
                "targets": '_all',
                "createdCell": function (td, cellData, rowData, row, col) {
                    //console.log(col)
                    if (col >= 4) { // 4 is the column where the numeric values begin
                        //console.log(cellData)
                        num = parseFloat(cellData.replace(',', '.'))
                        rgb = colorBar(num)
                        $(td).css('background-color', `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`)
                    }
                }
            },
            ],
            "autoWidth": false,
            dom: 'lBfrtip',
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Visibilidad'
                },
                {
                    text: 'Formato',
                    action: function (e, dt, node, config) {
                        // Header of columns to hide before exporting
                        colHeaders = ['#', 'Frontera', 'H0'];

                        for (var i = 0; i < table.columns()[0].length; i++) {
                            column = table.column(i)
                            // aria-label: "Código: Activar para ordenar la columna de manera ascendente"
                            colHeader = column.header().getAttribute('aria-label').split(':')[0]
                            if (colHeaders.includes(colHeader)) {
                                column.visible(!column.visible()); // Toggle visibility of column
                            }
                        }

                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: ':visible'
                    },
                    title: title
                }

            ]
        })
    table.columns.adjust().draw()

    // Hide H0 column by default
    for (var i = 0; i < table.columns()[0].length; i++) {
        column = table.column(i);
        if (column.header().getAttribute('aria-label').split(':')[0] == 'H0') {
            column.visible(false);
        }
    }

    column.visible(!column.visible());

}