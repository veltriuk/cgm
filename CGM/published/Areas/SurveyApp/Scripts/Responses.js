﻿$(function () {
    enabled_validations();
    enabled_aditional_response();
    enabled_TLY();

    $("#Id_Response_Type").change(function () {
        enabled_validations();
    });

    $('#chk_aditionalresponse').click(function () {        
        enabled_aditional_response();
    });

});

function enabled_validations() {
    var Id_Response_Type = $("#Id_Response_Type").val();

    if (Id_Response_Type == 1) {
        $("#box_validation").show();
    }
    else {
        $("#box_validation").hide();
    }

    if (Id_Response_Type == 4 || Id_Response_Type == 5) {
        $("#box_datasource").show();
    }
    else {
        $("#box_datasource").hide();
    }
    
    
}

function enabled_aditional_response() {    

    if ($('#chk_aditionalresponse').is(':checked')) {
        $("#box_aditionalresponse").show();
    }
    else {
        $("#box_aditionalresponse").hide();
    }
}

function enabled_TLY() {
    //------------
    var id_ControlGroup = $("#id_ControlGroup").val();    

    if (id_ControlGroup == 3) {
        $("#box_TLY").show();
    }
    else {
        $("#box_TLY").hide();
    }
}